class AddNewFildToTestimonials < ActiveRecord::Migration[5.1]
  def change
  	remove_index :testimonials, :user_id
  	remove_column :testimonials, :user_id
  	add_column :testimonials, :name, :string
  	add_column :testimonials, :address, :string
  	add_attachment :testimonials, :image
  end
end
