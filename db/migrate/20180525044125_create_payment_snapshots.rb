class CreatePaymentSnapshots < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_snapshots do |t|
      t.string :code
      t.integer :admin_id
      t.string :user_type
      t.integer :user_id
      t.date :payment_start
      t.date :payment_end
      t.decimal :fee_am, default: 0
      t.decimal :fee_member, default: 0
      t.decimal :fee_referral, default: 0
      t.decimal :fee_company, default: 0
      t.decimal :total, default: 0
      t.decimal :price, default: 0
      t.string :category
      t.string :message
      t.string :from_acc
      t.string :to_acc
      t.string :status, default: "pending"

      t.timestamps
    end
    add_index :payment_snapshots, :admin_id
    add_index :payment_snapshots, :user_id
  end
end
