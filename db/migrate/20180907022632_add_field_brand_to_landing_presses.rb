class AddFieldBrandToLandingPresses < ActiveRecord::Migration[5.1]
  def change
  	add_column :landing_presses, :brand, :string
  end
end
