class ChangeTwitterInInstagram < ActiveRecord::Migration[5.1]
  def change
    rename_column :contacts, :twitter, :instagram
    rename_column :web_settings, :twitter, :instagram
  end
end
