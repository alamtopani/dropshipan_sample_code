class AddGoogleAnalyticToWebSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :web_settings, :google_analytic, :string
  end
end
