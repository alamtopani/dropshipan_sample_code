class AddCourierInOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :courier, :string
  end
end
