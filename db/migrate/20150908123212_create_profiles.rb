class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.string :full_name
      t.date :birthday
      t.string :gender
      t.string :phone
      t.string :no_ktp
      t.string :no_npwp
      t.attachment :avatar
      t.string :meta_title
      t.string :meta_keywords
      t.text :meta_description
      t.string :bank_name
      t.string :bank_branch
      t.string :account_name
      t.string :account_number
      t.attachment :cover

      t.timestamps
    end

    add_index :profiles, :user_id
  end
end
