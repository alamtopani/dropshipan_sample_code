class CreateLandingPages < ActiveRecord::Migration[5.0]
  def change
    create_table :landing_pages do |t|
      t.string :title
      t.string :slug
      t.text :description
      t.boolean :status, default: false
      t.string :category

      t.timestamps
    end
  end
end
