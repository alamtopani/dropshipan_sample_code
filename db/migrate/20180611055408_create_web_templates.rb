class CreateWebTemplates < ActiveRecord::Migration[5.1]
  def change
    create_table :web_templates do |t|
      t.string :name
      t.string :status, default: "pending"
      t.string :code_css
      t.attachment :image

      t.timestamps
    end
  end
end