class CreateLandingPressProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :landing_press_products do |t|
      t.integer :landing_press_id
      t.attachment :image
      t.string :title
      t.integer :position

      t.timestamps
    end
    	add_index :landing_press_products, :landing_press_id
  end
end
