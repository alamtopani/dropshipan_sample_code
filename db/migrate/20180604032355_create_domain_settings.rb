class CreateDomainSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :domain_settings do |t|
      t.integer :member_id
      t.string :name
      t.string :ip4
      t.string :ip6
      t.boolean :status, default: false

      t.timestamps
    end
    add_index :domain_settings, :member_id
  end
end
