class RenameChangeColumnBrandIdInProducts < ActiveRecord::Migration[5.1]
  def change
    rename_column :products, :brand_id, :brand_name
    change_column :products, :brand_name, :string
  end
end
