class AddStatusToRequestLp < ActiveRecord::Migration[5.1]
  def change
  	add_column :landing_presses, :status, :boolean, default: false
  end
end
