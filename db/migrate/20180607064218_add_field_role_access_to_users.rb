class AddFieldRoleAccessToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :role_access, :string
  end
end
