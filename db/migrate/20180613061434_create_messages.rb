class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.integer :group_message_id
      t.integer :from
      t.integer :to
      t.string :message
      t.string :status, default: 'unread'

      t.timestamps
    end
    add_index :messages, :group_message_id
  end
end
