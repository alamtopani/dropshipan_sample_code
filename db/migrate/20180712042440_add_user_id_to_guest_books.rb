class AddUserIdToGuestBooks < ActiveRecord::Migration[5.1]
  def change
  	add_column :guest_books, :user_id, :string
  end
end
