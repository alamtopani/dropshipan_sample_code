class AddPromoInInvoices < ActiveRecord::Migration[5.1]
  def change
    add_column :invoices, :promo, :boolean, default: false
  end
end
