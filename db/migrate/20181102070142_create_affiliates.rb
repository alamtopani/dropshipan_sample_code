class CreateAffiliates < ActiveRecord::Migration[5.1]
  def change
    create_table :affiliates do |t|
			t.integer :member_id
			t.integer :invoice_id
			t.integer :member_registration_id
			t.decimal :basic_price, default: 0
			t.decimal :sale_price, default: 0
			t.decimal :fee_company, default: 0
			t.decimal :fee_member, default: 0
			t.decimal :fee_affiliate, default: 0
			t.string :status, default: "pending"
			t.string :code

      t.timestamps
    end
    add_index :affiliates, :member_id
    add_index :affiliates, :invoice_id
    add_index :affiliates, :member_registration_id
  end
end
