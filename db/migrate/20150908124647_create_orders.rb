class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.integer :member_id
      t.integer :account_manager_id
      t.string :slug
      t.string :code
      t.string :payment_token
      t.string :payer_name
      t.string :payer_email
      t.string :payer_address
      t.string :payer_phone
      t.string :payment_method
      t.string :shipping_method
      t.string :payment_to
      t.decimal :price, default: 0
      t.decimal :basic_price, default: 0
      t.decimal :company_price, default: 0
      t.decimal :shipping_price, default: 0
      t.decimal :total_price, default: 0
      t.decimal :total_basic_price, default: 0
      t.decimal :total_company_price, default: 0
      t.string :track_order, default: 'verification'
      t.date :purchased_at
      t.integer :order_status_id
      t.string :number_shipping

      t.timestamps null: false
    end

    add_index :orders, :member_id
    add_index :orders, :account_manager_id
  end
end
