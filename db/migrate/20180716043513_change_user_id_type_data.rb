class ChangeUserIdTypeData < ActiveRecord::Migration[5.1]
  def change
  	change_column :guest_books, :user_id, 'integer USING CAST(user_id AS integer)'
  	add_index :guest_books, :user_id
  end
end
