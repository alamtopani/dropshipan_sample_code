class CreatePaymentReferrals < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_referrals do |t|
      t.string :code
      t.integer :admin_id
      t.integer :member_id
      t.date :payment_start
      t.date :payment_end
      t.decimal :commission_referral
      t.string :from_acc
      t.string :to_acc
      t.string :message
      t.boolean :payment_pending
      t.string :quote
      t.string :status

      t.timestamps
    end
    add_index :payment_referrals, :admin_id
    add_index :payment_referrals, :member_id
  end
end
