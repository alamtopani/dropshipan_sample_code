class CreateRegions < ActiveRecord::Migration[5.0]
  def change
    create_table :regions do |t|
      t.string :name
      t.string :ancestry
      t.boolean :featured, default: false
      t.attachment :image

      t.timestamps null: false
    end
  end
end
