class CreateSpendings < ActiveRecord::Migration[5.1]
  def change
    create_table :spendings do |t|
      t.integer :admin_id
      t.string :code
			t.string :title
			t.string :description
			t.decimal :price
			t.date :transaction_date
			t.string :from_acc 
			t.string :to_acc 
			t.attachment :file
			t.string :status,  default: "pending"
			t.string :category,  default: "other"

      t.timestamps
    end
    add_index :spendings, :admin_id
  end
end
