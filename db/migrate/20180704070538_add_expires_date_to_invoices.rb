class AddExpiresDateToInvoices < ActiveRecord::Migration[5.1]
  def change
    add_column :invoices, :expired_date, :date
  end
end
