class AddAcceptedRulesToWebSetting < ActiveRecord::Migration[5.1]
  def change
  	add_column :web_settings, :accepted_rules, :string
  	add_column :users, :accepted, :boolean, default: false
  end
end
