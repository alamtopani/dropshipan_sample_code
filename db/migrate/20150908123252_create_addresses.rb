class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.text :address
      t.string :city
      t.string :province
      t.string :country
      t.string :postcode
      t.string :addressable_type
      t.integer :addressable_id
      t.string :district

      t.timestamps
    end

    add_index :addresses, :addressable_type
    add_index :addresses, :addressable_id
  end
end
