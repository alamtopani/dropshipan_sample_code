class CreateProductStocks < ActiveRecord::Migration[5.1]
  def change
    create_table :product_stocks do |t|
      t.integer :product_id
      t.string :variant
      t.integer :stock, default: 0

      t.timestamps
    end

    add_index :product_stocks, :product_id
  end
end
