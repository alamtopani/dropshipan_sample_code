class AddOriginInOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :origin_code, :string
    add_column :orders, :origin_name, :string
  end
end
