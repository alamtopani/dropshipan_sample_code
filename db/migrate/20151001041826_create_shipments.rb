class CreateShipments < ActiveRecord::Migration[5.0]
  def change
    create_table :shipments do |t|
      t.string :name
      t.string :province
      t.string :city
      t.string :state
      t.string :postal_code
      t.text :address
      t.float :weight
      t.decimal :price
      t.string :shipmentable_type
      t.string :shipmentable_id

      t.timestamps null: false
    end
  end
end
