class AddProductIdToLandingPresses < ActiveRecord::Migration[5.1]
  def change
  	add_column :landing_presses, :product_id, :integer
  end
end
