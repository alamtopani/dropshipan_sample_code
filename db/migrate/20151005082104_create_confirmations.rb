class CreateConfirmations < ActiveRecord::Migration[5.0]
  def change
    create_table :confirmations do |t|
      t.integer :member_id
      t.string :slug
      t.string :code
      t.string :no_invoice
      t.string :name
      t.string :email
      t.datetime :payment_date
      t.decimal :nominal
      t.string :bank_account
      t.string :payment_method
      t.string :sender_name
      t.text :message
      t.attachment :file
      t.string :confirmation_type, default: 'to_member'

      t.timestamps null: false
    end

    add_index :confirmations, :member_id
  end
end
