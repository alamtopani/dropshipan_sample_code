class CreateProductTestimonials < ActiveRecord::Migration[5.1]
  def change
    create_table :product_testimonials do |t|
      t.integer :product_id
      t.attachment :image

      t.timestamps
    end
    add_index :product_testimonials, :product_id
  end
end
