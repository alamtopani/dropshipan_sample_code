class CreateStakeholderOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :stakeholder_orders do |t|
      t.string :no_invoice
      t.integer :order_id
      t.integer :merchant_id
      t.integer :member_id
      t.integer :account_manager_id
      t.integer :weight
      t.decimal :price
      t.decimal :basic_price, default: 0
      t.decimal :company_price, default: 0
      t.decimal :shipping_price, default: 0
      t.decimal :total_price, default: 0
      t.decimal :total_basic_price, default: 0
      t.decimal :total_company_price, default: 0
      t.boolean :payment_status, default: false
      t.string :track_order, default: 'verification'

      t.timestamps null: false
    end

    add_index :stakeholder_orders, :order_id
    add_index :stakeholder_orders, :member_id
    add_index :stakeholder_orders, :account_manager_id
    add_index :stakeholder_orders, :merchant_id
  end
end
