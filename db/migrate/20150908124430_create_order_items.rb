class CreateOrderItems < ActiveRecord::Migration[5.0]
  def change
    create_table :order_items do |t|
      t.string :no_invoice
      t.integer :catalog_id
      t.integer :product_id
      t.integer :order_id
      t.integer :member_id
      t.integer :account_manager_id
      t.integer :merchant_id
      t.integer :stakeholder_order_id
      t.decimal :price, default: 0
      t.decimal :basic_price, default: 0
      t.decimal :company_price, default: 0
      t.integer :quantity
      t.decimal :total_price, default: 0
      t.decimal :total_basic_price, default: 0
      t.decimal :total_company_price, default: 0
      t.integer :weight
      t.decimal :discount, default: 0
      t.string :variant
      t.string :message
      t.string :status, default: 'pending'
      t.string :cancelled_reason

      t.timestamps null: false
    end

    add_index :order_items, :order_id
    add_index :order_items, :catalog_id
    add_index :order_items, :product_id
    add_index :order_items, :member_id
    add_index :order_items, :account_manager_id
    add_index :order_items, :merchant_id
    add_index :order_items, :stakeholder_order_id
  end
end
