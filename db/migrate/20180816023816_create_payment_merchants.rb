class CreatePaymentMerchants < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_merchants do |t|
      t.string :code
      t.integer :merchant_id
      t.integer :admin_id
			t.date :payment_start
			t.date :payment_end
			t.decimal :price
			t.decimal :ongkir
			t.decimal :total
			t.string :from_acc
			t.string :to_acc
			t.string :status, default: "pending"
			t.string :message

      t.timestamps
    end
    	add_index :payment_merchants, :merchant_id
    	add_index :payment_merchants, :admin_id
  end
end
