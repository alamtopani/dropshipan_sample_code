class AddFieldToWebSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :web_settings, :order_via_cart, :boolean, default: true
    add_column :web_settings, :order_via_wa, :boolean, default: true
  end
end
