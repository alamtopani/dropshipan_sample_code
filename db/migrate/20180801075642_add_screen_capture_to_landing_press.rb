class AddScreenCaptureToLandingPress < ActiveRecord::Migration[5.1]
  def change
  	add_attachment :landing_presses, :screen_capture
  end
end
