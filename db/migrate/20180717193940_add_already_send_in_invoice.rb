class AddAlreadySendInInvoice < ActiveRecord::Migration[5.1]
  def change
    add_column :invoices, :already_send, :boolean, default: false
  end
end
