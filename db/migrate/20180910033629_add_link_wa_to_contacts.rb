class AddLinkWaToContacts < ActiveRecord::Migration[5.1]
  def change
    add_column :contacts, :link_wa, :string
    add_column :contacts, :use_link_wa, :boolean, default: false
  end
end
