class CreatePackages < ActiveRecord::Migration[5.1]
  def change
    create_table :packages do |t|
      t.string :title
      t.string :package_type
      t.integer :period
      t.decimal :price, default: 0
      t.text :description
      t.string :status, default: "inactive"
      t.attachment :image

      t.timestamps
    end
  end
end
