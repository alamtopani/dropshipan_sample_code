class CreateWebSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :web_settings do |t|
      t.integer :user_id
      t.string :title
      t.text :description
      t.text :keywords
      t.text :header_tags
      t.text :footer_tags
      t.string :contact
      t.string :email
      t.attachment :favicon
      t.attachment :logo
      t.attachment :logo_banner
      t.string :facebook
      t.string :twitter
      t.string :robot
      t.string :author
      t.string :corpyright
      t.string :revisit
      t.string :expires
      t.string :revisit_after
      t.string :geo_placename
      t.string :language
      t.string :country
      t.string :content_language
      t.string :distribution
      t.string :generator
      t.string :rating
      t.string :target
      t.string :search_engines
      t.string :address
      t.string :longitude
      t.string :latitude
      t.string :fb_fixels_dashboard
      t.string :fb_fixels_view
      t.string :fb_fixels_cart
      t.string :fb_fixels_checkout
      t.string :fb_fixels_purchase

      t.timestamps
    end
    add_index :web_settings, :user_id
  end
end
