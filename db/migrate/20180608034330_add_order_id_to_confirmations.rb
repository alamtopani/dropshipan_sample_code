class AddOrderIdToConfirmations < ActiveRecord::Migration[5.1]
  def change
    add_column :confirmations, :order_id, :integer

    add_index :confirmations, :order_id
  end
end
