class RenameColumnTemplateIdInSiteInfo < ActiveRecord::Migration[5.1]
  def change
    rename_column :site_infos, :template_id, :web_template_id

    add_index :site_infos, :web_template_id
  end
end
