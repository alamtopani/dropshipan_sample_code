class AddDistrictFullToAddress < ActiveRecord::Migration[5.1]
  def change
    add_column :addresses, :district_full, :string
  end
end
