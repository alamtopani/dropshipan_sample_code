class CreateRequestLandingPresses < ActiveRecord::Migration[5.1]
  def change
    create_table :request_landing_presses do |t|
      t.string :code
      t.integer :member_id
      t.integer :landing_press_id
      t.string :price
      t.string :contact

      t.timestamps
    end
    	add_index :request_landing_presses, :member_id
    	add_index :request_landing_presses, :landing_press_id
  end
end
