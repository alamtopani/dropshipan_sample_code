class AddAvailableCodToMerchantInfo < ActiveRecord::Migration[5.1]
  def change
  	add_column :merchant_infos, :available_cod, :boolean, default: false
  end
end
