class CreateGuestBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :guest_books do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.text :description
      t.boolean :status, default: false

      t.timestamps null: false
    end
  end
end
