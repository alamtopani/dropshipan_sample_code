class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
      t.string :phone
      t.string :handphone
      t.string :bbm
      t.string :line
      t.string :wa
      t.string :facebook
      t.string :twitter
      t.string :website
      t.string :contactable_type
      t.integer :contactable_id

      t.timestamps null: false
    end
    add_index :contacts, :contactable_type
    add_index :contacts, :contactable_id
  end
end
