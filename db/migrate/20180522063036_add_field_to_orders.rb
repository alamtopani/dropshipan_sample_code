class AddFieldToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :uniq_code, :string

    change_column :invoices, :period_start, :date
    change_column :invoices, :period_end, :date
  end
end
