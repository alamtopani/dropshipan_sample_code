class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.string :name
      t.string :ancestry
      t.string :slug
      t.string :link_download
      t.attachment :file
      t.text       :description
      t.boolean    :status, default: false

      t.timestamps null: false
    end
  end
end
