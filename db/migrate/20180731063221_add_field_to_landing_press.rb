class AddFieldToLandingPress < ActiveRecord::Migration[5.1]
  def change
  	add_column :landing_presses, :code, :string
  	add_column :landing_presses, :category, :string
  	add_column :landing_presses, :link_video, :string
  end
end
