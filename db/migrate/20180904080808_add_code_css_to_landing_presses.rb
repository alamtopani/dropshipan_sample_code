class AddCodeCssToLandingPresses < ActiveRecord::Migration[5.1]
  def change
    add_column :landing_presses, :code_css, :string
  end
end
