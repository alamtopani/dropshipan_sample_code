class CreateLandingPresses < ActiveRecord::Migration[5.1]
  def change
    create_table :landing_presses do |t|
      t.attachment :banner
		  t.string :title1
		  t.string :description1
		  t.attachment :image1
		  t.attachment :image2
		  t.attachment :image3
		  t.string :title2
		  t.string :description2
		  t.attachment :image4
		  t.attachment :image5	
		  t.string :title3
		  t.string :description3
		  t.string :title_banner
		  t.string :title4
		  t.string :title_banner2
		  t.string :description4
		  t.attachment :image6
		  t.string :title5
		  t.string :description5
		  t.string :price
		  t.string :telephone
		  t.string :footer

      t.timestamps
    end
  end
end
