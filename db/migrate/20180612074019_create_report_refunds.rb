class CreateReportRefunds < ActiveRecord::Migration[5.1]
  def change
    create_table :report_refunds do |t|
      t.integer :order_id
      t.integer :order_item_id
      t.decimal :refund_value, default: 0
      t.decimal :ongkir, default: 0
      t.decimal :total, default: 0
      t.string :status, default: "pending"

      t.timestamps
    end
    add_index :report_refunds, :order_id
    add_index :report_refunds, :order_item_id
  end
end
