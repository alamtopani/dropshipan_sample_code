class ChangeRenameReportRefundField < ActiveRecord::Migration[5.1]
  def change
    rename_column :report_refunds, :order_item_id, :quantity
  end
end
