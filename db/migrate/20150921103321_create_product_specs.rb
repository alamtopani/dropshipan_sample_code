class CreateProductSpecs < ActiveRecord::Migration[5.0]
  def change
    create_table :product_specs do |t|
      t.integer :product_id
      t.integer :brand_id
      t.text :information
      t.text :material
      t.integer :stock, default: 0
      t.datetime :deleted_at

      t.timestamps null: false
    end

    add_index :product_specs, :brand_id
    add_index :product_specs, :product_id
    add_index :product_specs, :deleted_at
  end
end
