class AddFieldToReportRefund < ActiveRecord::Migration[5.1]
  def change
    add_column :report_refunds, :member_id, :integer
    add_column :report_refunds, :account_manager_id, :integer
    add_column :report_refunds, :from_acc, :string
    add_column :report_refunds, :to_acc, :string

    add_index :report_refunds, :member_id
    add_index :report_refunds, :account_manager_id
  end
end
