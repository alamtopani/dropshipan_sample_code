class TotalDelayToPaymentReferral < ActiveRecord::Migration[5.1]
  def change
  	add_column :payment_referrals, :total_delay, :integer
  end
end
