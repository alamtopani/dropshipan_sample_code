class CreateMerchantInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :merchant_infos do |t|
      t.integer :merchant_id
      t.string :title
      t.attachment :banner
      t.text :description
      t.text :policies
      t.text :payment
      t.text :shipping
      t.text :refund_and_exchange
      t.text :faq
      t.string :product_type
      t.string :no_bpom
      t.integer :distribution_stock
      t.string :sample_product
      t.attachment :sample_product_image

      t.timestamps null: false
    end

    add_index :merchant_infos, :merchant_id
  end
end
