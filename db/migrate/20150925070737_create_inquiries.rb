class CreateInquiries < ActiveRecord::Migration[5.0]
  def change
    create_table :inquiries do |t|
      t.integer :user_id
      t.string :name
      t.string :email
      t.string :phone
      t.text :message
      t.string :inquiriable_type
      t.integer :inquiriable_id
      t.boolean :status, default: false
      t.string :ancestry
      t.integer :owner_id

      t.timestamps null: false
    end

    add_index :inquiries, :user_id
    add_index :inquiries, :owner_id
  end
end
