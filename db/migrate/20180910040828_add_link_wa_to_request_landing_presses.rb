class AddLinkWaToRequestLandingPresses < ActiveRecord::Migration[5.1]
  def change
    add_column :request_landing_presses, :link_wa, :string
    add_column :request_landing_presses, :use_link_wa, :boolean, default: false
  end
end
