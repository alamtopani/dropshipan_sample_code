class CreateSiteInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :site_infos do |t|
      t.integer :member_id
      t.integer :package_id
      t.integer :template_id
      t.boolean :buy_domain
      t.string :domain_name
      t.boolean :buy_logo
      t.string :logo_title
      t.decimal :discount_global, default: 0
      t.string :keywords_1
      t.string :keywords_2
      t.string :keywords_3

      t.timestamps
    end
    add_index :site_infos, :member_id
    add_index :site_infos, :package_id
  end
end
