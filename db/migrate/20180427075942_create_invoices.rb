class CreateInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :invoices do |t|
      t.integer :user_id
      t.string :code
      t.string :subject
      t.string :description
      t.decimal :price, default: 0
      t.string :uniq_code
      t.string :bank_name
      t.string :bank_branch
      t.string :account_name
      t.string :account_number
      t.datetime :period_start
      t.datetime :period_end
      t.string :status, default: "unpaid"
      t.string :invoiceable_type
      t.integer :invoiceable_id

      t.timestamps
    end
    add_index :invoices, :user_id
    add_index :invoices, :invoiceable_type
    add_index :invoices, :invoiceable_id
  end
end