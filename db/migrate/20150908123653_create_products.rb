class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :code
      t.string :slug
      t.integer :merchant_id
      t.integer :category_id
      t.integer :sub_category_id
      t.integer :brand_id
      t.string :title
      t.decimal :basic_price, default: 0
      t.decimal :price, default: 0
      t.decimal :recommended_price, default: 0
      t.text :description
      t.boolean :status, default: false
      t.boolean :featured, default: false
      t.string :status_stock
      t.date :start_at
      t.date :end_at
      t.decimal :weight, default: 0
      t.attachment :cover
      t.datetime :deleted_at

      t.timestamps null: false
    end

    add_index :products, :merchant_id
    add_index :products, :category_id
    add_index :products, :sub_category_id
    add_index :products, :brand_id
    add_index :products, :deleted_at
  end
end
