class AddFieldMessageToDomainSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :domain_settings, :message, :string
  end
end
