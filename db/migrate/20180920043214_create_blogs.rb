class CreateBlogs < ActiveRecord::Migration[5.1]
  def change
    create_table :blogs do |t|
    	t.string :slug
			t.integer :user_id
			t.string :title
			t.string :description
			t.string :category
			t.string :link_video
			t.attachment :file
			t.string :status, default: 'active'
			t.integer :view_count, default: 0

      t.timestamps
    end
    	add_index :blogs, :user_id
  end
end
