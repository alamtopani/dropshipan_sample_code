class AddFromAccToConfirmations < ActiveRecord::Migration[5.1]
  def change
    add_column :confirmations, :from_bank_name, :string
    add_column :confirmations, :from_account_name, :string
    add_column :confirmations, :from_account_number, :string
  end
end
