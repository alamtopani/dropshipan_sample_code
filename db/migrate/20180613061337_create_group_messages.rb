class CreateGroupMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :group_messages do |t|
      t.integer :member_id
      t.integer :account_manager_id
      t.string :status, default: 'active'

      t.timestamps
    end
    add_index :group_messages, :member_id
    add_index :group_messages, :account_manager_id
  end
end
