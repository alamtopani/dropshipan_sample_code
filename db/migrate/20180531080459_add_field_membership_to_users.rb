class AddFieldMembershipToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :membership, :boolean, default: false
  end
end
