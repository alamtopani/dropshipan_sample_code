class CreatePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments do |t|
      t.string :name
      t.attachment :logo
      t.string :bank_name
      t.string :account_number
      t.integer :paymentable_id
      t.string :paymentable_type
      t.integer :position

      t.timestamps null: false
    end
  end
end
