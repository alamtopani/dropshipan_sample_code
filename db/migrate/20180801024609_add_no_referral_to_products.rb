class AddNoReferralToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :no_referral, :boolean, default: false
  end
end
