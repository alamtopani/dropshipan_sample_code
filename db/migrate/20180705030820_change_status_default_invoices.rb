class ChangeStatusDefaultInvoices < ActiveRecord::Migration[5.1]
  def change
    change_column_default(:invoices, :status, 'pending')
  end
end
