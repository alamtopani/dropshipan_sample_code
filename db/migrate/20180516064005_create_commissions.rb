class CreateCommissions < ActiveRecord::Migration[5.1]
  def change
    create_table :commissions do |t|
      t.integer :catalog_id
      t.integer :product_id
      t.integer :member_id
      t.integer :account_manager_id
      t.integer :order_id
      t.integer :order_item_id
      t.string :uniq_code
      t.integer :quantity, default: 0
      t.decimal :commission_member, default: 0
      t.decimal :commission_am, default: 0
      t.decimal :commission_company, default: 0
      t.datetime :commission_date
      t.string :status, default: 'pending'

      t.timestamps
    end

    add_index :commissions, :catalog_id
    add_index :commissions, :product_id
    add_index :commissions, :member_id
    add_index :commissions, :account_manager_id
    add_index :commissions, :order_id
    add_index :commissions, :order_item_id
  end
end
