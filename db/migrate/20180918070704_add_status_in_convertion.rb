class AddStatusInConvertion < ActiveRecord::Migration[5.1]
  def change
    add_column :convertions, :status, :string, default: 'visited'
    change_column :convertions, :click_id, :string
  end
end
