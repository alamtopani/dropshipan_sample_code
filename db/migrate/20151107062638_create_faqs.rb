class CreateFaqs < ActiveRecord::Migration[5.0]
  def change
    create_table :faqs do |t|
      t.string :payment
      t.string :shipping
      t.string :refund_and_exchange
      t.string :faq
      t.integer :faqable_id
      t.string :faqable_type

      t.timestamps null: false
    end
  end
end
