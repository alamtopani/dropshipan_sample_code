class AddMetaTitleMetaKeywordsToRequestLandingPresses < ActiveRecord::Migration[5.1]
  def change
    add_column :request_landing_presses, :meta_title, :string
    add_column :request_landing_presses, :meta_keywords, :string
  end
end
