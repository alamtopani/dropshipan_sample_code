class EditColumnNameDomainSetting < ActiveRecord::Migration[5.1]
  def change
    rename_column :domain_settings, :ip4, :ns1
    rename_column :domain_settings, :ip6, :ns2
    change_column :domain_settings, :status, :string
    change_column_default :domain_settings, :status, "pending"
  end
end
