class AddAccountManagerIdToConfirmations < ActiveRecord::Migration[5.1]
  def change
    add_column :confirmations, :account_manager_id, :integer

    add_index :confirmations, :account_manager_id
  end
end
