class CreateCatalogs < ActiveRecord::Migration[5.1]
  def change
    create_table :catalogs do |t|
      t.integer :product_id
      t.integer :member_id
      t.integer :account_manager_id
      t.decimal :price, default: 0
      t.decimal :discount, default: 0
      t.datetime :deleted_at
      t.string :description
      t.string :meta_title
      t.string :meta_keywords
      t.string :meta_description
      t.string :status, default: 'pending'
      t.string :message

      t.timestamps
    end

    add_index :catalogs, :member_id
    add_index :catalogs, :account_manager_id
    add_index :catalogs, :product_id
  end
end
