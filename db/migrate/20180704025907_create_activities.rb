class CreateActivities < ActiveRecord::Migration[5.1]
  def change
    create_table :activities do |t|
      t.integer :user_id
      t.string :title
      t.string :description
      t.string :activitiable_id
      t.string :activitiable_type
      t.string :status, default: "unread"

      t.timestamps
    end
    add_index :activities, :user_id
    add_index :activities, :activitiable_id
  end
end