class CreateGalleries < ActiveRecord::Migration[5.0]
  def change
    create_table :galleries do |t|
      t.string :title
      t.text :description
      t.attachment :file
      t.string :galleriable_type
      t.integer :galleriable_id
      t.integer :position
      t.datetime :deleted_at

      t.timestamps
    end

    add_index :galleries, :galleriable_type
    add_index :galleries, :galleriable_id
    add_index :galleries, :deleted_at
  end
end
