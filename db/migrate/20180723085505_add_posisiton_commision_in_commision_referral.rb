class AddPosisitonCommisionInCommisionReferral < ActiveRecord::Migration[5.1]
  def change
    add_column :commission_referrals, :position_ref, :string
    add_column :commission_referrals, :percentage, :integer
  end
end
