class SetDefaultField < ActiveRecord::Migration[5.1]
  def change
    change_column_default :payment_merchants, :price, 0
    change_column_default :payment_merchants, :ongkir, 0
    change_column_default :payment_merchants, :total, 0

    change_column_default :payment_referrals, :commission_referral, 0
    change_column_default :payment_referrals, :payment_pending, true
    change_column_default :payment_referrals, :status, "pending"
  end
end
