class CreateConvertions < ActiveRecord::Migration[5.1]
  def change
    create_table :convertions do |t|
      t.integer :pub_id
      t.integer :campaign_id
      t.integer :click_id
      t.integer :invoice_id
      t.string :country_code, default: "ID"
      t.decimal :payout, default: 5

      t.timestamps
    end
    add_index :convertions, :invoice_id
  end
end
