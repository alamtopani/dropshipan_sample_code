module SeedWebSetting
  def self.seed
    WebSetting.find_or_create_by({
      user_id: User.first.id,
      header_tags: "Dropshipan Market",
      footer_tags: "Dropshipan Market",
      contact: '(0266) 234657',
      email: 'sales@Dropshipan.com',
      facebook: 'http://www.facebook.com/',
      instagram: 'http://www.instagram.com/',
      title: "Dropshipan Market",
      keywords: "Dropshipan Market, Situs Jual beli barang",
      description: 'Etsy was founded in June 2005 in an apartment in Brooklyn, New York to fill a need for an online community where crafters, artists and makers could sell their handmade and vintage goods and craft supplies. In the spirit of handmade, founder Rob Kalin and two friends designed the first site',
      robot: 'Follow',
      author: "Dropshipan Market",
      corpyright: "Dropshipan Market",
      revisit: '2 Days',
      expires: 'Never',
      revisit_after: '2 Days',
      geo_placename: 'Indonesia',
      language: 'ID',
      country: 'ID',
      content_language: 'All-Language',
      distribution: 'global',
      generator: 'website',
      rating: 'general',
      target: 'global',
      search_engines: "Dropshipan Market, Jual Beli Barang"
    })
  end
end
