module SeedLandingPage
  CATEGORY = ['about_us','buy','sell','help']
  ABOUT_LANDING_PAGE = ['Tentang Kami','Kisah Penjual']
  BUY_LANDING_PAGE = ['Belanja di Kami','Cara Berbelanja','Pembayaran','Pengembalian']
  SELL_LANDING_PAGE = ['Jualan di Kami','Cara Berjualan','Gold Merchant','Beriklan']
  HELP_LANDING_PAGE = ['Syarat dan Ketentuan','Kebijakan Privasi','Pusat Resolusi']

  def self.seed 
    CATEGORY.each do |category|
      if category['about_us']
        ABOUT_LANDING_PAGE.each do |page|
          page = LandingPage.find_or_initialize_by(title: page)
          page.category = 'about_us'
          page.status = true
          page.save
        end
      elsif category['buy']
        BUY_LANDING_PAGE.each do |page|
          page = LandingPage.find_or_initialize_by(title: page)
          page.category = 'buy'
          page.status = true
          page.save
        end
      elsif category['sell']
        SELL_LANDING_PAGE.each do |page|
          page = LandingPage.find_or_initialize_by(title: page)
          page.category = 'sell'
          page.status = true
          page.save
        end
      elsif category['help']
        HELP_LANDING_PAGE.each do |page|
          page = LandingPage.find_or_initialize_by(title: page)
          page.category = 'help'
          page.status = true
          page.save
        end
      end 
    end
  end
end