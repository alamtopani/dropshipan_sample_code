# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181102070142) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.integer "user_id"
    t.string "title"
    t.string "description"
    t.string "activitiable_id"
    t.string "activitiable_type"
    t.string "status", default: "unread"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["activitiable_id"], name: "index_activities_on_activitiable_id"
    t.index ["user_id"], name: "index_activities_on_user_id"
  end

  create_table "addresses", id: :serial, force: :cascade do |t|
    t.text "address"
    t.string "city"
    t.string "province"
    t.string "country"
    t.string "postcode"
    t.string "addressable_type"
    t.integer "addressable_id"
    t.string "district"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "district_full"
    t.index ["addressable_id"], name: "index_addresses_on_addressable_id"
    t.index ["addressable_type"], name: "index_addresses_on_addressable_type"
  end

  create_table "affiliates", force: :cascade do |t|
    t.integer "member_id"
    t.integer "invoice_id"
    t.integer "member_registration_id"
    t.decimal "basic_price", default: "0.0"
    t.decimal "sale_price", default: "0.0"
    t.decimal "fee_company", default: "0.0"
    t.decimal "fee_member", default: "0.0"
    t.decimal "fee_affiliate", default: "0.0"
    t.string "status", default: "pending"
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["invoice_id"], name: "index_affiliates_on_invoice_id"
    t.index ["member_id"], name: "index_affiliates_on_member_id"
    t.index ["member_registration_id"], name: "index_affiliates_on_member_registration_id"
  end

  create_table "ahoy_events", force: :cascade do |t|
    t.bigint "visit_id"
    t.bigint "user_id"
    t.string "name"
    t.jsonb "properties"
    t.datetime "time"
    t.index "properties jsonb_path_ops", name: "index_ahoy_events_on_properties_jsonb_path_ops", using: :gin
    t.index ["name", "time"], name: "index_ahoy_events_on_name_and_time"
    t.index ["user_id"], name: "index_ahoy_events_on_user_id"
    t.index ["visit_id"], name: "index_ahoy_events_on_visit_id"
  end

  create_table "ahoy_visits", force: :cascade do |t|
    t.string "visit_token"
    t.string "visitor_token"
    t.bigint "user_id"
    t.string "ip"
    t.text "user_agent"
    t.text "referrer"
    t.string "referring_domain"
    t.string "search_keyword"
    t.text "landing_page"
    t.string "browser"
    t.string "os"
    t.string "device_type"
    t.string "country"
    t.string "region"
    t.string "city"
    t.string "utm_source"
    t.string "utm_medium"
    t.string "utm_term"
    t.string "utm_content"
    t.string "utm_campaign"
    t.datetime "started_at"
    t.index ["user_id"], name: "index_ahoy_visits_on_user_id"
    t.index ["visit_token"], name: "index_ahoy_visits_on_visit_token", unique: true
  end

  create_table "blogs", force: :cascade do |t|
    t.string "slug"
    t.integer "user_id"
    t.string "title"
    t.string "description"
    t.string "category"
    t.string "link_video"
    t.string "file_file_name"
    t.string "file_content_type"
    t.integer "file_file_size"
    t.datetime "file_updated_at"
    t.string "status", default: "active"
    t.integer "view_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_blogs_on_user_id"
  end

  create_table "brands", force: :cascade do |t|
    t.string "name"
    t.string "logo_file_name"
    t.string "logo_content_type"
    t.integer "logo_file_size"
    t.datetime "logo_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "catalogs", force: :cascade do |t|
    t.integer "product_id"
    t.integer "member_id"
    t.integer "account_manager_id"
    t.decimal "price", default: "0.0"
    t.decimal "discount", default: "0.0"
    t.datetime "deleted_at"
    t.string "description"
    t.string "meta_title"
    t.string "meta_keywords"
    t.string "meta_description"
    t.string "status", default: "pending"
    t.string "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code"
    t.index ["account_manager_id"], name: "index_catalogs_on_account_manager_id"
    t.index ["member_id"], name: "index_catalogs_on_member_id"
    t.index ["product_id"], name: "index_catalogs_on_product_id"
  end

  create_table "categories", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "ancestry"
    t.string "slug"
    t.string "link_download"
    t.string "file_file_name"
    t.string "file_content_type"
    t.integer "file_file_size"
    t.datetime "file_updated_at"
    t.text "description"
    t.boolean "status", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "commentable_type"
    t.integer "commentable_id"
    t.text "comment"
    t.boolean "status", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["commentable_id"], name: "index_comments_on_commentable_id"
    t.index ["commentable_type"], name: "index_comments_on_commentable_type"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "commission_referrals", force: :cascade do |t|
    t.integer "member_id"
    t.integer "commission_id"
    t.decimal "commission_value", default: "0.0"
    t.string "status", default: "pending"
    t.datetime "commission_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "position_ref"
    t.integer "percentage"
    t.index ["commission_id"], name: "index_commission_referrals_on_commission_id"
    t.index ["member_id"], name: "index_commission_referrals_on_member_id"
  end

  create_table "commissions", force: :cascade do |t|
    t.integer "catalog_id"
    t.integer "product_id"
    t.integer "member_id"
    t.integer "account_manager_id"
    t.integer "order_id"
    t.integer "order_item_id"
    t.string "uniq_code"
    t.integer "quantity", default: 0
    t.decimal "commission_member", default: "0.0"
    t.decimal "commission_am", default: "0.0"
    t.decimal "commission_company", default: "0.0"
    t.datetime "commission_date"
    t.string "status", default: "pending"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_manager_id"], name: "index_commissions_on_account_manager_id"
    t.index ["catalog_id"], name: "index_commissions_on_catalog_id"
    t.index ["member_id"], name: "index_commissions_on_member_id"
    t.index ["order_id"], name: "index_commissions_on_order_id"
    t.index ["order_item_id"], name: "index_commissions_on_order_item_id"
    t.index ["product_id"], name: "index_commissions_on_product_id"
  end

  create_table "confirmations", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.string "slug"
    t.string "code"
    t.string "no_invoice"
    t.string "name"
    t.string "email"
    t.datetime "payment_date"
    t.decimal "nominal"
    t.string "bank_account"
    t.string "payment_method"
    t.string "sender_name"
    t.text "message"
    t.string "file_file_name"
    t.string "file_content_type"
    t.integer "file_file_size"
    t.datetime "file_updated_at"
    t.string "confirmation_type", default: "to_member"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "from_bank_name"
    t.string "from_account_name"
    t.string "from_account_number"
    t.integer "order_id"
    t.integer "account_manager_id"
    t.index ["account_manager_id"], name: "index_confirmations_on_account_manager_id"
    t.index ["member_id"], name: "index_confirmations_on_member_id"
    t.index ["order_id"], name: "index_confirmations_on_order_id"
  end

  create_table "contacts", id: :serial, force: :cascade do |t|
    t.string "phone"
    t.string "handphone"
    t.string "bbm"
    t.string "line"
    t.string "wa"
    t.string "facebook"
    t.string "instagram"
    t.string "website"
    t.string "contactable_type"
    t.integer "contactable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "link_wa"
    t.boolean "use_link_wa", default: false
    t.index ["contactable_id"], name: "index_contacts_on_contactable_id"
    t.index ["contactable_type"], name: "index_contacts_on_contactable_type"
  end

  create_table "convertions", force: :cascade do |t|
    t.integer "pub_id"
    t.integer "campaign_id"
    t.string "click_id"
    t.integer "invoice_id"
    t.string "country_code", default: "ID"
    t.decimal "payout", default: "5.0"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status", default: "visited"
    t.index ["invoice_id"], name: "index_convertions_on_invoice_id"
  end

  create_table "domain_settings", force: :cascade do |t|
    t.integer "member_id"
    t.string "name"
    t.string "ns1"
    t.string "ns2"
    t.string "status", default: "pending"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "message"
    t.index ["member_id"], name: "index_domain_settings_on_member_id"
  end

  create_table "faqs", id: :serial, force: :cascade do |t|
    t.string "payment"
    t.string "shipping"
    t.string "refund_and_exchange"
    t.string "faq"
    t.integer "faqable_id"
    t.string "faqable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "galleries", id: :serial, force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.string "file_file_name"
    t.string "file_content_type"
    t.integer "file_file_size"
    t.datetime "file_updated_at"
    t.string "galleriable_type"
    t.integer "galleriable_id"
    t.integer "position"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deleted_at"], name: "index_galleries_on_deleted_at"
    t.index ["galleriable_id"], name: "index_galleries_on_galleriable_id"
    t.index ["galleriable_type"], name: "index_galleries_on_galleriable_type"
  end

  create_table "group_messages", force: :cascade do |t|
    t.integer "member_id"
    t.integer "account_manager_id"
    t.string "status", default: "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_manager_id"], name: "index_group_messages_on_account_manager_id"
    t.index ["member_id"], name: "index_group_messages_on_member_id"
  end

  create_table "guest_books", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "phone"
    t.text "description"
    t.boolean "status", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["user_id"], name: "index_guest_books_on_user_id"
  end

  create_table "impressions", id: :serial, force: :cascade do |t|
    t.string "impressionable_type"
    t.integer "impressionable_id"
    t.integer "user_id"
    t.string "controller_name"
    t.string "action_name"
    t.string "view_name"
    t.string "request_hash"
    t.string "ip_address"
    t.string "session_hash"
    t.text "message"
    t.text "referrer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "params"
    t.index ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index"
    t.index ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index"
    t.index ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index"
    t.index ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index"
    t.index ["impressionable_type", "impressionable_id", "params"], name: "poly_params_request_index"
    t.index ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index"
    t.index ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index"
    t.index ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index"
    t.index ["user_id"], name: "index_impressions_on_user_id"
  end

  create_table "inquiries", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "name"
    t.string "email"
    t.string "phone"
    t.text "message"
    t.string "inquiriable_type"
    t.integer "inquiriable_id"
    t.boolean "status", default: false
    t.string "ancestry"
    t.integer "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_id"], name: "index_inquiries_on_owner_id"
    t.index ["user_id"], name: "index_inquiries_on_user_id"
  end

  create_table "invoices", force: :cascade do |t|
    t.integer "user_id"
    t.string "code"
    t.string "subject"
    t.string "description"
    t.decimal "price", default: "0.0"
    t.string "uniq_code"
    t.string "bank_name"
    t.string "bank_branch"
    t.string "account_name"
    t.string "account_number"
    t.date "period_start"
    t.date "period_end"
    t.string "status", default: "pending"
    t.string "invoiceable_type"
    t.integer "invoiceable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "expired_date"
    t.boolean "promo", default: false
    t.boolean "already_send", default: false
    t.index ["invoiceable_id"], name: "index_invoices_on_invoiceable_id"
    t.index ["invoiceable_type"], name: "index_invoices_on_invoiceable_type"
    t.index ["user_id"], name: "index_invoices_on_user_id"
  end

  create_table "landing_pages", id: :serial, force: :cascade do |t|
    t.string "title"
    t.string "slug"
    t.text "description"
    t.boolean "status", default: false
    t.string "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "landing_press_galleries", force: :cascade do |t|
    t.integer "landing_press_id"
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.string "title"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["landing_press_id"], name: "index_landing_press_galleries_on_landing_press_id"
  end

  create_table "landing_press_products", force: :cascade do |t|
    t.integer "landing_press_id"
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.string "title"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["landing_press_id"], name: "index_landing_press_products_on_landing_press_id"
  end

  create_table "landing_press_testimonials", force: :cascade do |t|
    t.integer "landing_press_id"
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.string "title"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["landing_press_id"], name: "index_landing_press_testimonials_on_landing_press_id"
  end

  create_table "landing_presses", force: :cascade do |t|
    t.string "banner_file_name"
    t.string "banner_content_type"
    t.integer "banner_file_size"
    t.datetime "banner_updated_at"
    t.string "title1"
    t.string "description1"
    t.string "image1_file_name"
    t.string "image1_content_type"
    t.integer "image1_file_size"
    t.datetime "image1_updated_at"
    t.string "image2_file_name"
    t.string "image2_content_type"
    t.integer "image2_file_size"
    t.datetime "image2_updated_at"
    t.string "image3_file_name"
    t.string "image3_content_type"
    t.integer "image3_file_size"
    t.datetime "image3_updated_at"
    t.string "title2"
    t.string "description2"
    t.string "image4_file_name"
    t.string "image4_content_type"
    t.integer "image4_file_size"
    t.datetime "image4_updated_at"
    t.string "image5_file_name"
    t.string "image5_content_type"
    t.integer "image5_file_size"
    t.datetime "image5_updated_at"
    t.string "title3"
    t.string "description3"
    t.string "title_banner"
    t.string "title4"
    t.string "title_banner2"
    t.string "description4"
    t.string "image6_file_name"
    t.string "image6_content_type"
    t.integer "image6_file_size"
    t.datetime "image6_updated_at"
    t.string "title5"
    t.string "description5"
    t.string "price"
    t.string "telephone"
    t.string "footer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code"
    t.string "category"
    t.string "link_video"
    t.string "screen_capture_file_name"
    t.string "screen_capture_content_type"
    t.integer "screen_capture_file_size"
    t.datetime "screen_capture_updated_at"
    t.boolean "status", default: false
    t.string "code_css"
    t.string "brand"
    t.integer "product_id"
  end

  create_table "merchant_infos", id: :serial, force: :cascade do |t|
    t.integer "merchant_id"
    t.string "title"
    t.string "banner_file_name"
    t.string "banner_content_type"
    t.integer "banner_file_size"
    t.datetime "banner_updated_at"
    t.text "description"
    t.text "policies"
    t.text "payment"
    t.text "shipping"
    t.text "refund_and_exchange"
    t.text "faq"
    t.string "product_type"
    t.string "no_bpom"
    t.integer "distribution_stock"
    t.string "sample_product"
    t.string "sample_product_image_file_name"
    t.string "sample_product_image_content_type"
    t.integer "sample_product_image_file_size"
    t.datetime "sample_product_image_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "available_cod", default: false
    t.index ["merchant_id"], name: "index_merchant_infos_on_merchant_id"
  end

  create_table "messages", force: :cascade do |t|
    t.integer "group_message_id"
    t.integer "from"
    t.integer "to"
    t.string "message"
    t.string "status", default: "unread"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_message_id"], name: "index_messages_on_group_message_id"
  end

  create_table "order_items", id: :serial, force: :cascade do |t|
    t.string "no_invoice"
    t.integer "catalog_id"
    t.integer "product_id"
    t.integer "order_id"
    t.integer "member_id"
    t.integer "account_manager_id"
    t.integer "merchant_id"
    t.integer "stakeholder_order_id"
    t.decimal "price", default: "0.0"
    t.decimal "basic_price", default: "0.0"
    t.decimal "company_price", default: "0.0"
    t.integer "quantity"
    t.decimal "total_price", default: "0.0"
    t.decimal "total_basic_price", default: "0.0"
    t.decimal "total_company_price", default: "0.0"
    t.integer "weight"
    t.decimal "discount", default: "0.0"
    t.string "variant"
    t.string "message"
    t.string "status", default: "pending"
    t.string "cancelled_reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_manager_id"], name: "index_order_items_on_account_manager_id"
    t.index ["catalog_id"], name: "index_order_items_on_catalog_id"
    t.index ["member_id"], name: "index_order_items_on_member_id"
    t.index ["merchant_id"], name: "index_order_items_on_merchant_id"
    t.index ["order_id"], name: "index_order_items_on_order_id"
    t.index ["product_id"], name: "index_order_items_on_product_id"
    t.index ["stakeholder_order_id"], name: "index_order_items_on_stakeholder_order_id"
  end

  create_table "orders", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.integer "account_manager_id"
    t.string "slug"
    t.string "code"
    t.string "payment_token"
    t.string "payer_name"
    t.string "payer_email"
    t.string "payer_address"
    t.string "payer_phone"
    t.string "payment_method"
    t.string "shipping_method"
    t.string "payment_to"
    t.decimal "price", default: "0.0"
    t.decimal "basic_price", default: "0.0"
    t.decimal "company_price", default: "0.0"
    t.decimal "shipping_price", default: "0.0"
    t.decimal "total_price", default: "0.0"
    t.decimal "total_basic_price", default: "0.0"
    t.decimal "total_company_price", default: "0.0"
    t.string "track_order", default: "verification"
    t.date "purchased_at"
    t.integer "order_status_id"
    t.string "number_shipping"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "uniq_code"
    t.date "due_date"
    t.string "origin_code"
    t.string "origin_name"
    t.string "courier"
    t.index ["account_manager_id"], name: "index_orders_on_account_manager_id"
    t.index ["member_id"], name: "index_orders_on_member_id"
  end

  create_table "packages", force: :cascade do |t|
    t.string "title"
    t.string "package_type"
    t.integer "period"
    t.decimal "price", default: "0.0"
    t.text "description"
    t.string "status", default: "inactive"
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payment_merchants", force: :cascade do |t|
    t.string "code"
    t.integer "merchant_id"
    t.integer "admin_id"
    t.date "payment_start"
    t.date "payment_end"
    t.decimal "price", default: "0.0"
    t.decimal "ongkir", default: "0.0"
    t.decimal "total", default: "0.0"
    t.string "from_acc"
    t.string "to_acc"
    t.string "status", default: "pending"
    t.string "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["admin_id"], name: "index_payment_merchants_on_admin_id"
    t.index ["merchant_id"], name: "index_payment_merchants_on_merchant_id"
  end

  create_table "payment_referrals", force: :cascade do |t|
    t.string "code"
    t.integer "admin_id"
    t.integer "member_id"
    t.date "payment_start"
    t.date "payment_end"
    t.decimal "commission_referral", default: "0.0"
    t.string "from_acc"
    t.string "to_acc"
    t.string "message"
    t.boolean "payment_pending", default: true
    t.string "quote"
    t.string "status", default: "pending"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "total_delay"
    t.index ["admin_id"], name: "index_payment_referrals_on_admin_id"
    t.index ["member_id"], name: "index_payment_referrals_on_member_id"
  end

  create_table "payment_snapshots", force: :cascade do |t|
    t.string "code"
    t.integer "admin_id"
    t.string "user_type"
    t.integer "user_id"
    t.date "payment_start"
    t.date "payment_end"
    t.decimal "fee_am", default: "0.0"
    t.decimal "fee_member", default: "0.0"
    t.decimal "fee_referral", default: "0.0"
    t.decimal "fee_company", default: "0.0"
    t.decimal "total", default: "0.0"
    t.decimal "price", default: "0.0"
    t.string "category"
    t.string "message"
    t.string "from_acc"
    t.string "to_acc"
    t.string "status", default: "pending"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["admin_id"], name: "index_payment_snapshots_on_admin_id"
    t.index ["user_id"], name: "index_payment_snapshots_on_user_id"
  end

  create_table "payments", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "logo_file_name"
    t.string "logo_content_type"
    t.integer "logo_file_size"
    t.datetime "logo_updated_at"
    t.string "bank_name"
    t.string "account_number"
    t.integer "paymentable_id"
    t.string "paymentable_type"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_specs", id: :serial, force: :cascade do |t|
    t.integer "product_id"
    t.integer "brand_id"
    t.text "information"
    t.text "material"
    t.integer "stock", default: 0
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["brand_id"], name: "index_product_specs_on_brand_id"
    t.index ["deleted_at"], name: "index_product_specs_on_deleted_at"
    t.index ["product_id"], name: "index_product_specs_on_product_id"
  end

  create_table "product_stocks", force: :cascade do |t|
    t.integer "product_id"
    t.string "variant"
    t.integer "stock", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_product_stocks_on_product_id"
  end

  create_table "product_testimonials", force: :cascade do |t|
    t.integer "product_id"
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_product_testimonials_on_product_id"
  end

  create_table "products", id: :serial, force: :cascade do |t|
    t.string "code"
    t.string "slug"
    t.integer "merchant_id"
    t.integer "category_id"
    t.integer "sub_category_id"
    t.string "brand_name"
    t.string "title"
    t.decimal "basic_price", default: "0.0"
    t.decimal "price", default: "0.0"
    t.decimal "recommended_price", default: "0.0"
    t.text "description"
    t.boolean "status", default: false
    t.boolean "featured", default: false
    t.string "status_stock"
    t.date "start_at"
    t.date "end_at"
    t.decimal "weight", default: "0.0"
    t.string "cover_file_name"
    t.string "cover_content_type"
    t.integer "cover_file_size"
    t.datetime "cover_updated_at"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "link_video"
    t.boolean "no_referral", default: false
    t.index ["brand_name"], name: "index_products_on_brand_name"
    t.index ["category_id"], name: "index_products_on_category_id"
    t.index ["deleted_at"], name: "index_products_on_deleted_at"
    t.index ["merchant_id"], name: "index_products_on_merchant_id"
    t.index ["sub_category_id"], name: "index_products_on_sub_category_id"
  end

  create_table "profiles", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "full_name"
    t.date "birthday"
    t.string "gender"
    t.string "phone"
    t.string "no_ktp"
    t.string "no_npwp"
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.integer "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string "meta_title"
    t.string "meta_keywords"
    t.text "meta_description"
    t.string "bank_name"
    t.string "bank_branch"
    t.string "account_name"
    t.string "account_number"
    t.string "cover_file_name"
    t.string "cover_content_type"
    t.integer "cover_file_size"
    t.datetime "cover_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_profiles_on_user_id"
  end

  create_table "regions", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "ancestry"
    t.boolean "featured", default: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "report_refunds", force: :cascade do |t|
    t.integer "order_id"
    t.integer "quantity"
    t.decimal "refund_value", default: "0.0"
    t.decimal "ongkir", default: "0.0"
    t.decimal "total", default: "0.0"
    t.string "status", default: "pending"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "member_id"
    t.integer "account_manager_id"
    t.string "from_acc"
    t.string "to_acc"
    t.index ["account_manager_id"], name: "index_report_refunds_on_account_manager_id"
    t.index ["member_id"], name: "index_report_refunds_on_member_id"
    t.index ["order_id"], name: "index_report_refunds_on_order_id"
    t.index ["quantity"], name: "index_report_refunds_on_quantity"
  end

  create_table "request_landing_presses", force: :cascade do |t|
    t.string "code"
    t.integer "member_id"
    t.integer "landing_press_id"
    t.string "price"
    t.string "contact"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "meta_title"
    t.string "meta_keywords"
    t.string "link_wa"
    t.boolean "use_link_wa", default: false
    t.index ["landing_press_id"], name: "index_request_landing_presses_on_landing_press_id"
    t.index ["member_id"], name: "index_request_landing_presses_on_member_id"
  end

  create_table "shipments", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "province"
    t.string "city"
    t.string "state"
    t.string "postal_code"
    t.text "address"
    t.float "weight"
    t.decimal "price"
    t.string "shipmentable_type"
    t.string "shipmentable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shipping_methods", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "logo_file_name"
    t.string "logo_content_type"
    t.integer "logo_file_size"
    t.datetime "logo_updated_at"
    t.boolean "status"
    t.string "shipping_methodable_type"
    t.integer "shipping_methodable_id"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "site_infos", force: :cascade do |t|
    t.integer "member_id"
    t.integer "package_id"
    t.integer "web_template_id"
    t.boolean "buy_domain"
    t.string "domain_name"
    t.boolean "buy_logo"
    t.string "logo_title"
    t.decimal "discount_global", default: "0.0"
    t.string "keywords_1"
    t.string "keywords_2"
    t.string "keywords_3"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["member_id"], name: "index_site_infos_on_member_id"
    t.index ["package_id"], name: "index_site_infos_on_package_id"
    t.index ["web_template_id"], name: "index_site_infos_on_web_template_id"
  end

  create_table "spendings", force: :cascade do |t|
    t.integer "admin_id"
    t.string "code"
    t.string "title"
    t.string "description"
    t.decimal "price"
    t.date "transaction_date"
    t.string "from_acc"
    t.string "to_acc"
    t.string "file_file_name"
    t.string "file_content_type"
    t.integer "file_file_size"
    t.datetime "file_updated_at"
    t.string "status", default: "pending"
    t.string "category", default: "other"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["admin_id"], name: "index_spendings_on_admin_id"
  end

  create_table "stakeholder_orders", id: :serial, force: :cascade do |t|
    t.string "no_invoice"
    t.integer "order_id"
    t.integer "merchant_id"
    t.integer "member_id"
    t.integer "account_manager_id"
    t.integer "weight"
    t.decimal "price"
    t.decimal "basic_price", default: "0.0"
    t.decimal "company_price", default: "0.0"
    t.decimal "shipping_price", default: "0.0"
    t.decimal "total_price", default: "0.0"
    t.decimal "total_basic_price", default: "0.0"
    t.decimal "total_company_price", default: "0.0"
    t.boolean "payment_status", default: false
    t.string "track_order", default: "verification"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_manager_id"], name: "index_stakeholder_orders_on_account_manager_id"
    t.index ["member_id"], name: "index_stakeholder_orders_on_member_id"
    t.index ["merchant_id"], name: "index_stakeholder_orders_on_merchant_id"
    t.index ["order_id"], name: "index_stakeholder_orders_on_order_id"
  end

  create_table "subscribes", id: :serial, force: :cascade do |t|
    t.string "email"
    t.boolean "status", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "testimonials", id: :serial, force: :cascade do |t|
    t.text "message"
    t.boolean "status", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "address"
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.string "code"
    t.string "username"
    t.string "provider"
    t.string "uid"
    t.string "slug"
    t.boolean "featured", default: false
    t.boolean "verified", default: false
    t.string "type"
    t.integer "package_id"
    t.integer "account_manager_id"
    t.integer "ref_id1"
    t.integer "ref_id2"
    t.integer "ref_id3"
    t.integer "user_fee", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "membership", default: false
    t.string "role_access"
    t.boolean "status_payment", default: false
    t.boolean "accepted", default: false
    t.index ["account_manager_id"], name: "index_users_on_account_manager_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["package_id"], name: "index_users_on_package_id"
    t.index ["ref_id1"], name: "index_users_on_ref_id1"
    t.index ["ref_id2"], name: "index_users_on_ref_id2"
    t.index ["ref_id3"], name: "index_users_on_ref_id3"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  create_table "votes", id: :serial, force: :cascade do |t|
    t.string "votable_type"
    t.integer "votable_id"
    t.string "voter_type"
    t.integer "voter_id"
    t.boolean "vote_flag"
    t.string "vote_scope"
    t.integer "vote_weight"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope"
    t.index ["votable_type", "votable_id"], name: "index_votes_on_votable_type_and_votable_id"
    t.index ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope"
    t.index ["voter_type", "voter_id"], name: "index_votes_on_voter_type_and_voter_id"
  end

  create_table "web_settings", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "title"
    t.text "description"
    t.text "keywords"
    t.text "header_tags"
    t.text "footer_tags"
    t.string "contact"
    t.string "email"
    t.string "favicon_file_name"
    t.string "favicon_content_type"
    t.integer "favicon_file_size"
    t.datetime "favicon_updated_at"
    t.string "logo_file_name"
    t.string "logo_content_type"
    t.integer "logo_file_size"
    t.datetime "logo_updated_at"
    t.string "logo_banner_file_name"
    t.string "logo_banner_content_type"
    t.integer "logo_banner_file_size"
    t.datetime "logo_banner_updated_at"
    t.string "facebook"
    t.string "instagram"
    t.string "robot"
    t.string "author"
    t.string "corpyright"
    t.string "revisit"
    t.string "expires"
    t.string "revisit_after"
    t.string "geo_placename"
    t.string "language"
    t.string "country"
    t.string "content_language"
    t.string "distribution"
    t.string "generator"
    t.string "rating"
    t.string "target"
    t.string "search_engines"
    t.string "address"
    t.string "longitude"
    t.string "latitude"
    t.string "fb_fixels_dashboard"
    t.string "fb_fixels_view"
    t.string "fb_fixels_cart"
    t.string "fb_fixels_checkout"
    t.string "fb_fixels_purchase"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "google_analytic"
    t.boolean "order_via_cart", default: true
    t.boolean "order_via_wa", default: true
    t.string "accepted_rules"
    t.index ["user_id"], name: "index_web_settings_on_user_id"
  end

  create_table "web_templates", force: :cascade do |t|
    t.string "name"
    t.string "status", default: "pending"
    t.string "code_css"
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
