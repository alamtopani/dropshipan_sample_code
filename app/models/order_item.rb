class OrderItem < ActiveRecord::Base
  belongs_to :catalog, foreign_key: 'catalog_id'
  belongs_to :product, foreign_key: 'product_id'
  belongs_to :order, foreign_key: 'order_id'
  belongs_to :stakeholder_order, foreign_key: 'stakeholder_order_id'
  belongs_to :member, foreign_key: 'member_id'
  belongs_to :merchant, foreign_key: 'merchant_id'
  belongs_to :account_manager

  has_one :commission

  include ScopeBased
  include TheOrder::OrderItemSearching

  scope :bonds, -> {
    eager_load(:product, :order, :member)
  }

  scope :bonds_catalog, -> {
    eager_load(:catalog, :order)
  }

  scope :in_progress, -> {where("orders.order_status_id =?", 1)}
  scope :without_in_progress, -> {where.not("orders.order_status_id =?", 1)}
  scope :verification, -> {where("orders.order_status_id =?", 2)}
  scope :paid, -> {where("orders.order_status_id =?", 3)}
  scope :cancelled, -> {where("orders.order_status_id =?", 4)}

  scope :track_pending, ->{where("orders.track_order =?", OrderItem::PENDING)}
  scope :track_verification, ->{where("orders.track_order =?", OrderItem::VERIFICATION)}
  scope :track_packing, ->{where("orders.track_order =?", OrderItem::PACKING)}
  scope :track_verification_packing, ->{where("orders.track_order =? OR orders.track_order =?", OrderItem::VERIFICATION, OrderItem::PACKING)}
  scope :track_shipped, ->{where("orders.track_order =?", OrderItem::SHIPPED)}
  scope :track_received, ->{where("orders.track_order =?", OrderItem::RECEVIED)}
  scope :track_cancelled, ->{where("orders.track_order =?", OrderItem::CANCELLED)}

  scope :all_pending, ->{where("order_items.status =?", OrderItem::PENDING)}
  scope :all_verification, ->{where("order_items.status =?", OrderItem::VERIFICATION)}
  scope :all_packing, ->{where("order_items.status =?", OrderItem::PACKING)}
  scope :all_shipped, ->{where("order_items.status =?", OrderItem::SHIPPED)}
  scope :all_received, ->{where("order_items.status =?", OrderItem::RECEVIED)}
  scope :all_refund, ->{where("order_items.status =?", OrderItem::REFUND)}
  scope :all_cancelled, ->{where("order_items.status =?", OrderItem::CANCELLED)}
  scope :not_refund_and_cancelled, ->{where.not("order_items.status =?", OrderItem::REFUND).where.not("order_items.status =?", OrderItem::CANCELLED)}

  before_create :finalize

  PENDING = 'pending'
  VERIFICATION = 'verification'
  PACKING = 'packing'
  SHIPPED = 'shipped'
  RECEVIED = 'received'
  REFUND = 'refund'
  CANCELLED = 'cancelled'

  STATUSES = [OrderItem::PENDING, OrderItem::VERIFICATION, OrderItem::PACKING, OrderItem::SHIPPED, OrderItem::RECEVIED, OrderItem::REFUND, OrderItem::CANCELLED]

  def status?
    return '<span class="label label-default">Pending</span>'.html_safe if self.status == OrderItem::PENDING
    return '<span class="label label-warning">Pemeriksaan</span>'.html_safe if self.status == OrderItem::VERIFICATION
    return '<span class="label label-info">Packing</span>'.html_safe if self.status == OrderItem::PACKING
    return '<span class="label label-primary">Dikirim</span>'.html_safe if self.status == OrderItem::SHIPPED
    return '<span class="label label-success">Diterima</span>'.html_safe if self.status == OrderItem::RECEVIED
    return '<span class="label label-danger">Dibatalkan</span>'.html_safe if self.status == OrderItem::CANCELLED
    return '<span class="label label-danger">Refund</span>'.html_safe if self.status == OrderItem::REFUND
  end

  def status_text?
    return 'Pending'.html_safe if self.status == OrderItem::PENDING
    return 'Pemeriksaan'.html_safe if self.status == OrderItem::VERIFICATION
    return 'Packing'.html_safe if self.status == OrderItem::PACKING
    return 'Dikirim'.html_safe if self.status == OrderItem::SHIPPED
    return 'Diterima'.html_safe if self.status == OrderItem::RECEVIED
    return 'Dibatalkan'.html_safe if self.status == OrderItem::CANCELLED
    return 'Refund'.html_safe if self.status == OrderItem::REFUND
  end

  def self.check_status_text(status)
    return 'Pending'.html_safe if status == OrderItem::PENDING
    return 'Pemeriksaan'.html_safe if status == OrderItem::VERIFICATION
    return 'Packing'.html_safe if status == OrderItem::PACKING
    return 'Dikirim'.html_safe if status == OrderItem::SHIPPED
    return 'Diterima'.html_safe if status == OrderItem::RECEVIED
    return 'Dibatalkan'.html_safe if status == OrderItem::CANCELLED
    return 'Refund'.html_safe if status == OrderItem::REFUND
  end

  def payment_status?
    return '<span class="label label-primary">Diproses</span>'.html_safe if self.order.order_status_id == 1
    return '<span class="label label-warning">Pemeriksaan</span>'.html_safe if self.order.order_status_id == 2
    return '<span class="label label-success">Sudah Dibayar</span>'.html_safe if self.order.order_status_id == 3
    return '<span class="label label-danger">Dibatalkan</span>'.html_safe if self.order.order_status_id == 4
  end

  def confirmation?
    confirmation = Confirmation.where("LOWER(confirmations.no_invoice) LIKE LOWER('#{self.order.code}')").first
    if confirmation.present?
      return '<span class="label label-success">OK</span>'.html_safe
    else
      return '<span class="label label-danger">NO</span>'.html_safe
    end
  end

  def cancelled_reason?
    if self.cancelled? || self.refund?
      return "<span class='label label-default'>#{self.cancelled_reason}</span>".html_safe
    end
  end
  
  #------------------- GET PRICE IN ORDER ITEM -------------------
  def price
    if persisted?
      self[:price]
    else
      product.recommended_price
    end
  end

  def basic_price
    if persisted?
      self[:basic_price]
    else
      product.basic_price
    end
  end

  def company_price
    if persisted?
      self[:company_price]
    else
      product.price
    end
  end

  #------------------- TOTAL QUANTITY + PRICE IN ORDER ITEM -------------------
  def total_price?
    if User.current.present? && User.current.member?
      company_price * quantity
    else
      price * quantity
    end
  end

  def total_basic_price?
    basic_price * quantity
  end

  def total_company_price?
    company_price * quantity
  end

  def get_merchant
    get_merchant = Merchant.find self.merchant_id
  end

  #------------------- GET MERCHANT -------------------
  def merchant
    product.merchant.id
  end

  #------------------- QUANTITY REDUCED -------------------
  def quantity_reduced
    product = self.product
    product_spec = product.product_spec
    product_spec.stock = product_spec.stock - self.quantity
    if product_spec.save
      if product.product_stocks.available.present?
        stock = product.product_stock(self.variant)
        product_stock = product.product_stocks.find_by(variant: self.variant)
        product_stock.stock = stock - self.quantity
        product_stock.save
      end
    end
  end

  #------------------- QUANTITY CANCEL REDUCED -------------------
  def quantity_cancellation
    product = self.product
    product_spec = product.product_spec
    product_spec.stock = product_spec.stock + self.quantity
    if product_spec.save
      if product.product_stocks.available.present?
        stock = product.product_stock(self.variant)
        product_stock = product.product_stocks.find_by(variant: self.variant)
        product_stock.stock = stock + self.quantity
        product_stock.save
      end
    end
  end

  #------------------- CHECK QUANTITY -------------------
  def check_quantity
    self.product.product_spec.stock
  end

  #------------------- STATUS -------------------
  def cancelled?
    self.status == OrderItem::CANCELLED
  end

  def refund?
    self.status == OrderItem::REFUND
  end

  #------------------- CHECK STOCKS -------------------
  def product_stock_available?
    self.product.present? && self.product.ready_stock? && self.product.product_stocks.present? && self.product.product_stocks.available.present?
  end

  def product_stock?
    self.product.product_stocks.find_by(variant: self.variant).stock rescue 0
  end

  #------------------- ADD COMMISION -------------------
  def add_commision
    total_basic_price = self.total_basic_price.to_f
    total_company_price = self.total_company_price.to_f
    commission_member = (self.total_price.to_f - total_company_price)
    commission_am = (total_company_price * (self.member.account_manager.user_fee.to_i/100.to_f)).to_f
    commission_company = ((total_company_price - total_basic_price) - commission_am.to_f).to_f

    commission = Commission.create({
      member_id: self.member_id,
      account_manager_id: self.member.account_manager_id,
      product_id: self.product_id,
      catalog_id: self.catalog_id,
      order_id: self.order_id,
      order_item_id: self.id,
      commission_member: commission_member.to_f,
      commission_company: commission_company.to_f,
      commission_am: commission_am.to_f,
      status: Commission::APPROVED,
      quantity: self.quantity,
      commission_date: self.order.created_at
    })

    if self.product.include_referral?
      if commission.member.ref_id1.present?
        ref_id1 = Member.find(commission.member.ref_id1)

        if ref_id1.present?
          commission_1 = CommissionReferral.create({
            member_id: ref_id1.id, commission_id: commission.id, commission_value: (total_company_price * User::FEE_REF1), status: Commission::APPROVED, commission_date: commission.commission_date, percentage: User::FEE_REF1*100, position_ref: 'ref1'
          })
        end

        if ref_id1.ref_id1.present?
          commission_2 = CommissionReferral.create({
            member_id: ref_id1.ref_id1, commission_id: commission.id, commission_value: (total_company_price * User::FEE_REF2), status: Commission::APPROVED, commission_date: commission.commission_date, percentage: User::FEE_REF2*100, position_ref: 'ref2'
          })
        end

        if ref_id1.ref_id2.present?
          commission_3 = CommissionReferral.create({
            member_id: ref_id1.ref_id2, commission_id: commission.id, commission_value: (total_company_price * User::FEE_REF3), status: Commission::APPROVED, commission_date: commission.commission_date, percentage: User::FEE_REF3*100, position_ref: 'ref3'
          })
        end

        commission_1 = commission_1.present? ? commission_1.commission_value.to_f : 0
        commission_2 = commission_2.present? ? commission_2.commission_value.to_f : 0
        commission_3 = commission_3.present? ? commission_3.commission_value.to_f : 0
        commission.commission_company = (commission_company - (commission_1.to_f + commission_2.to_f + commission_3.to_f))
        commission.save
      end
    end
  end

  private 
    def finalize
      self.merchant_id = merchant
      self[:price] = self.catalog.present? ? self.catalog.get_price? : price
      self[:basic_price] = basic_price
      self[:company_price] = company_price
      self[:total_price] = quantity * self[:price]
      self[:total_basic_price] = quantity * self[:basic_price]
      self[:total_company_price] = quantity * self[:company_price]
      self.weight = self.product.weight * quantity
      discount = self.catalog.present? && self.catalog.discount > 0 ? (self.catalog.price - (self.catalog.price * self.catalog.discount/100)) : 0
      self.discount = discount * quantity
    end
end
