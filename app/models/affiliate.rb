class Affiliate < ApplicationRecord

	before_create :prepare_code

	belongs_to :member, optional: true
	belongs_to :member_registration, class_name: "Member", optional: true

	scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :pending, -> { where(status: "pending") }
  scope :process, -> { where(status: "process") }
  scope :paid, -> { where(status: "reject") }
  scope :rejects, -> { where(status: "paid") }

  scope :bonds, -> {
    eager_load(:member)
  }

  include TheAffiliate::AffiliateSearching

	AFFILIATE_BASIC_PRICE = 688000
	AFFILIATE_SALE_PRICE = 1415000
	AFFILIATE_FEE_MEMBER = 450000
	AFFILIATE_FEE_COMPANY = 277000
	AFFILIATE_FEE = 450000

	STATUSES = ["pending","process","reject","paid"]

	def status?
    return '<span class="label label-warning">pending</span>'.html_safe if self.status == "pending"
    return '<span class="label label-info">process</span>'.html_safe if self.status == "process"
    return '<span class="label label-danger">reject</span>'.html_safe if self.status == "reject"
    return '<span class="label label-success">paid</span>'.html_safe if self.status == "paid"  
  end

  def member_referral?
    ["#{self.try(:member_registration).try(:username)}", "(#{self.try(:member_registration_id)})"].select(&:'present?').join(' ')
  end

  def payment_to?
    ["#{self.try(:member).try(:profile).try(:bank_name)},", "#{self.try(:member).try(:profile).try(:account_name)}", "#{self.try(:member).try(:profile).try(:account_number)}"].select(&:'present?').join(' ')
  end

	def prepare_code
		self.code = SecureRandom.hex(6) if self.code.blank?
	end

end
