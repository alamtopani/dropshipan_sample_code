class CommissionReferral < ApplicationRecord
  belongs_to :commission
  belongs_to :member

  scope :latest, -> {order(created_at: :desc)}
  scope :oldest, -> {order(created_at: :asc)}
  scope :all_approved, -> {where("commission_referrals.status =?", CommissionReferral::APPROVED)}
  scope :all_pending, -> {where("commission_referrals.status =?", CommissionReferral::PENDING)}
  scope :all_reject, -> {where("commission_referrals.status =?", CommissionReferral::REJECT)}
  scope :bonds, -> {
    eager_load(:member, commission: [:order, :catalog, :product])
  }

  include TheCommissionReferral::CommissionReferralSearching

  # ------- STATUS --------
  PENDING = 'pending'.freeze
  APPROVED = 'approved'.freeze
  REJECT = 'reject'.freeze

  STATUSES = [["Pemeriksaan", CommissionReferral::PENDING], ["Disetujui", CommissionReferral::APPROVED], ["Dibatalkan", CommissionReferral::REJECT]].freeze

  def status?
    return '<span class="label label-warning">Pemeriksaan</span>'.html_safe if self.status == CommissionReferral::PENDING
    return '<span class="label label-success">Disetujui</span>'.html_safe if self.status == CommissionReferral::APPROVED
    return '<span class="label label-danger">Dibatalkan</span>'.html_safe if self.status == CommissionReferral::REJECT
  end
end
