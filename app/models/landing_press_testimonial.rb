class LandingPressTestimonial < ApplicationRecord
	belongs_to :landing_press

	has_attached_file :image,
                    styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :image, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }
end
