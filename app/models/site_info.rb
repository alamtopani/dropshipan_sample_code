class SiteInfo < ActiveRecord::Base
	belongs_to :member, foreign_key: 'user_id'
  belongs_to :web_template

  before_save :before_save_service

  private
    def before_save_service
      self.domain_name = self.domain_name.downcase if self.domain_name.present?
    end
end
