class LandingPage < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]
  
  include ScopeBased

  scope :activated, ->{where(status: true)}
  scope :about_us, ->{where(category: 'about_us')}
  scope :buy, ->{where(category: 'buy')}
  scope :sell, ->{where(category: 'sell')}
  scope :help, ->{where(category: 'help')}
end
