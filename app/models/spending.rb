class Spending < ApplicationRecord
	scope :oldest, ->{order(created_at: :asc)}
	scope :latest, ->{order(created_at: :desc)}
  scope :pending, ->{where("spendings.status =?", self::PENDING)}
  scope :process, ->{where("spendings.status =?", self::PROCESS)}
  scope :paid, ->{where("spendings.status =?", self::PAID)}
  scope :rejects, ->{where("spendings.status =?", self::REJECT)}

	has_attached_file :file, styles: { 
                      medium:   '256x256>', 
                      small:    '128x128>',
                      thumb:    '64x64>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :file, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

  include TheSpending::SpendingSearching

  # STATUS
  PENDING = 'pending'.freeze
  PROCESS = 'process'.freeze
  PAID = 'paid'.freeze
  REJECT = 'reject'.freeze

  STATUSES = [self::PENDING, self::PROCESS, self::PAID, self::REJECT].freeze

  # CATEGORIES

  OTHER = 'other'.freeze
  EXPLOYEE_SALARY = 'exployee salary'.freeze
  MONTHLY_BILL= 'monthly bill'.freeze

  CATEGORY = [self::OTHER, self::EXPLOYEE_SALARY, self::MONTHLY_BILL].freeze

  def status?
    return '<span class="label label-warning">pending</span>'.html_safe if self.status == Spending::PENDING
    return '<span class="label label-info">process</span>'.html_safe if self.status == Spending::PROCESS
    return '<span class="label label-success">paid</span>'.html_safe if self.status == Spending::PAID
    return '<span class="label label-danger">reject</span>'.html_safe if self.status == Spending::REJECT
  end
  
end
