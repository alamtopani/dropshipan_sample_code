class Category < ActiveRecord::Base
	include Tree
	extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  has_many :category_products, class_name: "Category", foreign_key: 'category_id'
  has_many :sub_category_products, class_name: "Category", foreign_key: 'sub_category_id'

	include ScopeBased
  default_scope {order(name: :asc)}
  
	scope :alfa, -> {order("name ASC")}
  scope :active, ->{where(status: true)}
  scope :only_roots, ->{where(ancestry: nil)}
  scope :only_childrens, ->{where.not(ancestry: nil).where(ancestry: Category.active.only_roots.pluck(:id))}

  has_attached_file :file, styles: { 
                      medium:   '256x256>', 
                      small:    '128x128>',
                      thumb:    '64x64>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :file, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

  def active?
    if self.status == true
      return "<label class='label label-success'>Active</label>".html_safe
    else
      return "<label class='label label-danger'>Inactive</label>".html_safe
    end
  end

  def link_download?
    if self.link_download.present?
      return '<a href="#{self.link_download}" target="_blank"><button class="btn btn-xs btn-default"><i class="fa fa-cloud-download"></i></button></a>'.html_safe
    else
      "-"
    end
  end
end
