class Address < ActiveRecord::Base
	belongs_to :addressable, polymorphic: true

  def place_city_name_merchant
    self.city.split(':')[1]
  end

  def place_city_code_merchant
    self.city.split(':')[0]
  end

  def place_info_merchant
    [address, postcode, district.split(':')[1], city.split(':')[1], province.split(':')[1]].select(&:'present?').join(', ')
  end

  def place_info
    [address, postcode, city, province].select(&:'present?').join(', ')
  end

  def place_info_with_district
    [address, postcode, district, city, province].select(&:'present?').join(', ')
  end
end
