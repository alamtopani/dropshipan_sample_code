class Activity < ApplicationRecord
  belongs_to :activitiable, polymorphic: true
  belongs_to :user
  belongs_to :order

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :unread, ->{where(status: "unread")}

  scope :bonds, ->{eager_load(:user)}

  include TheActivity::ActivitySearching

  # ----- STATUSES -----
  READ = 'read'
  UNREAD = 'unread'

  STATUSES = [Activity::READ, Activity::UNREAD]

  # ------- RELATIONS --------

  def admin
    admin = Admin.find_by(id: self.user_id)
  end

  def account_manager
    account_manager = AccountManager.find_by(id: self.user_id)
  end

  def member
    member = Member.find_by(id: self.user_id)
  end

  def order
    if self.activitiable_type == "Order"
      order = Order.find_by(id: self.activitiable_id)
    end
  end

  def invoice
    if self.activitiable_type == "Invoice"
      invoice = Invoice.find_by(id: self.activitiable_id)
    end
  end

  def payment_snapshot
    if self.activitiable_type == "PaymentSnapshot"
      payment_snapshot = PaymentSnapshot.find_by(id: self.activitiable_id)
    end
  end

  def payment_merchant
    if self.activitiable_type == "PaymentMerchant"
      payment_merchant = PaymentMerchant.find_by(id: self.activitiable_id)
    end
  end

  def payment_referral
    if self.activitiable_type == "PaymentReferral"
      payment_snapshot = PaymentReferral.find_by(id: self.activitiable_id)
    end
  end
end
