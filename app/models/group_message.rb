class GroupMessage < ApplicationRecord
  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :active, ->{where(status: "active")}
  scope :nonactive, ->{where(status: "nonactive")}

  has_many :messages
  
end
