class AccountManager < User
  extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]

  default_scope {where(type: 'AccountManager')}

  has_many :commissions, foreign_key: 'account_manager_id'
  has_many :orders, foreign_key: 'account_manager_id'
  has_many :order_items, foreign_key: 'account_manager_id'
  has_many :stakeholder_orders, foreign_key: 'account_manager_id'
  has_many :members, foreign_key: 'account_manager_id'
  has_many :catalogs, foreign_key: 'account_manager_id'
  has_many :confirmations, foreign_key: 'account_manager_id'
  has_many :payment_snapshots, foreign_key: 'user_id'
  has_many :report_refunds, foreign_key: 'account_manager_id'
  
  before_create :confirmation_email

  private
    def confirmation_email
      self.confirmation_token = nil
      self.confirmed_at = Time.zone.now
    end

end