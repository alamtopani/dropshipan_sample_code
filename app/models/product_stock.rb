class ProductStock < ApplicationRecord
  belongs_to :product

  include ScopeBased
  default_scope {order(variant: :asc)}

  scope :available, -> {where("product_stocks.stock >?", 0)}
end
