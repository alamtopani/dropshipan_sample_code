class Package < ApplicationRecord
  scope :latest, -> {order(created_at: :desc)}
  scope :oldest, -> {order(created_at: :asc)}
  scope :active, -> {where(status: 'active')}

  has_attached_file :image, styles: { 
                      large:    '512x512>', 
                      medium:   '256x256>', 
                      small:    '128x128>',
                      thumb:    '64x64>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :image, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

  include ThePackage::PackageSearching

  has_many :users
  has_many :members
  has_many :invoices

  # ------- STATUS --------
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze

  STATUSES = [self::ACTIVE, self::INACTIVE].freeze
  
  def status?
    return '<span class="label label-success">Active</span>'.html_safe if self.status == Package::ACTIVE
    return '<span class="label label-danger">Inactive</span>'.html_safe if self.status == Package::INACTIVE
  end

  def premium?
    self.package_type == "Premium"
  end

  def advance?
    self.package_type == "Advance"
  end

  def premium_or_advance?
    self.package_type == "Premium" || self.package_type == "Advance"
  end
end
