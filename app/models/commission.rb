class Commission < ApplicationRecord

  scope :latest, -> {order(created_at: :desc)}
  scope :oldest, -> {order(created_at: :asc)}

  belongs_to :account_manager
  belongs_to :member
  belongs_to :order
  belongs_to :order_item
  belongs_to :catalog
  belongs_to :product

  has_many :commission_referrals

  include TheCommission::CommissionSearching

  # ------- STATUS --------
  PENDING = 'pending'.freeze
  REJECT = 'reject'.freeze
  APPROVED = 'approved'.freeze

  STATUSES = [["Pemeriksaan", Commission::PENDING], ["Disetujui", Commission::APPROVED], ["Dibatalkan", Commission::REJECT]].freeze

  scope :all_pending, -> {where("commissions.status =?", Commission::PENDING)}
  scope :all_reject, -> {where("commissions.status =?", Commission::REJECT)}
  scope :all_approved, -> {where("commissions.status =?", Commission::APPROVED)}
  scope :bonds, -> {
    eager_load(:member, :order, :catalog, :product)
  }

  before_create :before_create_service

  def status?
    return '<span class="label label-warning">Pemeriksaan</span>'.html_safe if self.status == Commission::PENDING
    return '<span class="label label-success">Disetujui</span>'.html_safe if self.status == Commission::APPROVED
    return '<span class="label label-danger">Dibatalkan</span>'.html_safe if self.status == Commission::REJECT
  end

  private
    def before_create_service
      self.uniq_code = 'K'+ 5.times.map{Random.rand(10)}.join
    end

end
