class Shipment < ActiveRecord::Base
  include ScopeBased
  belongs_to :shipmentable, polymorphic: true

  scope :alfa_province, ->{order(province: :asc)}
  scope :alfa_city, ->{order(city: :asc)}
  scope :alfa_state, ->{order(state: :asc)}

  include TheOrder::ShipmentSearching

  def place_info
    [address, postal_code, state, city, province].select(&:'present?').join(', ')
  end

  def self.import(file, shipping)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)

    (4..spreadsheet.last_row).each do |i|
      shipment = Shipment.new
      shipment.name = shipping
      shipment.province = spreadsheet.row(i)[0]
      shipment.city = spreadsheet.row(i)[1]
      shipment.state = spreadsheet.row(i)[2]
      shipment.price = spreadsheet.row(i)[3]
      shipment.save
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
     when '.csv' then Roo::Csv.new(file.path)
     when '.xls' then Roo::Excel.new(file.path)
     when '.xlsx' then Roo::Excelx.new(file.path)
     else raise "Unknown file type: #{file.original_filename}"
    end
  end
end
