class TrackOrder < EnumerateIt::Base
  associate_values(
    :verification   => ['verification', 'Pemeriksaan'],  
    :shipped        => ['shipped', 'Dikirim'],
    :received       => ['received', 'Diterima'],
    :cancelled      => ['cancelled', 'Dibatalkan']
  )
end