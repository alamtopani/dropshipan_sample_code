class Payment < ActiveRecord::Base
  belongs_to :paymentable, polymorphic: true
  default_scope { order('payments.position ASC') }

  has_attached_file :logo,
                    styles: {
                      large: '1000>',
                      medium:'500>',
                      small: '300>',
                      thumb: '150>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :logo, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }
end
