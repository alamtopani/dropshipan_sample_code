class Convertion < ApplicationRecord
  belongs_to :invoice

  # ------- STATUS --------
  APPROVED = 'approved'.freeze
  REJECT = 'reject'.freeze
  VISITED = 'visited'.freeze

  def status?
    if self.status == ConvertionReport::APPROVED
      return '<i class="pe-7s-check" data-toggle="tooltip" title="Approved"></i>'.html_safe
    elsif self.status == ConvertionReport::REJECT
      return '<i class="pe-7s-attention" data-toggle="tooltip" title="Reject"></i>'.html_safe
    elsif self.status == ConvertionReport::VISITED
      return ''.html_safe
    end
  end
end
