class ShippingMethod < ActiveRecord::Base
  belongs_to :shipping_methodable, polymorphic: true

  default_scope { order('shipping_methods.position ASC') }
  scope :activated, -> {where(status: true)}

  has_attached_file :logo,
                    styles: {
                      large: '1000>',
                      medium:'500>',
                      small: '300>',
                      thumb: '150>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :logo, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  # associate_values(
  #   :jne    => ['JNE - Express Across Nations', 'jne.png'],
  #   :tiki    => ['TIKI - Hanya Satu Titipan Kilat', 'tiki.png'],
  #   :pos    => ['POS - Pos Indonesia', 'pos.png']
  # )
end