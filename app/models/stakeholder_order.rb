class StakeholderOrder < ActiveRecord::Base
  belongs_to :order, foreign_key: 'order_id'
  belongs_to :member, foreign_key: 'member_id'
  belongs_to :merchant, foreign_key: 'merchant_id'
  belongs_to :account_manager

  has_many :order_items, foreign_key: 'stakeholder_order_id', dependent: :destroy
  accepts_nested_attributes_for :order_items, reject_if: :all_blank, allow_destroy: true

  scope :paid_success, -> {where(payment_status: true)}
  scope :in_progress, -> {where("orders.order_status_id =?", 1)}
  scope :without_in_progress, -> {where.not("orders.order_status_id =?", 1)}
  scope :verification, -> {where("orders.order_status_id =?", 2)}
  scope :paid, -> {where("orders.order_status_id =?", 3)}
  scope :cancelled, -> {where("orders.order_status_id =?", 4)}

  scope :track_pending, ->{where("orders.track_order =?", OrderItem::PENDING)}
  scope :track_verification, ->{where("orders.track_order =?", OrderItem::VERIFICATION)}
  scope :track_packing, ->{where("orders.track_order =?", OrderItem::PACKING)}
  scope :track_verification_packing, ->{where("orders.track_order =? OR orders.track_order =?", OrderItem::VERIFICATION, OrderItem::PACKING)}
  scope :track_shipped, ->{where("orders.track_order =?", OrderItem::SHIPPED)}
  scope :track_received, ->{where("orders.track_order =?", OrderItem::RECEVIED)}
  scope :track_cancelled, ->{where("orders.track_order =?", OrderItem::CANCELLED)}
  scope :track_shipped_received, ->{where("orders.track_order =? OR orders.track_order =?", OrderItem::SHIPPED, OrderItem::RECEVIED)}

  include ScopeBased
  include TheOrder::StakeholderOrderSearching

  scope :bonds, -> {
    eager_load(:merchant, :member, :order)
  }

  def payment_status?
    return '<span class="label label-primary">Diproses</span>'.html_safe if self.order.order_status_id == 1
    return '<span class="label label-warning">Pemeriksaan</span>'.html_safe if self.order.order_status_id == 2
    return '<span class="label label-success">Sudah Dibayar</span>'.html_safe if self.order.order_status_id == 3
    return '<span class="label label-danger">Dibatalkan</span>'.html_safe if self.order.order_status_id == 4
  end

  def status?
    if self.order.cod?
      if self.track_order == Order::VERIFICATION || self.track_order == Order::PACKING || self.track_order == Order::CANCELLED
        self.order.courier?
      else
        return '<span class="label label-warning">Pemeriksaan</span>'.html_safe if self.track_order == Order::VERIFICATION
        return '<span class="label label-info">Packing</span>'.html_safe if self.track_order == Order::PACKING
        return '<span class="label label-primary">Dikirim</span>'.html_safe if self.track_order == Order::SHIPPED
        return '<span class="label label-success">Diterima</span>'.html_safe if self.track_order == Order::RECEIVED
        return '<span class="label label-danger">Dibatalkan</span>'.html_safe if self.track_order == Order::CANCELLED
      end
    else
      return '<span class="label label-warning">Pemeriksaan</span>'.html_safe if self.track_order == Order::VERIFICATION
      return '<span class="label label-info">Packing</span>'.html_safe if self.track_order == Order::PACKING
      return '<span class="label label-primary">Dikirim</span>'.html_safe if self.track_order == Order::SHIPPED
      return '<span class="label label-success">Diterima</span>'.html_safe if self.track_order == Order::RECEIVED
      return '<span class="label label-danger">Dibatalkan</span>'.html_safe if self.track_order == Order::CANCELLED
    end
  end

  def confirmation?
    confirmation = Confirmation.where("LOWER(confirmations.no_invoice) LIKE LOWER('#{self.order.code}')").first
    if confirmation.present?
      return '<span class="label label-success">OK</span>'.html_safe
    else
      return '<span class="label label-danger">NO</span>'.html_safe
    end
  end

  def quantity?
    if self.order_items.present?
      quantity = self.order_items.not_refund_and_cancelled.pluck(:quantity).sum.to_i
    end
  end

  #------------------- GET TOTAL WEIGHT -------------------
  def weight?
    if self.order_items.present?
      weight = self.order_items.not_refund_and_cancelled.pluck(:weight).sum.to_i
    end
  end

  def total_weight?
    if self.order_items.present?
      weight = self.order_items.not_refund_and_cancelled.pluck(:weight).sum.to_i
      if weight < 1
        total = 1
      else
        total = weight
      end
    end
  end

  #------------------- GET TOTAL PRICE -------------------
  def total?
    if User.current.present? && User.current.member?
      order_items.not_refund_and_cancelled.collect { |oi| oi.valid? ? (oi.quantity * oi.company_price) : 0 }.sum
    else
      order_items.not_refund_and_cancelled.collect { |oi| oi.valid? ? (oi.quantity * oi.price) : 0 }.sum
    end
  end

  def basic_price?
    order_items.not_refund_and_cancelled.collect { |oi| oi.valid? ? (oi.quantity * oi.basic_price) : 0 }.sum
  end

  def company_price?
    order_items.not_refund_and_cancelled.collect { |oi| oi.valid? ? (oi.quantity * oi.company_price) : 0 }.sum
  end

end
