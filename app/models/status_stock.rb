class StatusStock < EnumerateIt::Base
  associate_values(
    :pre_order      => ['pre_order', 'PRE ORDER'],  
    :always_ready   => ['always_ready', 'STOK SELALU TERSEDIA'], 
    :ready_stock    => ['ready_stock', 'READY STOK']
  )
end