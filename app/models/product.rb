class Product < ActiveRecord::Base
	extend FriendlyId
  friendly_id :code, use: [:slugged, :finders]

  is_impressionable
  acts_as_votable
  acts_as_paranoid
  after_initialize :after_initialized, :populate_galleries

  has_one :landing_presses

  scope :active, -> {where("products.status =?", true)}

  has_attached_file :cover,
                    styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :cover, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  PRE_ORDER = 'pre_order'
  ALWAYS_READY = 'always_ready'
  READY_STOCK = 'ready_stock'

  include TheProduct::ProductScope
  include TheProduct::ProductConfig
  include TheProduct::ProductSearching
  include ScopeBased

  # ------- STATUS --------
  STATUSES = [["Activated", true], ["Non activated", false]].freeze

  # ------- FEATURED --------
  FEATURED = [["Featured", true], ["Non featured", false]].freeze

  # ------- FEE_REFERRAL --------
  FEE_REFERRAL = [["Include Fee Referral", false], ["No Fee Referral", true]].freeze
  
  private
    def after_initialized
      self.product_spec ||= ProductSpec.new if self.product_spec.blank?
    end

    def populate_galleries
      if self.galleries.length < 6
        [
          'Cover',
          'Image1',
          'Image2',
          'Image3',
          'Image4',
          'Image5',
        ].each_with_index do |media_title, index|
          _galery = self.galleries.select{|g| g.title.to_s.downcase == media_title.downcase}.first
          unless _galery
            self.build_image(media_title, index+1)
          else
            _galery.position = index+1
          end
        end
      end if self.new_record?
    end
end