class PaymentStatus < EnumerateIt::Base
  associate_values(
    :verification   => [2, 'Pemeriksaan'],  
    :in_progress    => [1, 'Diproses'], 
    :paid           => [3, 'Sudah Dibayar'],   
    :cancelled      => [4, 'Dibatalkan']
  )
end