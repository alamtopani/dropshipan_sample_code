module TheVisit
  module VisitSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_start_at(_start_at)
        return if _start_at.blank?
        where("DATE(started_at) >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("DATE(started_at) <=?", _end_at.to_date)
      end

      def search_by(options={})
        results = all

        if options[:commit_day].present?
          @date = Time.zone.now.to_date

          if options[:commit_day] == 'last_7_day'
            start_at = @date - 6.days
            end_at = @date
          elsif options[:commit_day] == 'yesterday'
            start_at = @date - 1.days
            end_at = start_at
          elsif options[:commit_day] == 'today'
            start_at = @date
            end_at = @date
          end

          options[:start_at] = start_at
          options[:end_at] = end_at
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        return results
      end

    end
  end
end
