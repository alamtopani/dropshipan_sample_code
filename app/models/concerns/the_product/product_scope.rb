module TheProduct
	module ProductScope
		extend ActiveSupport::Concern

		included do
			validates_uniqueness_of :code

			has_many :catalogs, foreign_key: 'product_id'
			has_many :commissions
			
			belongs_to :merchant, foreign_key: 'merchant_id'
		  belongs_to :parent_category, class_name: 'Category', foreign_key: 'category_id'
		  belongs_to :sub_category, class_name: 'Category', foreign_key: 'sub_category_id'

		  has_one :product_spec, foreign_key: 'product_id'
		  accepts_nested_attributes_for :product_spec

		  has_many :product_stocks
		  accepts_nested_attributes_for :product_stocks, reject_if: :all_blank, allow_destroy: true

			has_many :comments, as: :commentable, dependent: :destroy
			accepts_nested_attributes_for :comments, reject_if: :all_blank, allow_destroy: true

      has_many :galleries, as: :galleriable, dependent: :destroy
      accepts_nested_attributes_for :galleries, reject_if: :all_blank, allow_destroy: true

		  has_many :product_testimonials, dependent: :destroy
		  accepts_nested_attributes_for :product_testimonials, reject_if: :all_blank, allow_destroy: true

		  has_many :order_items

			scope :latest, -> {order(created_at: :desc)}
			scope :oldest, -> {order(created_at: :asc)}

		  scope :low_to_high, -> {order("products.price ASC")}
		  scope :high_to_low, -> {order("products.price DESC")}

		  scope :last_day, -> {where('products.created_at >= :last_days_ago', :last_days_ago  => Time.now - 1.days)}
		  scope :last_week, -> {where('products.created_at >= :last_weeks_ago', :last_weeks_ago  => Time.now - 7.days)}
		  scope :last_month, -> {where('products.created_at >= :last_months_ago', :last_months_ago  => Time.now - 30.days)}
		  scope :last_year, -> {where('products.created_at >= :last_years_ago', :last_years_ago  => Time.now - 360.days)}

		  scope :verify, -> {where("products.status = ?", true)}
		  scope :unverify, -> {where("products.status = ?", false)}
		  scope :featured, -> {where("products.featured = ?", true)}

		  scope :bonds, -> {
		  	eager_load({ merchant: [:profile, :address]}, :parent_category, :sub_category)
		  }

		end
	end
end
