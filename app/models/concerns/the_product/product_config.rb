module TheProduct
  module ProductConfig
    extend ActiveSupport::Concern

    def pre_order?
      self.status_stock == Product::PRE_ORDER
    end

    def always_ready?
      self.status_stock == Product::ALWAYS_READY
    end

    def ready_stock?
      self.status_stock == Product::READY_STOCK
    end

    def have_stock?
      self.product_spec.stock > 0
    end

    def not_have_stock?
      self.product_spec.stock < 1
    end

    def product_stock_available?
      self.ready_stock? && self.product_stocks.present? && self.product_stocks.available.present?
    end

    def product_stock(variant)
      self.product_stocks.find_by(variant: variant).stock rescue 0
    end

    def status_stock_label?
      if self.pre_order?
        return '<span class="status preorder">PREORDER</span><span class="stock">+ 1 Minggu</span>'.html_safe
      elsif self.always_ready?
        return "<span class='status'>SELALU ADA</span><span class='stock'>Tersedia</span>".html_safe
      else
        if self.product_spec.stock > 0
          return "<span class='status'>READY STOK</span><span class='stock'>#{self.product_spec.stock} Tersedia</span>".html_safe
        else
          return '<span class="status out-stock">STOK HABIS</span>'.html_safe
        end
      end
    end
    
    def only_status_stock?
      if self.pre_order?
        return '<span class="status preorder">PREORDER</span>'.html_safe
      elsif self.always_ready?
        return "<span class='status'>SELALU ADA</span>".html_safe
      else
        if self.product_spec.stock > 0
          return "<span class='status'>READY STOK</span>".html_safe
        else
          return '<span class="status out-stock">STOK HABIS</span>'.html_safe
        end
      end
    end

    def active?
      if self.status == true
        return "<label class='label label-success'><i class='fa fa-check'></i> Aktif</label>".html_safe
      else
        return "<label class='label label-danger'><i class='fa fa-exclamation'></i> Belum Aktif</label>".html_safe
      end
    end

    def active_text?
      if self.status == true
        return "Aktif"
      else
        return "Tidak Aktif"
      end
    end

    def featured?
      if self.featured == true
        return "[Featured]".html_safe
      end
    end

    def status_stock?
      if self.pre_order?
        return '<span class="">PreOrder</span>'.html_safe
      elsif self.always_ready?
        return "<span class=''>Selalu Tersedia</span>".html_safe
      else
        if self.product_spec.stock > 0
          return "<span class=''>#{self.product_spec.stock} Tersedia</span>".html_safe
        else
          return '<span class="">Habis</span>'.html_safe
        end
      end
    end

    def have_sold?
      self.order_items.bonds.paid.pluck(:quantity).sum || 0
    end

    def build_image(media_title, position=nil)
      gallery = self.galleries.build({title: media_title})
      gallery.position = position
    end

    def include_referral?
      self.no_referral == false
    end

    def fee_referral?
      if self.no_referral == false
        return "<div class='label label-success'><i class='fa fa-check'></i> Ya</div>".html_safe
      else
        return "<div class='label label-danger'><i class='fa fa-exclamation'></i> Tidak</div>".html_safe
      end
    end

    def fee_referral_text?
      if self.no_referral == false
        return "<div class='label label-success'>Include Fee Referral</div>".html_safe
      else
        return "<div class='label label-danger'>No Fee Referral</div>".html_safe
      end
    end
  end
end
