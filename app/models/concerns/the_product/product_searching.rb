module TheProduct
  module ProductSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(products.code) LIKE LOWER(:key)",
          "LOWER(products.title) LIKE LOWER(:key)",
          "LOWER(products.brand_name) LIKE LOWER(:key)",
          "LOWER(users.username) LIKE LOWER(:key)",
          "LOWER(users.email) LIKE LOWER(:key)",
          "LOWER(profiles.full_name) LIKE LOWER(:key)"
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_category(_category)
        return if _category.blank?
        where("LOWER(categories.name) LIKE LOWER(:key)", key: "%#{_category}%")
      end

      def by_sub_category(_sub_category)
        return if _sub_category.blank?
        where("products.sub_category_id =?", _sub_category)
      end

      def by_category_id(_category_id)
        return if _category_id.blank?
        where("products.category_id =?", _category_id)
      end

      def by_sub_category_id(_sub_category_id)
        return if _sub_category_id.blank?
        where("products.sub_category_id =?", _sub_category_id)
      end

      def by_province(_province)
        return if _province.blank?
        where("addresses.city =?", _province)
      end

      def by_status_stock(_status_stock)
        return if _status_stock.blank?
        where("products.status_stock =?", _status_stock)
      end

      def by_status(_status)
        return if _status.blank?
        where("products.status =?", _status)
      end

      def by_featured(_featured)
        return if _featured.blank?
        where("products.featured =?", _featured)
      end

      def sort_by_order(_sort)
        return if _sort.blank?
        self.send(_sort)
      end

      def price_range(_price_range)
        return if _price_range.blank?
        price = _price_range.split(";")
        where("products.price >=?", price[0].to_i).where("products.price <=?", price[1].to_i)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where("DATE(products.created_at) >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("DATE(products.created_at) <=?", _end_at.to_date)
      end

      def by_fee_referral(_fee_referral)
        return if _fee_referral.blank?
        where("products.no_referral =?", _fee_referral)
      end

      def search_by(options={})
      
        results = bonds

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:category].present?
          results = results.by_category(options[:category])
        end

        if options[:sub_category].present?
          category_id = Category.find_by(name: options[:sub_category]).id
          results = results.by_sub_category(category_id)
        end

        if options[:category_id].present?
          results = results.by_category_id(options[:category_id])
        end

        if options[:sub_category_id].present?
          results = results.by_sub_category_id(options[:sub_category_id])
        end

        if options[:province].present?
          results = results.by_province(options[:province])
        end

        if options[:status_stock].present?
          results = results.by_status_stock(options[:status_stock])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:featured].present?
          results = results.by_featured(options[:featured])
        end

        if options[:sort_by].present?
          results = results.sort_by_order(options[:sort_by])
        end

        if options[:price_range].present?
          results = results.price_range(options[:price_range])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:fee_referral].present?
          results = results.by_fee_referral(options[:fee_referral])
        end

        return results
      end

    end
  end
end
