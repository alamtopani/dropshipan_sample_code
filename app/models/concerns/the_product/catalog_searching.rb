module TheProduct
  module CatalogSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(products.code) LIKE LOWER(:key)",
          "LOWER(products.title) LIKE LOWER(:key)",
          "LOWER(products.brand_name) LIKE LOWER(:key)",
          "LOWER(catalogs.code) LIKE LOWER(:key)",
          "LOWER(users.username) LIKE LOWER(:key)",
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_status(_status)
        return if _status.blank?
        where("catalogs.status =?", _status)
      end

      def by_category(_category)
        return if _category.blank?
        where("LOWER(categories.name) LIKE LOWER(:key)", key: "%#{_category}%")
      end

      def by_sub_category(_sub_category)
        return if _sub_category.blank?
        where("products.sub_category_id =?", _sub_category)
      end

      def by_category_id(_category_id)
        return if _category_id.blank?
        where("products.category_id =?", _category_id)
      end

      def by_sub_category_id(_sub_category_id)
        return if _sub_category_id.blank?
        where("products.sub_category_id =?", _sub_category_id)
      end

      def by_province(_province)
        return if _province.blank?
        where("addresses.city =?", _province)
      end

      def by_status_stock(_status_stock)
        return if _status_stock.blank?
        where("products.status_stock =?", _status_stock)
      end

      def by_status(_status)
        return if _status.blank?
        where("catalogs.status =?", _status)
      end

      def sort_by_order(_sort)
        return if _sort.blank?
        self.send(_sort)
      end

      def price_range(_price_range)
        return if _price_range.blank?
        price = _price_range.split(";")
        where("catalogs.price >=?", price[0].to_i).where("catalogs.price <=?", price[1].to_i)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where("DATE(catalogs.created_at) >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("DATE(catalogs.created_at) <=?", _end_at.to_date)
      end

      def by_fee_referral(_fee_referral)
        return if _fee_referral.blank?
        where("products.no_referral =?", _fee_referral)
      end

      def search_by(options={})
      
        results = bonds

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:category].present?
          results = results.by_category(options[:category])
        end

        if options[:sub_category].present?
          category_id = Category.find_by(name: options[:sub_category]).id
          results = results.by_sub_category(category_id)
        end

        if options[:category_id].present?
          results = results.by_category_id(options[:category_id])
        end

        if options[:sub_category_id].present?
          results = results.by_sub_category_id(options[:sub_category_id])
        end

        if options[:province].present?
          results = results.by_province(options[:province])
        end

        if options[:status_stock].present?
          results = results.by_status_stock(options[:status_stock])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:sort_by].present?
          results = results.sort_by_order(options[:sort_by])
        elsif options[:sort_by].blank?
          results = results.latest
        end

        if options[:price_range].present?
          results = results.price_range(options[:price_range])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:key_catalog].present?
          results = results.by_keywords(options[:key_catalog])
        end

        if options[:status_catalog].present?
          results = results.by_status(options[:status_catalog])
        end

        if options[:category_catalog].present?
          results = results.by_category(options[:category_catalog])
        end

        if options[:start_at_catalog].present?
          results = results.by_start_at(options[:start_at_catalog])
        end

        if options[:end_at_catalog].present?
          results = results.by_end_at(options[:end_at_catalog])
        end

        if options[:fee_referral].present?
          results = results.by_fee_referral(options[:fee_referral])
        end


        return results
      end

    end
  end
end
