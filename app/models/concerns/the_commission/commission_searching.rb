module TheCommission
  module CommissionSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(commissions.uniq_code) LIKE LOWER(:key)",
          "LOWER(orders.code) LIKE LOWER(:key)",
          "LOWER(catalogs.code) LIKE LOWER(:key)",
          "LOWER(products.code) LIKE LOWER(:key)",
          "LOWER(products.title) LIKE LOWER(:key)",
          "LOWER(users.username) LIKE LOWER(:key)",
          "LOWER(users.email) LIKE LOWER(:key)"
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_price(_price)
        return if _price.blank?
        where("orders.total_price =?", _price)
      end

      def by_user_id(_user_id)
        return if _user_id.blank?
        where("commissions.member_id =? OR commissions.account_manager_id =?", _user_id, _user_id)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where("DATE(commissions.created_at) >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("DATE(commissions.created_at) <=?", _end_at.to_date)
      end

      def by_status(_status)
        return if _status.blank?
        where("commissions.status =?", _status)
      end

      def by_order_item_id(_order_item_id)
        return if _order_item_id.blank?
        where("commissions.order_item_id =?", _order_item_id)
      end

      def search_by(options={})
        results = bonds

        if options[:commit_day].present?
          @date = Time.zone.now.to_date

          if options[:commit_day] == 'last_7_day'
            start_at = @date - 6.days
            end_at = @date
          elsif options[:commit_day] == 'yesterday'
            start_at = @date - 1.days
            end_at = start_at
          elsif options[:commit_day] == 'today'
            start_at = @date
            end_at = @date
          end

          options[:start_at] = start_at
          options[:end_at] = end_at
        end

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:order_item_id].present?
          results = results.by_order_item_id(options[:order_item_id])
        end

        if options[:user_id].present?
          results = results.by_user_id(options[:user_id])
        end

        if options[:price].present?
          results = results.by_price(options[:price])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:key_commission].present?
          results = results.by_keywords(options[:key_commission])
        end

        if options[:status_commission].present?
          results = results.by_status(options[:status_commission])
        end

        if options[:user_id_commission].present?
          results = results.by_user_id(options[:user_id_commission])
        end

        if options[:start_at_commission].present?
          results = results.by_start_at(options[:start_at_commission])
        end

        if options[:end_at_commission].present?
          results = results.by_end_at(options[:end_at_commission])
        end

        return results
      end

    end
  end
end
