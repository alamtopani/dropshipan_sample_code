module TheUser
	module UserConfig
		extend ActiveSupport::Concern

		included do
			devise  :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :confirmable,
	         		:validatable, :lockable, :timeoutable, :omniauthable, omniauth_providers: [:facebook], :authentication_keys => [:login]

	    after_initialize :after_initialized

	    has_one :profile, foreign_key: 'user_id', dependent: :destroy
	    accepts_nested_attributes_for :profile, reject_if: :all_blank

	    has_one :address, as: :addressable, dependent: :destroy
	    accepts_nested_attributes_for :address, reject_if: :all_blank

	    has_one :contact, as: :contactable, dependent: :destroy
	    accepts_nested_attributes_for :contact, reject_if: :all_blank

		  has_one :subscribe, foreign_key: 'email', dependent: :destroy
		  accepts_nested_attributes_for :subscribe, reject_if: :all_blank

      has_many :invoices, foreign_key: 'user_id', dependent: :destroy
      accepts_nested_attributes_for :invoices, reject_if: :all_blank

      has_many :blogs, foreign_key: 'user_id', dependent: :destroy
      accepts_nested_attributes_for :blogs, reject_if: :all_blank

      has_many :activities, foreign_key: 'user_id', dependent: :destroy
      accepts_nested_attributes_for :activities, reject_if: :all_blank 

      has_many :guest_books, foreign_key: 'user_id'

      belongs_to :package

		  validates_uniqueness_of :username, :email
		  before_save :downcase_username, :prepare_code

		  scope :admin, ->{ where(type: 'Admin') }
		  scope :member, ->{ where(type: 'Member') }
		  scope :merchant, ->{ where(type: 'Merchant') }
      scope :verified, ->{ where(verified: true)}
		  scope :active, ->{ where(membership: true)}
      scope :not_active, ->{ where(membership: false)}
		  scope :not_verified, ->{ where(verified: false)}
		  scope :featured, ->{ where(featured: true)}
		  scope :latest, -> {order(created_at: :desc)}
			scope :oldest, -> {order(created_at: :asc)}
			scope :bonds, -> { eager_load(:profile, :address) }
			scope :beweekly, -> {where("users.status_payment =?", false)}
			scope :optional, -> {where("users.status_payment =?", true)}
		end

		# -------STATUS PAYMENT------
		STATUS_PAYMENT = [["Optional", true], ["Beweekly", false]]

    #--------------- ALL TYPE USER --------------------
		def admin?
			self.type == "Admin"
		end

		def member?
			self.type == "Member"
		end

    def merchant?
      self.type == "Merchant"
    end

		def account_manager?
			self.type == "AccountManager"
		end

    def type?
      if self.type == "Member"
        return "Member"
      elsif self.type == "AccountManager"
        return "Manager"
      else
        return "-"
      end
    end

    #--------------- ALL STATUS USER --------------------
    def verified?
      if self.confirmed?
        if self.verified == true
          return "<label class='label label-success'><i class='fa fa-check-circle-o'></i> Terverifikasi</label>".html_safe
        else
          return "<label class='label label-danger'><i class='fa fa-warning'></i> Tidak Terverifikasi</label>".html_safe
        end
      else
        return "<label class='label label-danger'><i class='fa fa-warning'></i> Email Belum di Konfirmasi</label>".html_safe
      end
    end

    def status_payment?
    	if self.status_payment == true
    		return "<label class='label label-info'><i class='fa fa-check-circle-o'></i> Pembayaran Optional</label>".html_safe
    	else
    		return "<label class='label label-default'><i class='fa fa-warning'></i> Pembayaran Beweekly</label>".html_safe
   		end
    end

		def membership?
	  	if self.membership == true
	  		return "<label class='label label-success'><i class='fa fa-check-circle-o'></i> Berbayar</label>".html_safe
		  else
		  	return "<label class='label label-danger'><i class='fa fa-warning'></i> Tidak Berbayar</label>".html_safe
		  end
	  end

	  def featured?
	  	if self.featured == true
	  		return "<i class='fa fa-check'></i> Featured".html_safe
		  else
		  	return "".html_safe
		  end
	  end

	  def featured_status
	    self.featured ? 'Featured' : 'Un Featured'
	  end

	  #--------------- ALL ACTION USER ----------------------

		def change_featured_status!
	    if self.featured
	      self.featured = false
	      self.save
	    elsif !self.featured
	      self.featured = true
	      self.save
	    end
	  end

    # ---------------- ACTIVITY CREATE ---------------------
    def self.prepare_activity(resource, user_id, title, description)
      Activity.create(user_id: user_id, title: title, description: description, activitiable_id: resource.id, activitiable_type: resource.class.name)
    end

		protected
			#--------------- GENERATE CODE ID USER ----------------------
			def prepare_code
				self.code = SecureRandom.hex(3) if self.code.blank?
			end

			def after_initialized
				self.profile = Profile.new if self.profile.blank?
				self.address = Address.new if self.address.blank?
				self.contact = Contact.new if self.contact.blank?
			end

			def downcase_username
				self.username = self.username.downcase if username_changed?
			end
	end
end
