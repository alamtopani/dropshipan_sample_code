module TheUser
  module UserSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(users.code) LIKE LOWER(:key)",
          "LOWER(users.username) LIKE LOWER(:key)",
          "LOWER(users.email) LIKE LOWER(:key)",
          "LOWER(profiles.full_name) LIKE LOWER(:key)"
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_status(_status)
        return if _status.blank?
        if _status == "Email has not confirmed"
          where(confirmed_at: nil)
        else
          where("users.verified =?", _status)
        end
      end

      def by_status_payment(_status_payment)
        return if _status_payment.blank?
        where("users.status_payment =?", _status_payment)
      end

      def by_featured(_featured)
        return if _featured.blank?
        where("users.featured =?", _featured)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where("DATE(users.created_at) >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("DATE(users.created_at) <=?", _end_at.to_date)
      end

      def search_by(options={})
        results = bonds

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:status_payment].present?
          results = results.by_status_payment(options[:status_payment])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:featured].present?
          results = results.by_featured(options[:featured])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:key_member].present?
          results = results.by_keywords(options[:key_member])
        end

        if options[:status_member].present?
          results = results.by_status(options[:status_member])
        end

        if options[:featured_member].present?
          results = results.by_featured(options[:featured_member])
        end

        if options[:start_at_member].present?
          results = results.by_start_at(options[:start_at_member])
        end

        if options[:end_at_member].present?
          results = results.by_end_at(options[:end_at_member])
        end

        return results
      end

    end

  end
end
