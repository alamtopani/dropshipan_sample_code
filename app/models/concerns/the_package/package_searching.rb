module ThePackage
  module PackageSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(packages.title) LIKE LOWER(:key)",
          "LOWER(packages.description) LIKE LOWER(:key)",
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_status(_status)
        return if _status.blank?
        where("packages.status =?", _status)
      end

      def search_by(options={})
        results = latest

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        return results
      end

    end
  end
end
