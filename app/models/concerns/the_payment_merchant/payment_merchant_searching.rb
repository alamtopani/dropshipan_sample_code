module ThePaymentMerchant
  module PaymentMerchantSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(payment_merchants.code) LIKE LOWER(:key)",
          "LOWER(users.username) LIKE LOWER(:key)"
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_status(_status)
        return if _status.blank?
        where("payment_merchants.status =?", _status)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where("DATE(payment_merchants.created_at) >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("DATE(payment_merchants.created_at) <=?", _end_at.to_date)
      end

      def search_by(options={})
        results = bonds

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:key_payment_merchant].present?
          results = results.by_keywords(options[:key_payment_merchant])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:status_payment_merchant].present?
          results = results.by_status(options[:status_payment_merchant])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:start_at_payment_merchant].present?
          results = results.by_start_at(options[:start_at_payment_merchant])
        end

        if options[:end_at_payment_merchant].present?
          results = results.by_end_at(options[:end_at_payment_merchant])
        end

        return results
      end

    end
  end
end
