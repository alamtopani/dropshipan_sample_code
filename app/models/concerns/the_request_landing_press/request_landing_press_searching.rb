module TheRequestLandingPress
  module RequestLandingPressSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(request_landing_presses.code) LIKE LOWER(:key)",
          "LOWER(request_landing_presses.price) LIKE LOWER(:key)",
          "LOWER(request_landing_presses.contact) LIKE LOWER(:key)",
          "LOWER(landing_presses.title1) LIKE LOWER(:key)"
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_category(_category)
        return if _category.blank?
        where("landing_presses.category =?", _category)
      end

      def by_brand(_brand)
        return if _brand.blank?
        where("landing_presses.brand =?", _brand)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where("DATE(request_landing_presses.created_at) >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("DATE(request_landing_presses.created_at) <=?", _end_at.to_date)
      end

      def search_by(options={})
        results = bonds

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:category].present?
          results = results.by_category(options[:category])
        end

        if options[:brand].present?
          results = results.by_brand(options[:brand])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:key_request_landing_press].present?
          results = results.by_keywords(options[:key_request_landing_press])
        end

        if options[:category_request_landing_press].present?
          results = results.by_category(options[:category_request_landing_press])
        end

        if options[:start_at_request_landing_press].present?
          results = results.by_start_at(options[:start_at_request_landing_press])
        end

        if options[:end_at_request_landing_press].present?
          results = results.by_end_at(options[:end_at_request_landing_press])
        end

        return results
      end

    end
  end
end
