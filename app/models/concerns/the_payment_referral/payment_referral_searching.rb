module ThePaymentReferral
  module PaymentReferralSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(payment_referrals.code) LIKE LOWER(:key)",
          "LOWER(users.username) LIKE LOWER(:key)"
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_status(_status)
        return if _status.blank?
        where("payment_referrals.status =?", _status)
      end

      def by_status_payment(_status)
        return if _status.blank?
        where("payment_referrals.payment_pending =?", _status)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where("DATE(payment_referrals.created_at) >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("DATE(payment_referrals.created_at) <=?", _end_at.to_date)
      end

      def search_by(options={})
        results = bonds

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:status_payment].present?
          results = results.by_status_payment(options[:status_payment])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:key_payment_referral].present?
          results = results.by_keywords(options[:key_payment_referral])
        end

        if options[:status_payment_referral].present?
          results = results.by_status(options[:status_payment_referral])
        end

        if options[:status_payment_payment_referral].present?
          results = results.by_status_payment(options[:status_payment_payment_referral])
        end

        if options[:start_at_payment_referral].present?
          results = results.by_start_at(options[:start_at_payment_referral])
        end

        if options[:end_at_payment_referral].present?
          results = results.by_end_at(options[:end_at_payment_referral])
        end

        return results
      end

    end
  end
end
