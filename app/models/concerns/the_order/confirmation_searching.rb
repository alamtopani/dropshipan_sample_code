module TheOrder
	module ConfirmationSearching
		extend ActiveSupport::Concern

		module ClassMethods
			def by_keywords(_key)
				return if _key.blank?
				query_opts = [
					"LOWER(confirmations.no_invoice) LIKE LOWER(:key)",
					"LOWER(confirmations.name) LIKE LOWER(:key)",
					"LOWER(confirmations.sender_name) LIKE LOWER(:key)",
					"LOWER(confirmations.payment_method) LIKE LOWER(:key)",
					"LOWER(confirmations.bank_account) LIKE LOWER(:key)",
					"LOWER(confirmations.from_account_number) LIKE LOWER(:key)"
				].join(' OR ')
				where(query_opts, {key: "%#{_key}%"} )
			end

			def by_price(_price)
				return if _price.blank?
				where("confirmations.nominal =?", _price)
			end

			def by_order_status(_order_status_id)
				return if _order_status_id.blank?
				where("orders.order_status_id =?", _order_status_id)
			end

			def by_start_at(_start_at)
				return if _start_at.blank?
				where("DATE(confirmations.created_at) >=?", _start_at.to_date)
			end

			def by_end_at(_end_at)
				return if _end_at.blank?
				where("DATE(confirmations.created_at) <=?", _end_at.to_date)
			end

			def search_by(options={})
				results = bonds

				if options[:key].present?
					results = results.by_keywords(options[:key])
				end

				if options[:price].present?
					results = results.by_price(options[:price])
				end

				if options[:order_status_id].present?
					results = results.by_order_status(options[:order_status_id])
				end

				if options[:start_at].present?
					results = results.by_start_at(options[:start_at])
				end

				if options[:end_at].present?
					results = results.by_end_at(options[:end_at])
				end

				if options[:key_confirmation].present?
					results = results.by_keywords(options[:key_confirmation])
				end

				if options[:price_confirmation].present?
					results = results.by_price(options[:price_confirmation])
				end

				if options[:order_status_id_confirmation].present?
					results = results.by_order_status(options[:order_status_id_confirmation])
				end

				if options[:start_at_confirmation].present?
					results = results.by_start_at(options[:start_at_confirmation])
				end

				if options[:end_at_confirmation].present?
					results = results.by_end_at(options[:end_at_confirmation])
				end

				return results
			end

		end
	end
end
