module TheOrder
	module OrderItemSearching
		extend ActiveSupport::Concern

		module ClassMethods
			def by_keywords(_key)
				return if _key.blank?
				query_opts = [
					"LOWER(order_items.no_invoice) LIKE LOWER(:key)",
					"LOWER(orders.code) LIKE LOWER(:key)",
					"LOWER(products.code) LIKE LOWER(:key)",
					"LOWER(users.username) LIKE LOWER(:key)",
					"LOWER(users.email) LIKE LOWER(:key)"
				].join(' OR ')
				where(query_opts, {key: "%#{_key}%"} )
			end

      def by_price(_price)
        return if _price.blank?
        where("orders.total_price =?", _price)
      end

      def by_order_status(_order_status_id)
        return if _order_status_id.blank?
        where("orders.order_status_id =?", _order_status_id)
      end

      def by_status(_status)
        return if _status.blank?
        where("order_items.status =?", _status)
      end

			def by_user_id(_user_id)
				return if _user_id.blank?
				where("order_items.member_id =? OR order_items.account_manager_id =?", _user_id, _user_id)
			end

			def by_start_at(_start_at)
				return if _start_at.blank?
				where("DATE(orders.purchased_at) >=?", _start_at.to_date)
			end

			def by_end_at(_end_at)
				return if _end_at.blank?
				where("DATE(orders.purchased_at) <=?", _end_at.to_date)
			end

			def search_by(options={})
				results = bonds

				if options[:key].present?
					results = results.by_keywords(options[:key])
				end

        if options[:order_status_id].present?
          results = results.by_order_status(options[:order_status_id])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

				if options[:user_id].present?
					results = results.by_user_id(options[:user_id])
				end

        if options[:price].present?
          results = results.by_price(options[:price])
        end

				if options[:start_at].present?
					results = results.by_start_at(options[:start_at])
				end

				if options[:end_at].present?
					results = results.by_end_at(options[:end_at])
				end

				return results
			end

		end
	end
end
