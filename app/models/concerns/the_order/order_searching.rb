module TheOrder
  module OrderSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(orders.code) LIKE LOWER(:key)",
          "LOWER(orders.payment_token) LIKE LOWER(:key)",
          "LOWER(orders.payer_name) LIKE LOWER(:key)",
          "LOWER(orders.payer_email) LIKE LOWER(:key)",
          "LOWER(orders.payer_phone) LIKE LOWER(:key)",
          "LOWER(users.username) LIKE LOWER(:key)",
          "LOWER(users.email) LIKE LOWER(:key)"
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_user_id(_user_id)
        return if _user_id.blank?
        where("orders.member_id =? OR orders.account_manager_id =?", _user_id, _user_id)
      end

      def by_number_shipping(_number_shipping)
        return if _number_shipping.blank?
        where("orders.number_shipping =?", _number_shipping)
      end

      def by_price(_price)
        return if _price.blank?
        where("orders.total_price =?", _price)
      end

      def by_uniq_code(_uniq_code)
        return if _uniq_code.blank?
        where("orders.uniq_code =?", _uniq_code)
      end

      def by_order_status(_order_status_id)
        return if _order_status_id.blank?
        where("orders.order_status_id =?", _order_status_id)
      end

      def by_track_order(_track_order)
        return if _track_order.blank?
        where("orders.track_order =?", _track_order)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where("DATE(orders.purchased_at) >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("DATE(orders.purchased_at) <=?", _end_at.to_date)
      end

      def by_order_courier(_order_courier)
        return if _order_courier.blank?
        where("orders.courier =?", _order_courier)
      end

      def search_by(options={})
        results = bonds

        if options[:commit_day].present?
          @date = Time.zone.now.to_date

          if options[:commit_day] == 'last_7_day'
            start_at = @date - 6.days
            end_at = @date
          elsif options[:commit_day] == 'yesterday'
            start_at = @date - 1.days
            end_at = start_at
          elsif options[:commit_day] == 'today'
            start_at = @date
            end_at = @date
          end

          options[:start_at] = start_at
          options[:end_at] = end_at
        end

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:price].present?
          results = results.by_price(options[:price])
        end

        if options[:uniq_code].present?
          results = results.by_uniq_code(options[:uniq_code])
        end

        if options[:order_status_id].present?
          results = results.by_order_status(options[:order_status_id])
        end

        if options[:user_id].present?
          results = results.by_user_id(options[:user_id])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:key_order].present?
          results = results.by_keywords(options[:key_order])
        end

        if options[:track_order_order].present?
          results = results.by_track_order(options[:track_order_order])
        end

        if options[:number_shipping_order].present?
          results = results.by_number_shipping(options[:number_shipping_order])
        end

        if options[:order_status_id_order].present?
          results = results.by_order_status(options[:order_status_id_order])
        end

        if options[:start_at_order].present?
          results = results.by_start_at(options[:start_at_order])
        end

        if options[:end_at_order].present?
          results = results.by_end_at(options[:end_at_order])
        end

        if options[:courier].present?
          results = results.by_order_courier(options[:courier])
        end

        return results
      end

    end
  end
end
