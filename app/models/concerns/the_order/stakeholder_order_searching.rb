module TheOrder
  module StakeholderOrderSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(stakeholder_orders.no_invoice) LIKE LOWER(:key)",
          "LOWER(orders.code) LIKE LOWER(:key)",
          "LOWER(users.username) LIKE LOWER(:key)",
          "LOWER(users.email) LIKE LOWER(:key)"
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_price(_price)
        return if _price.blank?
        where("orders.total_price =?", _price)
      end

      def by_order_status(_order_status_id)
        return if _order_status_id.blank?
        where("orders.order_status_id =?", _order_status_id)
      end

      def by_order_code(_order_code)
        return if _order_code.blank?
        where("orders.code =?", _order_code)
      end

      def by_user_id(_user_id)
        return if _user_id.blank?
        where("stakeholder_orders.member_id =? OR stakeholder_orders.account_manager_id =? OR stakeholder_orders.merchant_id =?", _user_id, _user_id, _user_id)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where("orders.purchased_at >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("orders.purchased_at <=?", _end_at.to_date)
      end

      def by_track_order(_track_order)
        return if _track_order.blank?
        where("orders.track_order =?", _track_order)
      end

      def search_by(options={})
        results = bonds

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

         if options[:key_order].present?
          results = results.by_keywords(options[:key_order])
        end

        if options[:order_status_id].present?
          results = results.by_order_status(options[:order_status_id])
        end

        if options[:order_status_id_order].present?
          results = results.by_order_status(options[:order_status_id_order])
        end

        if options[:order_code].present?
          results = results.by_order_code(options[:order_code])
        end

        if options[:user_id].present?
          results = results.by_user_id(options[:user_id])
        end

        if options[:price].present?
          results = results.by_price(options[:price])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:start_at_order].present?
          results = results.by_start_at(options[:start_at_order])
        end

        if options[:end_at_order].present?
          results = results.by_end_at(options[:end_at_order])
        end

        if options[:track_order_order].present?
          results = results.by_track_order(options[:track_order_order])
        end

        return results
      end

    end
  end
end
