module TheInvoice
  module InvoiceSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(invoices.subject) LIKE LOWER(:key)",
          "LOWER(invoices.description) LIKE LOWER(:key)",
          "LOWER(invoices.code) LIKE LOWER(:key)",
          "LOWER(users.username) LIKE LOWER(:key)"
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_price(_price)
        return if _price.blank?
        where("invoices.price =?", _price)
      end

      def by_click_id(_click_id)
        return if _click_id.blank?
        where("convertions.click_id =?", _click_id)
      end

      def by_click_status(_click_status)
        return if _click_status.blank?
        if _click_status == 'Have Convertion'
          where.not("convertions.invoice_id IS NULL")
        elsif _click_status == 'Blank Convertion'
          where("convertions.invoice_id IS NULL")
        end
      end

      def by_package_id(_package_id)
        return if _package_id.blank?
        where("invoices.invoiceable_id =?", _package_id)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where("DATE(invoices.created_at) >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("DATE(invoices.created_at) <=?", _end_at.to_date)
      end

      def by_status(_status)
        return if _status.blank?
        where("invoices.status =?", _status)
      end

      def search_by(options={})
        results = bonds

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:package_id].present?
          results = results.by_package_id(options[:package_id])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:price].present?
          results = results.by_price(options[:price])
        end

        if options[:click_id].present?
          results = results.by_click_id(options[:click_id])
        end

        if options[:click_status].present?
          results = results.by_click_status(options[:click_status])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:key_invoice].present?
          results = results.by_keywords(options[:key_invoice])
        end

        if options[:status_invoice].present?
          results = results.by_status(options[:status_invoice])
        end

        if options[:price_invoice].present?
          results = results.by_price(options[:price_invoice])
        end

        if options[:start_at_invoice].present?
          results = results.by_start_at(options[:start_at_invoice])
        end

        if options[:end_at_invoice].present?
          results = results.by_end_at(options[:end_at_invoice])
        end

        return results
      end

    end
  end
end
