module TheLandingPress
  module LandingPressSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(landing_presses.title1) LIKE LOWER(:key)",
          "LOWER(landing_presses.code) LIKE LOWER(:key)"
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_category(_category)
        return if _category.blank?
        where("landing_presses.category =?", _category)
      end

      def by_brand(_brand)
        return if _brand.blank?
        where("landing_presses.brand =?", _brand)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where("DATE(landing_presses.created_at) >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("DATE(landing_presses.created_at) <=?", _end_at.to_date)
      end

      def search_by(options={})
        results = latest

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:category].present?
          results = results.by_category(options[:category])
        end

        if options[:brand].present?
          results = results.by_brand(options[:brand])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        return results
      end

    end
  end
end
