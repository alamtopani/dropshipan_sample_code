class LandingPress < ApplicationRecord
  
  has_many :landing_press_galleries, dependent: :destroy
  accepts_nested_attributes_for :landing_press_galleries, reject_if: :all_blank, allow_destroy: true
  
  has_many :landing_press_products, dependent: :destroy
  accepts_nested_attributes_for :landing_press_products, reject_if: :all_blank, allow_destroy: true

  has_many :landing_press_testimonials, dependent: :destroy
  accepts_nested_attributes_for :landing_press_testimonials, reject_if: :all_blank, allow_destroy: true

  has_many :request_landing_presses, dependent: :destroy
  accepts_nested_attributes_for :request_landing_presses, reject_if: :all_blank, allow_destroy: true

  belongs_to :product

  scope :latest, ->{order(created_at: :desc)}
	scope :oldest, ->{order(created_at: :asc)}
  scope :activated, ->{where(status: true)}

  after_initialize :populate_image_products
  after_initialize :populate_image_testimonials

  before_create :prepare_code

  include TheLandingPress::LandingPressSearching

  # -------------Category---------------
  CATEGORIES = ['Kecantikan', 'Hijab']

  has_attached_file :screen_capture,
                    styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :screen_capture, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

	has_attached_file :banner,
                    styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :banner, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  has_attached_file :image1,
                    styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :image1, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  has_attached_file :image2,
                    styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :image2, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  has_attached_file :image3,
                    styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :image3, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  has_attached_file :image4,
                    styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :image4, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  has_attached_file :image5,
                    styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :image5, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  has_attached_file :image6,
                    styles: {
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :image6, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  def build_image_product(media_title, position=nil)
    gallery = self.landing_press_products.build({title: media_title})
    gallery.position = position
  end

  def build_image_testimonial(media_title, position=nil)
    gallery = self.landing_press_testimonials.build({title: media_title})
    gallery.position = position
  end

  def status?
    if self.status == true
      return "<span class='label label-success'>Aktif</span>".html_safe
    else
      return "<span class='label label-danger'>Tidak Aktif<span>".html_safe
    end
  end

  private
    def populate_image_products
      if self.landing_press_products.length < 6
        [
          'Image1',
          'Image2',
          'Image3',
          'Image4',
          'Image5',
          'Image6',
        ].each_with_index do |media_title, index|
          _galery = self.landing_press_products.select{|g| g.title.to_s.downcase == media_title.downcase}.first
          unless _galery
            self.build_image_product(media_title, index+1)
          else
            _galery.position = index+1
          end
        end
      end if self.new_record?
    end

    def populate_image_testimonials
      if self.landing_press_testimonials.length < 8
        [
          'Image1',
          'Image2',
          'Image3',
          'Image4',
          'Image5',
          'Image6',
          'Image7',
          'Image8',
        ].each_with_index do |media_title, index|
          _galery = self.landing_press_testimonials.select{|g| g.title.to_s.downcase == media_title.downcase}.first
          unless _galery
            self.build_image_testimonial(media_title, index+1)
          else
            _galery.position = index+1
          end
        end
      end if self.new_record?
    end

    def prepare_code
      self.code = SecureRandom.hex(6) if self.code.blank?
    end
end
