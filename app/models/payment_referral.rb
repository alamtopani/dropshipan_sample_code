class PaymentReferral < ApplicationRecord
  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  scope :pending, -> { where(status: self::PENDING) }
  scope :process, -> { where(status: self::PROCESS) }
  scope :paid, -> { where(status: self::PAID) }
  scope :rejects, -> { where(status: self::REJECT) }
  scope :delay, -> {where(payment_pending: true)}

  has_many :activities, as: :activitiable, dependent: :destroy
  accepts_nested_attributes_for :activities, reject_if: :all_blank
  
  belongs_to :admin
  belongs_to :member

  scope :bonds, -> {
    eager_load(:member)
  }
  
  include ThePaymentReferral::PaymentReferralSearching

  before_create :before_create_service

  ORDER_TARGET = 1000000.to_f
  QUOTE = "Pembayaran komisi referral ditunda karena total penjualan Anda tidak memenuhi target."

  # ============
  #   STATUSES
  # ============

  PENDING = 'pending'.freeze
  PROCESS = 'process'.freeze
  PAID = 'paid'.freeze
  REJECT = 'reject'.freeze

  STATUSES = [self::PENDING, self::PROCESS, self::PAID, self::REJECT].freeze

  def status?
    if self.status == "process"
      return "<label class='label label-info'>Process</label>".html_safe
    elsif self.status == "paid"
      return "<label class='label label-success'>Paid</label>".html_safe
    elsif self.status == "reject"
      return "<label class='label label-danger'>Reject</label>".html_safe
    elsif self.status == "pending"
      return "<label class='label label-warning'>Pending</label>".html_safe
    end
  end

  # ======================
  #   STATUSES PAYMENT
  # ======================

  STATUSES_PAYMENT = [["Kirim", false], ["Tunda", true]].freeze

  def status_payment?
    if self.payment_pending == false
      return "<label class='label label-info'>Kirim</label>".html_safe
    else
      return "<label class='label label-warning'>Tunda</label>".html_safe
    end
  end

  def is_pending?
    self.status == PaymentReferral::PENDING
  end

  def is_process?
    self.status == PaymentReferral::PROCESS
  end

  def is_paid?
    self.status == PaymentReferral::PAID
  end

  def is_reject?
    self.status == PaymentReferral::REJECT
  end

  def payment_period?
    return "#{ApplicationController.helpers.get_date(self.payment_start)} s.d. #{ApplicationController.helpers.get_date(self.payment_end)}"
  end

  def get_commission_referral?
    return ApplicationController.helpers.get_currency(self.commission_referral)
  end

  def self.create_payment_referral(resource, payment_start, payment_end, payment, commissions_referral)
    @payment_referral = PaymentReferral.new
    @payment_referral.admin_id = Admin.first.id
    @payment_referral.member_id = resource.id
    @payment_referral.payment_start = payment_start
    @payment_referral.payment_end = payment_end
    @payment_referral.from_acc = "(#{payment.bank_name}) #{payment.name} - #{payment.account_number}"
    @payment_referral.to_acc = resource.profile.payment_to
    @payment_referral.message = "Pembayaran referral periode #{@payment_referral.payment_period?}."
    commission_referrals = commissions_referral.where(member_id: resource.id)
    @payment_referral.commission_referral = '%.4f' % commission_referrals.sum(:commission_referral)

    orders = resource.orders.where("DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=? AND orders.order_status_id =?", payment_start, payment_end, 3)
    total_order = '%.4f' % orders.sum(:total_price)
    if total_order.to_f >= PaymentReferral::ORDER_TARGET
      @payment_referral.payment_pending = false
    else
      @payment_referral.quote = PaymentReferral::QUOTE
    end
    
    if @payment_referral.commission_referral.to_i > 0
      @payment_referral.save
      Activity.create(user_id: @payment_referral.admin_id, title: "Pembayaran referral (#{@payment_referral.member.username})", description: "Sistem secara otomatis membuat pembayaran referral periode #{@payment_referral.payment_period?} sebesar #{@payment_referral.get_commission_referral?}.", activitiable_id: @payment_referral.id, activitiable_type: @payment_referral.class.name)
    end
  end

  def count_pending_payment
    payment_referrals = PaymentReferral.where.not(id: self.id).where(member_id: self.member_id).delay.count rescue 0
  end

  private
    def before_create_service
      self.code = 'PR'+ 10.times.map{Random.rand(10)}.join
    end
end
