class ProductSpec < ActiveRecord::Base
  belongs_to :product, foreign_key: 'product_id'
  acts_as_paranoid
end
