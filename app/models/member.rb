class Member < User
  extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]

  after_initialize :prepare_info, :populate_domain
  default_scope {where(type: 'Member')}

  has_many :request_landing_presses, dependent: :destroy
  accepts_nested_attributes_for :request_landing_presses, reject_if: :all_blank, allow_destroy: true

  has_many :catalogs, foreign_key: 'member_id', dependent: :destroy
  accepts_nested_attributes_for :catalogs, reject_if: :all_blank

  has_many :orders, foreign_key: 'member_id', dependent: :destroy
  accepts_nested_attributes_for :orders, reject_if: :all_blank

  has_many :stakeholder_orders, foreign_key: 'member_id', dependent: :destroy
  accepts_nested_attributes_for :stakeholder_orders, reject_if: :all_blank

  has_many :order_items, foreign_key: 'member_id', dependent: :destroy
  accepts_nested_attributes_for :order_items, reject_if: :all_blank

  has_one :site_info, foreign_key: 'member_id', dependent: :destroy
  accepts_nested_attributes_for :site_info, reject_if: :all_blank

  has_one :web_setting, foreign_key: 'user_id', dependent: :destroy
  accepts_nested_attributes_for :web_setting, reject_if: :all_blank

  has_many :domain_settings, foreign_key: 'member_id', dependent: :destroy
  accepts_nested_attributes_for :domain_settings, reject_if: :all_blank, allow_destroy: true

  has_many :confirmations, foreign_key: 'member_id'
  has_many :commissions, foreign_key: 'member_id'
  has_many :commission_referrals, foreign_key: 'member_id'
  has_many :invoices, foreign_key: 'user_id', dependent: :destroy
  has_many :payment_snapshots, foreign_key: 'user_id'
  has_many :report_refunds, foreign_key: 'member_id'
  has_many :payment_referrals
  has_many :affiliates, foreign_key: 'member_id', dependent: :destroy


  belongs_to :package
  belongs_to :account_manager

  scope :referral1, -> (id) {where("users.ref_id1 =?", id)}
  scope :referral2, -> (id) {where("users.ref_id2 =?", id)}  
  scope :referral3, -> (id) {where("users.ref_id3 =?", id)}

  scope :bonds_member, -> { eager_load(:profile, :site_info, :domain_settings) }

  include TheUser::MemberSearching
  
  def shop_title?
    self.web_setting.title.present? ? self.web_setting.title : self.username
  end

  def domain?
    self.site_info.domain_name + '.dusbelanja.com' if self.site_info.domain_name.present?
  end
  
  def web_preview
    "http://#{self.domain?}"
  end

  def referrals
    referrals = Member.where("users.ref_id1 =? OR users.ref_id2 =? OR users.ref_id3 =?", self.id, self.id, self.id)
  end

  def position_referral(parent_id)
    if self.ref_id1 == parent_id
      return "REF 1"
    elsif self.ref_id2 == parent_id
      return "REF 2"
    elsif self.ref_id3 == parent_id
      return "REF 3"
    end
  end

  def have_sales_quantity?
    sales = self.order_items.bonds_catalog.paid.pluck(:quantity).sum rescue 0
  end

  def have_sales_price?
    sales = self.orders.paid.pluck(:total_price).sum rescue 0
  end

  def invoices_paid
    return self.invoices.paid
  end

  def ref1
    ref1 = Member.find self.ref_id1 rescue "---"
  end

  def ref2
    ref2 = Member.find self.ref_id2 rescue "---"
  end

  def ref3
    ref3 = Member.find self.ref_id3 rescue "---"
  end

  def member_commission?
    @member_commission = self.commissions.all_approved.pluck(:commission_member).sum + self.commission_referrals.all_approved.pluck(:commission_value).sum
  end

  def period_active?
    return "#{ApplicationController.helpers.get_date(self.invoices.latest.paid.first.period_start)} s.d. #{ApplicationController.helpers.get_date(self.invoices.latest.paid.first.period_end)}"
  end

  private
    def prepare_info
      self.site_info = SiteInfo.new if self.site_info.blank?
      self.web_setting = WebSetting.new if self.web_setting.blank?
    end

    def populate_domain
      if self.domain_settings.length < 1
        self.domain_settings.build
      end if self.domain_settings.length < 1
    end
end