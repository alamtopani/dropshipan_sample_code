class RequestLandingPress < ApplicationRecord
	belongs_to :member
	belongs_to :landing_press

	before_create :prepare_code

	scope :latest, ->{order(created_at: :desc)}
	scope :oldest, ->{order(created_at: :asc)}

	include TheRequestLandingPress::RequestLandingPressSearching

	scope :bonds, -> {eager_load(:landing_press)}

	def link_preview?
    @link_preview = "http://#{self.member.domain?}"
  end

	def prepare_code
		self.code = SecureRandom.hex(6) if self.code.blank?
	end
end
