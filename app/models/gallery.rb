class Gallery < ActiveRecord::Base
  belongs_to :galleriable, polymorphic: true
  acts_as_paranoid
  
  default_scope { order('galleries.position ASC') }
  include ScopeBased

  scope :image_exist, ->{where.not(file_file_name: nil)}

  has_attached_file :file,
                    # processors: [:watermark],
                    styles: {
                      # large: { :geometry => '1000>', watermark_path: "#{Rails.root}/app/assets/images/watermark.png" },
                      large:    '1000>',
                      medium:   '500>',
                      small:    '300>',
                      thumb:    '64x64>'
                    },
                    default_url: 'no-image.png'

  validates_attachment :file, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }
end
