class Admin < User
  extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]
  
  default_scope {where(type: 'Admin')}

  has_many :payment_merchants, dependent: :destroy
  has_many :payment_referrals, dependent: :destroy

  before_create :confirmation_email

  # ----- ROLE ACCESS -----
  ADMIN = 'admin'
  FINANCE = 'finance'
  CHECKER_1 = 'checker_1'
  CHECKER_2 = 'checker_2'
  CHECKER_3 = 'checker_3'
  STAFF = 'staff'
  SHIPMENT = 'shipment'
  SUPPORT = 'support'

  ROLE_ACCESS = [Admin::ADMIN, Admin::FINANCE, Admin::CHECKER_1, Admin::CHECKER_2, Admin::CHECKER_3, Admin::STAFF, Admin::SHIPMENT, Admin::SUPPORT]

  def master?
    self.role_access == Admin::ADMIN  
  end

  def finance?
    self.role_access == Admin::FINANCE  
  end

  def checker_1?
    self.role_access == Admin::CHECKER_1
  end

  def checker_2?
    self.role_access == Admin::CHECKER_2
  end

  def checker_3?
    self.role_access == Admin::CHECKER_3
  end

  def staff?
    self.role_access == Admin::STAFF
  end

  def shipment?
    self.role_access == Admin::SHIPMENT
  end

  def support?
    self.role_access == Admin::SUPPORT
  end

  private
    def confirmation_email
      self.confirmation_token = nil
      self.confirmed_at = Time.zone.now
    end
end