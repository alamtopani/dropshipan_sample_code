class GuestBook < ActiveRecord::Base
	belongs_to :user, foreign_key: 'user_id'

  scope :unread, ->{where(status: false)}
	scope :latest, ->{order(created_at: :desc)}
	scope :oldest, ->{order(created_at: :asc)}
	scope :bonds, -> {
    eager_load(:user)
  }

  include TheGuestBook::GuestBookSearching

  STATUSES = [['Read',true], ['Unread', false]]

  def status?
    return '<span class="label label-success">Read</span>'.html_safe if self.status == true
    return '<span class="label label-warning">Unread</span>'.html_safe if self.status == false
  end

end
