class Message < ApplicationRecord
  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :sort_unread, ->{order(status: :desc)}
  scope :unread, ->{where(status: "unread")}
  scope :read, ->{where(status: "read")}

  belongs_to :group_message

  # ------- STATUS --------
  READ = 'read'.freeze
  UNREAD = 'unread'.freeze

  STATUSES = [self::READ, self::UNREAD].freeze

  def status?
    if self.status == "read"
      return "<label class='label label-success'>Read</label>".html_safe
    else
      return "<label class='label label-danger'>Unread</label>".html_safe
    end
  end

  def user
    user = User.find_by(id: self.from)
  end

  def user_to
    user_to = User.find_by(id: self.to)
  end

  def has_readed?
    if self.status == "read"
      return "<li style='background: none;'>".html_safe
    else
      return "<li>".html_safe
    end
  end
end
