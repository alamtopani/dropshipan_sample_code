class Gender < EnumerateIt::Base
  associate_values(
    :male   => ['Laki-Laki', 'Laki-Laki'],  
    :female    => ['Perempuan', 'Perempuan']
  )
end
