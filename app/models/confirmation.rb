class Confirmation < ActiveRecord::Base
  before_create :prepare_code
  extend FriendlyId
  friendly_id :code, use: [:slugged, :finders]

  belongs_to :member, foreign_key: 'member_id'
  belongs_to :order
  belongs_to :account_manager

  scope :bonds, -> {
    eager_load(:order)
  }

  has_attached_file :file,
                    styles: {
                      medium:   '500>',
                    },
                    default_url: 'no-image.png'

  validates_attachment :file, content_type: {
    content_type: ["image/jpg", "image/jpeg", "image/png"]
  }

  include ScopeBased
  include TheOrder::ConfirmationSearching

  def payment_to
    ["#{self.from_bank_name} An.", "#{self.sender_name}", "#{self.from_account_number}"].select(&:'present?').join(' ')
  end

  private
    def prepare_code
      self.code = SecureRandom.hex(6) if self.code.blank?
    end
end
