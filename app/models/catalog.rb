class Catalog < ApplicationRecord
  is_impressionable

  has_many :order_items
  has_many :commissions
  
  belongs_to :product, foreign_key: 'product_id'
  belongs_to :member, foreign_key: 'member_id'

  scope :latest, -> {order(created_at: :desc)}
  scope :oldest, -> {order(created_at: :asc)}
  scope :pending, -> {where("catalogs.status = ?", Catalog::PENDING)}
  scope :inactive, -> {where("catalogs.status = ?", Catalog::INACTIVE)}
  scope :rejected, -> {where("catalogs.status = ?", Catalog::REJECTED)}
  scope :approved, -> {where("catalogs.status = ?", Catalog::APPROVED)}

  scope :verify, -> {where("products.status = ?", true)}
  scope :unverify, -> {where("products.status = ?", false)}
  scope :featured, -> {where("products.featured = ?", true)}

  scope :low_to_high, -> {order("catalogs.price ASC")}
  scope :high_to_low, -> {order("catalogs.price DESC")}

  after_create :generate_code

  # ----- STATUSES -----
  PENDING = 'pending'
  INACTIVE = 'inactive'
  REJECTED = 'rejected'
  APPROVED = 'approved'

  STATUSES = [Catalog::PENDING, Catalog::INACTIVE, Catalog::REJECTED, Catalog::APPROVED]

  scope :bonds, -> {
    eager_load({ product: [:parent_category, :sub_category]}, :member)
  }

  include TheProduct::CatalogSearching

  def status_label?
    return '<span class="label label-warning">Pemeriksaan</span>'.html_safe if self.status == Catalog::PENDING
    return '<span class="label label-success">Aktif</span>'.html_safe if self.status == Catalog::APPROVED
    return '<span class="label label-danger">Dibatalkan</span>'.html_safe if self.status == Catalog::REJECTED
    return '<span class="label label-danger">Tidak Aktif</span>'.html_safe if self.status == Catalog::INACTIVE
  end

  def get_price?
    if User.current.present? && User.current.member?
      price = self.product.price.to_f
    else
      if self.discount > 0
        price = self.price.to_f - (self.price.to_f * self.discount.to_f/100).to_f
      else
        price = self.price.to_f
      end
    end
  end

  def get_price_in_list_member?
    if self.discount > 0
      price = self.price.to_f - (self.price.to_f * self.discount.to_f/100).to_f
    else
      price = self.price.to_f
    end
  end

  def status?
    if self.status == "approved"
      return "<i class='fa fa-check'></i>Published".html_safe
    else
      return "<i class='fa fa-exclamation'></i>Not Published".html_safe
    end
  end

  def is_active?
    self.status == "approved"
  end

  def is_inactive?
    self.status == "inactive"
  end

  def have_sold?
    self.order_items.bonds_catalog.paid.pluck(:quantity).sum || 0
  end

  private
    def generate_code
      self.code = [self.product.code, self.id].select(&:'present?').join('-')
      self.save
    end
end
