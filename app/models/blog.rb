class Blog < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  is_impressionable

	scope :latest, -> {order(created_at: :desc)}
	scope :oldest, -> {order(created_at: :asc)}
  scope :active, -> {where("blogs.status =?", "active")}
  scope :rejects, -> {where("blogs.status =?", "reject")}
  scope :tutorial, -> {where("blogs.category =?", self::TUTORIAL)}
  scope :tips, -> {where("blogs.category =?", self::TIPSTRICK)}

	has_attached_file :file, styles: { 
                      large:    '1000>',
                      medium:   '256x256>', 
                      small:    '128x128>',
                      thumb:    '64x64>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :file, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

  include TheBlog::BlogSearching

  TIPSTRICK = "Tips & Trick"
  TUTORIAL = "Tutorial"
  CATEGORIES = [self::TUTORIAL, self::TIPSTRICK]

  STATUSES = ['active','reject']

  def status?
    return '<span class="label label-success">active</span>'.html_safe if self.status == "active"
    return '<span class="label label-danger">reject</span>'.html_safe if self.status == "reject"
  end
end
