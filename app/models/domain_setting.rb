class DomainSetting < ApplicationRecord
  belongs_to :member

  scope :latest, -> {order(created_at: :desc)}
  scope :oldest, -> {order(created_at: :asc)}
  scope :pending, -> {where("domain_settings.status =?", DomainSetting::PENDING)}
  scope :process, -> {where("domain_settings.status =?", DomainSetting::PROCESS)}
  scope :active, -> {where("domain_settings.status =?", DomainSetting::ACTIVE)}
  scope :inactive, -> {where("domain_settings.status =?", DomainSetting::INACTIVE)}

  include TheDomainSetting::DomainSettingSearching

  scope :bonds, -> {
    eager_load(:member)
  }

  before_save :before_save_service
  
  # ------- STATUS --------
  PENDING = 'pending'.freeze
  PROCESS = 'process'.freeze
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze

  STATUSES = [self::PENDING, self::PROCESS, self::ACTIVE, self::INACTIVE].freeze

  def status?
    return '<span class="label label-warning">Pending</span>'.html_safe if self.status == DomainSetting::PENDING
    return '<span class="label label-info">Proses Pengajuan</span>'.html_safe if self.status == DomainSetting::PROCESS
    return '<span class="label label-success">Aktif</span>'.html_safe if self.status == DomainSetting::ACTIVE
    return '<span class="label label-danger">Tidak Aktif</span>'.html_safe if self.status == DomainSetting::INACTIVE
  end

  def is_pending?
    self.status == DomainSetting::PENDING
  end

  private
    def before_save_service
      self.name = self.name.downcase if self.name.present?
    end

end
