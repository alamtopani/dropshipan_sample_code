
class PaymentSnapshot < ApplicationRecord

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  scope :pending, -> { where(status: self::PENDING) }
  scope :process, -> { where(status: self::PROCESS) }
  scope :paid, -> { where(status: self::PAID) }
  scope :rejects, -> { where(status: self::REJECT) }

  has_many :activities, as: :activitiable, dependent: :destroy
  accepts_nested_attributes_for :activities, reject_if: :all_blank
  
  belongs_to :admin
  belongs_to :user
  belongs_to :member, foreign_key: "user_id"
  belongs_to :account_manager, foreign_key: "user_id"


  scope :bonds, -> {
    eager_load(:member)
  }
  
  include ThePaymentSnapshot::PaymentSnapshotSearching

  before_create :before_create_service


  # ============
  #   STATUSES
  # ============

  PENDING = 'pending'.freeze
  PROCESS = 'process'.freeze
  PAID = 'paid'.freeze
  REJECT = 'reject'.freeze

  STATUSES = [self::PENDING, self::PROCESS, self::PAID, self::REJECT].freeze

  # ============
  #  CATEGORIES
  # ============

  NEXT_PAYMENT = 'Next Payment'.freeze
  ONGOING_PAYMENT = 'Ongoing Payment'.freeze

  CATEGORIES = [[self::NEXT_PAYMENT, "1"], [self::ONGOING_PAYMENT, "2"]].freeze

  # def activities
  #   activities = Activity.where(activitiable_id: self.id, activitiable_type: self.class.name)
  # end

  def status?
    if self.status == "process"
      return "<label class='label label-info'>Process</label>".html_safe
    elsif self.status == "paid"
      return "<label class='label label-success'>Paid</label>".html_safe
    elsif self.status == "reject"
      return "<label class='label label-danger'>Reject</label>".html_safe
    elsif self.status == "pending"
      return "<label class='label label-warning'>Pending</label>".html_safe
    end
  end

  def is_pending?
    self.status == PaymentSnapshot::PENDING
  end

  def is_process?
    self.status == PaymentSnapshot::PROCESS
  end

  def is_paid?
    self.status == PaymentSnapshot::PAID
  end

  def is_reject?
    self.status == PaymentSnapshot::REJECT
  end

  def payment_period?
    return "#{ApplicationController.helpers.get_date(self.payment_start)} s.d. #{ApplicationController.helpers.get_date(self.payment_end)}"
  end

  def get_price?
    return ApplicationController.helpers.get_currency(self.price)
  end

  def self.create_payment_member(resource, payment_start, payment_end, payment, commissions_payment)
    @payment_snapshot = PaymentSnapshot.new
    @payment_snapshot.admin_id = Admin.first.id
    @payment_snapshot.user_type = "Member"
    @payment_snapshot.user_id = resource.id
    @payment_snapshot.payment_start = payment_start
    @payment_snapshot.payment_end = payment_end
    @payment_snapshot.status = PaymentSnapshot::PENDING
    @payment_snapshot.from_acc = "(#{payment.bank_name}) #{payment.name} - #{payment.account_number}"
    @payment_snapshot.to_acc = resource.profile.payment_to
    @payment_snapshot.message = "Pembayaran periode #{@payment_snapshot.payment_period?}."
    commissions = commissions_payment.where(member_id: resource.id)
    @payment_snapshot.fee_member = '%.4f' % commissions.sum(:commission_member)
    @payment_snapshot.fee_am = '%.4f' % commissions.sum(:commission_am)
    @payment_snapshot.fee_company = '%.4f' % commissions.sum(:commission_company)
    @payment_snapshot.total = @payment_snapshot.fee_member.to_f+@payment_snapshot.fee_am.to_f+@payment_snapshot.fee_company.to_f
    @payment_snapshot.price = @payment_snapshot.fee_member.to_f
    
    if @payment_snapshot.price.to_i > 0
      @payment_snapshot.save
      Activity.create(user_id: @payment_snapshot.admin_id, title: "Pembayaran #{@payment_snapshot.user_type} (#{@payment_snapshot.user.username})", description: "Sistem secara otomatis membuat pembayaran periode #{@payment_snapshot.payment_period?} sebesar #{@payment_snapshot.get_price?}.", activitiable_id: @payment_snapshot.id, activitiable_type: @payment_snapshot.class.name)
    end
  end

  def self.create_payment_account_manager(resource, payment_start, payment_end, payment, commissions_payment)
    @payment_snapshot = PaymentSnapshot.new
    @payment_snapshot.admin_id = Admin.first.id
    @payment_snapshot.user_type = "AccountManager"
    @payment_snapshot.user_id = resource.id
    @payment_snapshot.payment_start = payment_start
    @payment_snapshot.payment_end = payment_end
    @payment_snapshot.status = PaymentSnapshot::PENDING
    @payment_snapshot.from_acc = "(#{payment.bank_name}) #{payment.name} - #{payment.account_number}"
    @payment_snapshot.to_acc = resource.profile.payment_to
    @payment_snapshot.message = "Pembayaran periode #{@payment_snapshot.payment_period?}."
    commissions = commissions_payment.where(account_manager_id: resource.id)
    @payment_snapshot.fee_member = '%.4f' % commissions.sum(:commission_member)
    @payment_snapshot.fee_am = '%.4f' % commissions.sum(:commission_am)
    @payment_snapshot.fee_company = '%.4f' % commissions.sum(:commission_company)
    @payment_snapshot.total = @payment_snapshot.fee_member.to_f+@payment_snapshot.fee_am.to_f+@payment_snapshot.fee_company.to_f
    @payment_snapshot.price = @payment_snapshot.fee_am.to_f
    
    if @payment_snapshot.fee_am.to_i > 0
      @payment_snapshot.save
      Activity.create(user_id: @payment_snapshot.admin_id, title: "Pembayaran #{@payment_snapshot.user_type} (#{@payment_snapshot.user.username})", description: "Sistem secara otomatis membuat pembayaran periode #{@payment_snapshot.payment_period?} sebesar #{@payment_snapshot.get_price?}.", activitiable_id: @payment_snapshot.id, activitiable_type: @payment_snapshot.class.name)
    end
  end

  # def get_category
  #   category = self.category
  #   case category
  #   when '1' then category = 'Next Payment'
  #   when '2' then category = 'Ongoing Payment'   
  #   end
  # end

  private
    def before_create_service
      self.code = 'PS'+ 10.times.map{Random.rand(10)}.join
    end

end
