class Merchant < User
  extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]
  
  after_initialize :prepare_info
  default_scope {where(type: 'Merchant')}
  scope :featured, ->{ where(featured: true) }

  has_one :merchant_info, foreign_key: 'merchant_id', dependent: :destroy
  accepts_nested_attributes_for :merchant_info, reject_if: :all_blank

  has_many :products, foreign_key: 'merchant_id', dependent: :destroy
  accepts_nested_attributes_for :products, reject_if: :all_blank

  has_many :stakeholder_orders, foreign_key: 'merchant_id', dependent: :destroy
  accepts_nested_attributes_for :stakeholder_orders, reject_if: :all_blank

  has_many :order_items, foreign_key: 'merchant_id', dependent: :destroy
  accepts_nested_attributes_for :order_items, reject_if: :all_blank

  has_many :payment_merchants, dependent: :destroy

  has_one :invoice, foreign_key: 'user_id', dependent: :destroy
  accepts_nested_attributes_for :invoice, reject_if: :all_blank

  before_create :confirmation_email

  def have_sales_quantity?
    sales = self.stakeholder_orders.paid_success.includes(:order_items).pluck(:quantity).sum rescue 0
  end

  def have_sales_price?
    sales = self.stakeholder_orders.paid_success.pluck(:total_basic_price).sum rescue 0
  end

  def status_featured?
    self.featured == true
  end

  def is_cod?
    self.merchant_info.available_cod == true
  end

  def available_cod?
    if self.merchant_info.available_cod == true
      return "<label class='label label-info'>Bisa COD</label>".html_safe
    end
  end

  private
    def prepare_info
      self.merchant_info = MerchantInfo.new if self.merchant_info.blank?
    end

    def confirmation_email
      self.confirmation_token = nil
      self.confirmed_at = Time.zone.now
    end
end