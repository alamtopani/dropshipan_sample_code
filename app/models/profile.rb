class Profile < ActiveRecord::Base
	belongs_to :user, foreign_key: 'user_id'

	has_attached_file :avatar, styles: { 
                      large:    '512x512>', 
                      medium:   '256x256>', 
                      small:    '128x128>',
                      thumb:    '64x64>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :avatar, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

  has_attached_file :cover, styles: { 
                      large:    '512x512>', 
                      medium:   '256x256>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :cover, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

  def payment_to
    ["(#{self.bank_name}) ", "#{self.account_number} -", "#{self.account_name}"].select(&:'present?').join(' ')
  end

end
