class Brand < ApplicationRecord
  scope :latest, -> {order(name: :desc)}
  scope :oldest, -> {order(name: :asc)}

  has_attached_file :logo, styles: { 
                      large:    '512x512>', 
                      medium:   '256x256>', 
                      small:    '128x128>',
                      thumb:    '64x64>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :logo, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }
end
