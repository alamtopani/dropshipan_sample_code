class Comment < ActiveRecord::Base
	belongs_to :user, foreign_key: 'user_id'
	belongs_to :commentable, polymorphic: true

	default_scope {where(status: true)}
	
	include ScopeBased
end
