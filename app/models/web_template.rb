class WebTemplate < ApplicationRecord
  has_many :site_infos

  scope :latest, -> {order(created_at: :desc)}
  scope :oldest, -> {order(created_at: :asc)}
  scope :active, -> {where(status: "active")}

  has_attached_file :image, styles: { 
                      medium:   '256x256>', 
                      small:    '128x128>',
                      thumb:    '64x64>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :image, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

  include TheWebTemplate::WebTemplateSearching

  # ------- STATUS --------
  PENDING = 'pending'.freeze
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze

  STATUSES = [self::PENDING, self::ACTIVE, self::INACTIVE].freeze

  def status?
    return '<span class="label label-warning">Pending</span>'.html_safe if self.status == WebTemplate::PENDING
    return '<span class="label label-success">Active</span>'.html_safe if self.status == WebTemplate::ACTIVE
    return '<span class="label label-danger">Inactive</span>'.html_safe if self.status == WebTemplate::INACTIVE
  end
end
