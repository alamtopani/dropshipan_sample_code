class ReportRefund < ApplicationRecord
  belongs_to :order
  belongs_to :member
  belongs_to :account_manager

  scope :latest, -> {order(created_at: :desc)}
  scope :oldest, -> {order(created_at: :asc)}
  scope :pending, -> {where("report_refunds.status =?", ReportRefund::PENDING)}
  scope :process, -> {where("report_refunds.status =?", ReportRefund::PROCESS)}
  scope :paid, -> {where("report_refunds.status =?", ReportRefund::PAID)}
  scope :cancelled, -> {where("report_refunds.status =?", ReportRefund::CANCELLED)}

  before_save :before_save_service

  include TheReportRefund::ReportRefundSearching

  scope :bonds, -> {
    eager_load(:order, :member)
  }
  
  # ------- STATUS --------
  PENDING = 'pending'.freeze
  PROCESS = 'process'.freeze
  PAID = 'paid'.freeze
  CANCELLED = 'cancelled'.freeze

  STATUSES = [self::PENDING, self::PROCESS, self::PAID, self::CANCELLED].freeze

  def is_paid?
    self.status == ReportRefund::PAID
  end
  
  def status?
    return '<span class="label label-warning">Pending</span>'.html_safe if self.status == ReportRefund::PENDING
    return '<span class="label label-info">Process</span>'.html_safe if self.status == ReportRefund::PROCESS
    return '<span class="label label-success">Dibayarkan</span>'.html_safe if self.status == ReportRefund::PAID
    return '<span class="label label-danger">Dibatalkan</span>'.html_safe if self.status == ReportRefund::CANCELLED
  end

  def before_save_service
    self.member_id = self.order.member_id
    self.account_manager_id = self.order.account_manager_id
  end
end
