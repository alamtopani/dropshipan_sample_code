class Order < ActiveRecord::Base
  extend FriendlyId
  friendly_id :code, use: [:slugged, :finders]
  
  after_initialize :after_initialized
  belongs_to :member, foreign_key: 'member_id'

  has_many :stakeholder_orders, foreign_key: 'order_id', dependent: :destroy
  accepts_nested_attributes_for :stakeholder_orders, reject_if: :all_blank, allow_destroy: true

  has_many :order_items, foreign_key: 'order_id', dependent: :destroy
  accepts_nested_attributes_for :order_items, reject_if: :all_blank, allow_destroy: true

  has_one :address, as: :addressable, dependent: :destroy
  accepts_nested_attributes_for :address, reject_if: :all_blank

  has_many :commissions, dependent: :destroy
  accepts_nested_attributes_for :commissions, reject_if: :all_blank

  has_one :report_refund, dependent: :destroy
  accepts_nested_attributes_for :report_refund, reject_if: :all_blank

  has_many :activities, as: :activitiable, dependent: :destroy
  accepts_nested_attributes_for :activities, reject_if: :all_blank 

  belongs_to :account_manager
  has_one :confirmation

  include ScopeBased
  include TheOrder::OrderSearching

  scope :in_progress, -> {where("orders.order_status_id =?", 1)}
  scope :without_in_progress, -> {where.not("orders.order_status_id =?", 1)}
  scope :verification, -> {where("orders.order_status_id =?", 2)}
  scope :paid, -> {where("orders.order_status_id =?", 3)}
  scope :cancelled, -> {where("orders.order_status_id =?", 4)}
  scope :paid_cancel, ->{where(order_status_id: [3,4])}

  scope :bonds, -> {
    eager_load(:member)
  }

  before_create :prepare_code, :set_order_status

  VERIFICATION = 'verification'
  PACKING = 'packing'
  SHIPPED = 'shipped'
  RECEIVED = 'received'
  CANCELLED = 'cancelled'

  STATUSES = [["Pemeriksaan", OrderItem::VERIFICATION], ["Packing", OrderItem::PACKING], ["Dikirim", OrderItem::SHIPPED], ["Diterima", OrderItem::RECEVIED], ["Dibatalkan", OrderItem::CANCELLED]]
  PAYMENT_STATUS = [["Sudah Dibayar", 3],["Dibatalkan", 4]]
  COURIER = [['Kirim Barang Dengan Ekspedisi Pengiriman', 'JASA PENGIRIMAN'], ['COD/Ambil stockis dikantor Merchant', 'COD']]

  scope :track_pending, ->{where("orders.track_order =?", OrderItem::PENDING)}
  scope :track_verification, ->{where("orders.track_order =?", OrderItem::VERIFICATION)}
  scope :track_packing, ->{where("orders.track_order =?", OrderItem::PACKING)}
  scope :track_verification_packing, ->{where("orders.track_order =? OR orders.track_order =?", OrderItem::VERIFICATION, OrderItem::PACKING)}
  scope :track_verification_packing_cancelled, ->{where("orders.track_order =? OR orders.track_order =? OR orders.track_order =?", OrderItem::VERIFICATION, OrderItem::PACKING, OrderItem::CANCELLED)}
  scope :track_shipped, ->{where("orders.track_order =?", OrderItem::SHIPPED)}
  scope :track_packing_shipped, ->{where("orders.track_order =? OR orders.track_order =?", OrderItem::PACKING, OrderItem::SHIPPED)}
  scope :track_received, ->{where("orders.track_order =?", OrderItem::RECEVIED)}
  scope :track_shipped_received, ->{where("orders.track_order =? OR orders.track_order =?", OrderItem::SHIPPED, OrderItem::RECEVIED)}
  scope :track_cancelled, ->{where("orders.track_order =?", OrderItem::CANCELLED)}

  #------------------- PAYMENT METHOD -------------------
  def is_cod?
    self.stakeholder_orders.present? && self.stakeholder_orders.joins(merchant: :merchant_info).where('users.featured =?', true).where('merchant_infos.available_cod =?', true).present?
  end

  def address_merchant?
    if self.is_cod?
      return "#{self.stakeholder_orders.last.merchant.merchant_info.title} beralamat #{self.stakeholder_orders.last.merchant.address.place_info_merchant}, atau kontak di #{self.stakeholder_orders.last.merchant.profile.phone}".html_safe rescue '--'
    else
      return '--'
    end
  end

  def courier?
    if self.courier.include?('COD')
      return '<span class="label label-info">COD</span>'.html_safe
    else
      return '<span class="label label-primary">JASA PENGIRIMAN</span>'.html_safe
    end
  end

  def cod?
    self.courier == 'COD'
  end


  #------------------- GET STATUS -------------------
  def status_order?
    if self.cod? && self.track_order == Order::VERIFICATION || self.cod? && self.track_order == Order::PACKING
      self.courier?
    else
      return '<span class="label label-warning">Pemeriksaan</span>'.html_safe if self.track_order == Order::VERIFICATION
      return '<span class="label label-info">Packing</span>'.html_safe if self.track_order == Order::PACKING
      return '<span class="label label-primary">Dikirim</span>'.html_safe if self.track_order == Order::SHIPPED
      return '<span class="label label-success">Diterima</span>'.html_safe if self.track_order == Order::RECEIVED
      return '<span class="label label-danger">Dibatalkan</span>'.html_safe if self.track_order == Order::CANCELLED
    end
  end

  def status_order_text?
    if self.cod? && self.track_order == Order::VERIFICATION || self.cod? && self.track_order == Order::PACKING
      return 'COD'.html_safe
    else
      return 'Pemeriksaan'.html_safe if self.track_order == Order::VERIFICATION
      return 'Packing'.html_safe if self.track_order == Order::PACKING
      return 'Dikirim'.html_safe if self.track_order == Order::SHIPPED
      return 'Diterima'.html_safe if self.track_order == Order::RECEIVED
      return 'Dibatalkan'.html_safe if self.track_order == Order::CANCELLED
    end
  end

  def self.check_status_order_text(track_order)
    return 'Pemeriksaan'.html_safe if track_order == Order::VERIFICATION
    return 'Packing'.html_safe if track_order == Order::PACKING
    return 'Dikirim'.html_safe if track_order == Order::SHIPPED
    return 'Diterima'.html_safe if track_order == Order::RECEIVED
    return 'Dibatalkan'.html_safe if track_order == Order::CANCELLED
  end

  def status?
    return '<span class="label label-primary">Diproses</span>'.html_safe if self.order_status_id == 1
    return '<span class="label label-warning">Pemeriksaan</span>'.html_safe if self.order_status_id == 2
    return '<span class="label label-success">Sudah Dibayar</span>'.html_safe if self.order_status_id == 3
    return '<span class="label label-danger">Dibatalkan</span>'.html_safe if self.order_status_id == 4
  end

  def status_text?
    return 'Diproses' if self.order_status_id == 1
    return 'Pemeriksaan' if self.order_status_id == 2
    return 'Sudah Dibayar' if self.order_status_id == 3
    return 'Dibatalkan' if self.order_status_id == 4
  end

  def self.check_status_text(order_status_id)
    return 'Diproses' if order_status_id == 1
    return 'Pemeriksaan' if order_status_id == 2
    return 'Sudah Dibayar' if order_status_id == 3
    return 'Dibatalkan' if order_status_id == 4
  end

  def in_progress?
    self.order_status_id == 1
  end

  def verification?
    self.order_status_id == 2
  end

  def paid?
    self.order_status_id == 3
  end

  def cancel?
    self.order_status_id == 4
  end

  def confirmation
    confirmation = Confirmation.where("LOWER(confirmations.no_invoice) LIKE LOWER('#{self.code}')").first
  end

  def confirmation?
    confirmation = Confirmation.where("LOWER(confirmations.no_invoice) LIKE LOWER('#{self.code}')").first
    if confirmation.present?
      return '<span class="label label-success">OK</span>'.html_safe
    else
      return '<span class="label label-default">NO</span>'.html_safe
    end
  end

  def confirmation_text?
    confirmation = Confirmation.where("LOWER(confirmations.no_invoice) LIKE LOWER('#{self.code}')").first
    if confirmation.present?
      return 'OK'
    else
      return 'NO'
    end
  end

  def quantity?
    if self.order_items.present?
      quantity = self.order_items.not_refund_and_cancelled.pluck(:quantity).sum.to_i
    end
  end

  #------------------- GET TOTAL WEIGHT -------------------
  def total_weight?
    if self.order_items.present?
      weight = self.order_items.not_refund_and_cancelled.pluck(:weight).sum.to_i
      if weight < 1
        total = 1
      else
        total = weight.to_f
      end
    end
  end
  
  def weight?
    if self.order_items.present?
      weight = self.order_items.not_refund_and_cancelled.pluck(:weight).sum.to_i
    end
  end

  #------------------- GET TOTAL PRICE -------------------
  def total?
    if User.current.present? && User.current.member?
      order_items.not_refund_and_cancelled.collect { |oi| oi.valid? ? (oi.quantity * oi.company_price) : 0 }.sum
    else
      order_items.not_refund_and_cancelled.collect { |oi| oi.valid? ? (oi.quantity * oi.price) : 0 }.sum
    end
  end

  def total_basic_price?
    order_items.not_refund_and_cancelled.collect { |oi| oi.valid? ? (oi.quantity * oi.basic_price) : 0 }.sum
  end

  def total_company_price?
    order_items.not_refund_and_cancelled.collect { |oi| oi.valid? ? (oi.quantity * oi.company_price) : 0 }.sum
  end

  def total_shipping_price?
    if self.stakeholder_orders.present?
      total_shipping_price = self.stakeholder_orders.pluck(:shipping_price).sum
    end
  end

  def total_payment?
    if self.stakeholder_orders.present?
      if User.current.present? && User.current.member?
        total_payment = self.stakeholder_orders.pluck(:total_company_price).sum
      else
        total_payment = self.stakeholder_orders.pluck(:total_price).sum
      end
    end
  end

  def total_payment_basic_price?
    if self.stakeholder_orders.present?
      total_payment = self.stakeholder_orders.pluck(:total_basic_price).sum
    end
  end

  def total_payment_company_price?
    if self.stakeholder_orders.present?
      total_payment = self.stakeholder_orders.pluck(:total_company_price).sum
    end
  end

  #------------------- PROSES CHECKOUT -------------------
  def self.pay(user, ordered, payerAddress, shipping_price, cod)
    begin
      order = self.find_by_code(ordered.code)
      order.payment_token = SecureRandom.urlsafe_base64(nil, false)
      order.payer_address = payerAddress
      order.purchased_at = Time.now
      order.order_status_id = 2
      order.account_manager_id = order.member.account_manager_id

      if order.save
        order.stakeholder_orders.each_with_index do |holder, index|
          holder.no_invoice = order.code + index.to_s
          holder.order_items.map.each do |item|
            item.no_invoice = holder.no_invoice
            if item.product.ready_stock?
              quantity = item.quantity <= item.product_stock? ? item.quantity : item.product_stock?
              item.quantity = quantity == 0 ? 1 : quantity
            end

            item.price = item.catalog.present? ? item.catalog.get_price? : item.price
            item.basic_price = item.basic_price
            item.company_price = item.company_price
            item.total_price = item.quantity * item.price
            item.total_basic_price = item.quantity * item.basic_price
            item.total_company_price = item.quantity * item.company_price
            item.weight = item.product.weight * item.quantity
            discount = item.catalog.present? && item.catalog.discount > 0 ? (item.catalog.price - (item.catalog.price * item.catalog.discount/100)) : 0
            item.discount = discount * item.quantity
            if item.product_stock_available? && item.product_stock? > 0
              item.quantity_reduced
            elsif item.product.ready_stock? && item.product_stock? < 1
              item.status = OrderItem::CANCELLED
              item.cancelled_reason = "Stok produk varian ini habis"
            end
            item.account_manager_id = order.account_manager_id
            item.save
          end

          # shipping_price = order.prepare_shipping_price(shipping_address[0],shipping_address[1],shipping_address[2],shipping_address[3])
          holder.weight = holder.total_weight?
          # holder.shipping_price = shipping_price * holder.weight
          holder.price = holder.total?
          holder.basic_price = holder.basic_price?
          holder.company_price = holder.company_price?
          holder.total_price = holder.shipping_price + holder.price
          holder.total_basic_price = holder.shipping_price + holder.basic_price
          holder.total_company_price = holder.shipping_price + holder.company_price
          holder.account_manager_id = order.account_manager_id
          holder.save
        end

        if order.order_items.not_refund_and_cancelled.blank?
          order.order_status_id = 4
          order.track_order = OrderItem::CANCELLED
          order.shipping_price = 0
        else
          rand_3_number = 3.times.map{rand(9)}.join.to_i
          order.uniq_code = rand_3_number
          order.shipping_price = (shipping_price + rand_3_number).to_i
          order.shipping_price = rand_3_number.to_i if cod == true
        end

        order.price = order.total?
        order.basic_price = order.total_basic_price?
        order.company_price = order.total_company_price?
        order.total_price = order.price + order.shipping_price
        order.total_basic_price = order.basic_price + order.shipping_price
        order.total_company_price = order.company_price + order.shipping_price
        order.save
      end
      return order
    rescue
      false
    end
  end

  #------------------- PROSES CANCELATION -------------------
  def cancellation
    if cancel?
      self.order_items.each do |item|
        item.quantity_cancellation
      end
    end
  end

  #------------------- PROSES CHANGE STATUS PAYMENT -------------------
  def prepare_status_payment
    if paid?
      self.stakeholder_orders.each do |holder|
        holder.payment_status = true
        holder.save
      end
    else
      self.stakeholder_orders.each do |holder|
        holder.payment_status = false
        holder.save
      end
    end
  end

  # ---------------------STATUS TRACK ORDER------------------
  def shipped?
    self.track_order == OrderItem::SHIPPED
  end

  def verification?
    self.track_order == OrderItem::VERIFICATION
  end

  def get_commisions_value(key)
    self.commissions.sum(key)
  end

  # ---------------------ACCESS ORDER------------------
  def select_track_order_access?
    if self.track_order == 'verification'
      collect_status = [["Pemeriksaan", "verification"], ["Packing", "packing"]]
    elsif self.track_order == 'packing'
      collect_status = [["Packing", "packing"], ["Dikirim", "shipped"]]
    elsif self.track_order == 'shipped'
      collect_status = [["Dikirim", "shipped"], ["Diterima", "received"]]
    end
  end

  def select_track_order_access_cod?
    if self.track_order == 'verification'
      collect_status = [["Pemeriksaan", "verification"], ["Packing", "packing"]]
    elsif self.track_order == 'packing'
      collect_status = [["Packing", "packing"], ["Diterima", "received"]]
    end
  end

  # --------------------- UPDATE ORDER ------------------
  def self.update_order(order, old_status, old_status_shipping)
    @old_status = old_status
    if @old_status == 2 && order.order_status_id == 3
      UserMailer.order_paid(order).deliver if order.stakeholder_orders.last.merchant.status_featured?
      unless order.cod?
        UserMailer.order_paid_reseller(order).deliver
      end
    end
    order.stakeholder_orders.each_with_index do |holder, index|
      holder.order_items.map.each do |item|
        item.status = order.track_order unless item.status == OrderItem::REFUND || item.status == OrderItem::CANCELLED
        if (@old_status != 4 && order.cancel?)
          item.status = OrderItem::CANCELLED
          item.quantity_cancellation if item.product.ready_stock?
        end
        item.weight = item.product.weight * item.quantity
        item.total_price = item.total_price?
        item.total_basic_price = item.total_basic_price?
        item.total_company_price = item.total_company_price?
        if item.save
          if order.cod?
            if order.paid? && (old_status_shipping == OrderItem::PACKING && order.track_order == OrderItem::RECEVIED)
              item.add_commision
            end
          else
            if order.paid? && (old_status_shipping == OrderItem::PACKING && order.track_order == OrderItem::SHIPPED)
              item.add_commision
            end
          end
        end
      end

      holder.track_order = order.track_order
      holder.track_order = OrderItem::CANCELLED if (@old_status != 4 && order.cancel?)
      holder.weight = holder.total_weight?
      holder.price = holder.total?
      holder.basic_price = holder.basic_price?
      holder.company_price = holder.company_price?
      holder.total_price = holder.shipping_price + holder.price
      holder.total_basic_price = holder.shipping_price + holder.basic_price
      holder.total_company_price = holder.shipping_price + holder.company_price
      holder.payment_status = order.paid? ? true : false
      holder.save
    end

    if (@old_status != 4 && order.cancel?)
      order.track_order = OrderItem::CANCELLED
      order.shipping_price = 0
    end
    order.price = order.total?
    order.basic_price = order.total_basic_price?
    order.company_price = order.total_company_price?
    order.total_price = order.total_payment? + order.shipping_price
    order.total_basic_price = order.total_payment_basic_price? + order.shipping_price
    order.total_company_price = order.total_payment_company_price? + order.shipping_price
    order.save
  end

  private

    def set_order_status
      self.order_status_id = 1
    end

    def prepare_code
      self.code = SecureRandom.hex(6) if self.code.blank?
      self.due_date = Time.zone.now.to_date+3.days
    end

    def after_initialized
      self.address = Address.new if self.address.blank?
    end
end
