class PaymentMerchant < ApplicationRecord
	scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  scope :pending, -> { where(status: self::PENDING) }
  scope :process, -> { where(status: self::PROCESS) }
  scope :paid, -> { where(status: self::PAID) }
  scope :rejects, -> { where(status: self::REJECT) }

  scope :bonds, -> {
    eager_load(:merchant)
  }
  
  belongs_to :merchant
  belongs_to :admin

  has_many :activities, as: :activitiable, dependent: :destroy
  
  include ThePaymentMerchant::PaymentMerchantSearching

  before_create :before_create_service


  # ============
  #   STATUSES
  # ============

  PENDING = 'pending'.freeze
  PROCESS = 'process'.freeze
  PAID = 'paid'.freeze
  REJECT = 'reject'.freeze

  STATUSES = [self::PENDING, self::PROCESS, self::PAID, self::REJECT].freeze

  def status?
    if self.status == "process"
      return "<label class='label label-info'>Process</label>".html_safe
    elsif self.status == "paid"
      return "<label class='label label-success'>Paid</label>".html_safe
    elsif self.status == "reject"
      return "<label class='label label-danger'>Reject</label>".html_safe
    elsif self.status == "pending"
      return "<label class='label label-warning'>Pending</label>".html_safe
    end
  end

  def is_pending?
    self.status == PaymentMerchant::PENDING
  end

  def is_process?
    self.status == PaymentMerchant::PROCESS
  end

  def is_paid?
    self.status == PaymentMerchant::PAID
  end

  def is_reject?
    self.status == PaymentMerchant::REJECT
  end

  def payment_period?
    return "#{ApplicationController.helpers.get_date(self.payment_start)} s.d. #{ApplicationController.helpers.get_date(self.payment_end)}"
  end

  def get_price?
    return ApplicationController.helpers.get_currency(self.price)
  end

  def self.create_payment_merchant(resource, payment_start, payment_end, payment, payment_merchant)
    @payment_merchant = PaymentMerchant.new
    @payment_merchant.admin_id = Admin.first.id
    @payment_merchant.merchant_id = resource.id
    @payment_merchant.payment_start = payment_start
    @payment_merchant.payment_end = payment_end
    @payment_merchant.status = PaymentMerchant::PENDING
    @payment_merchant.from_acc = "(#{payment.bank_name}) #{payment.name} - #{payment.account_number}"
    @payment_merchant.to_acc = resource.profile.payment_to
    @payment_merchant.message = "Pembayaran periode #{@payment_merchant.payment_period?}." 
    
    if resource.status_featured?
      order = Order.joins(:stakeholder_orders).where("stakeholder_orders.merchant_id =? AND DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=?", resource.id, payment_start, payment_end).paid
    else
      order = payment_merchant.where(merchant_id: resource.id)
    end
    @payment_merchant.price = '%.4f' % order.sum(:basic_price)
    @payment_merchant.ongkir = '%.4f' % order.sum(:shipping_price)
    @payment_merchant.total = '%.4f' % order.sum(:total_basic_price)

    if @payment_merchant.total.to_i > 0
      @payment_merchant.save 
      Activity.create(user_id: @payment_merchant.admin_id, title: "Pembayaran #{resource.username} (#{resource.merchant_info.title})", description: "Sistem secara otomatis membuat pembayaran periode #{@payment_merchant.payment_period?} sebesar #{@payment_merchant.get_price?}.", activitiable_id: @payment_merchant.id, activitiable_type: @payment_merchant.class.name)
    end
  end

  private
    def before_create_service
      self.code = 'PM'+ 10.times.map{Random.rand(10)}.join
    end
end
