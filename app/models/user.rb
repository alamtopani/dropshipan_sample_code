class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]
  # after_create :send_welcome_mail

  # before_create :check_referral_top
  
  is_impressionable
  acts_as_votable
  include TheUser::UserConfig
  include TheUser::UserAuthenticate
  include TheUser::UserSearching
  include ScopeBased

  # ---- COMMISSION FEE ----
  FEE_AM = 1/100.to_f
  FEE_REF1 = 10/100.to_f
  FEE_REF2 = 5/100.to_f
  FEE_REF3 = 3/100.to_f

  # ------- STATUS --------
  STATUSES = [["Verified", true], ["Non verified", false], ["Email has not confirmed"]].freeze
  
  # ------- FEATURED --------
  FEATURED = [["Featured", true], ["Non featured", false]].freeze
  
  def score
    self.get_upvotes.size
  end

  def self.current
    Thread.current[:user]
  end
  
  def self.current=(user)
    Thread.current[:user] = user
  end

  def the_name?
    self.username || self.profile.full_name
  end

  # --- CEK REFERRAL ---
  def check_referral_top(code)
    if self.type == "Member"
      ref = Member.where(code: code).last
      self.ref_id1 = ref.id
      if self.ref_id1.present?
        ref_id1 = Member.find(self.ref_id1)
        self.account_manager_id = ref_id1.account_manager_id

        if ref_id1.ref_id1.present?
          self.ref_id2 = ref_id1.ref_id1
        end

        if ref_id1.ref_id2.present?
          self.ref_id3 = ref_id1.ref_id2
        end
      end
    end
  end

  # --- CEK AFFILIATE ---
  def check_referral_one(ref_id1,cpa,invoice_id)
    if self.type == "Member"
      ref = Member.find_by(id: ref_id1)
      if ref.present?
        # ------  Affiliate  ------
        affiliate = Affiliate.new
        affiliate.member_id = ref.id
        affiliate.member_registration_id = self.id
        affiliate.basic_price = Affiliate::AFFILIATE_BASIC_PRICE
        affiliate.sale_price = Affiliate::AFFILIATE_SALE_PRICE
        affiliate.fee_company = Affiliate::AFFILIATE_FEE_COMPANY
        affiliate.invoice_id = invoice_id
        if cpa == true
          affiliate.fee_affiliate = Affiliate::AFFILIATE_FEE
        else
          affiliate.fee_member = Affiliate::AFFILIATE_FEE_MEMBER
        end
        affiliate.save
      else
        # ------  Affiliate  ------
        affiliate = Affiliate.new
        affiliate.member_id = nil
        affiliate.member_registration_id = self.id
        affiliate.basic_price = Affiliate::AFFILIATE_BASIC_PRICE
        affiliate.sale_price = Affiliate::AFFILIATE_SALE_PRICE
        affiliate.fee_company = Affiliate::AFFILIATE_FEE_COMPANY
        affiliate.invoice_id = invoice_id
        if cpa == true
          affiliate.fee_affiliate = Affiliate::AFFILIATE_FEE
        else
          affiliate.fee_company = Affiliate::AFFILIATE_FEE_MEMBER + Affiliate::AFFILIATE_FEE_COMPANY
        end
        affiliate.save
      end
    end
  end

  private
    def send_welcome_mail
      UserMailer.welcome_join(self).deliver
    end
end
