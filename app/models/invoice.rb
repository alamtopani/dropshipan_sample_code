class Invoice < ApplicationRecord
	# belongs_to :invoiceable, polymorphic: true

  has_many :activities, as: :activitiable, dependent: :destroy
  accepts_nested_attributes_for :activities, reject_if: :all_blank

  has_one :convertion
  
  belongs_to :user
  belongs_to :package, foreign_key: "invoiceable_id"

  belongs_to :member, foreign_key: 'user_id'
  
  scope :latest, -> {order(created_at: :desc)}
  scope :oldest, -> {order(created_at: :asc)}
  scope :paid, -> {where(status: Invoice::PAID)}
  scope :pending, -> {where(status: Invoice::PENDING)}
  scope :active_promo, -> {where(promo: true)}
  scope :non_active_promo, -> {where(promo: false)}

  before_create :before_create_service

  scope :current_packages, -> (id) {where(invoiceable_id: id)}
  scope :pending_packages_member, -> (member_id) {where(user_id: member_id, status: Invoice::PENDING)}
  scope :bonds, -> {
    eager_load(:user, :convertion)
  }

  include TheInvoice::InvoiceSearching

  # ------- STATUS --------
  PENDING = 'pending'.freeze
  PAID = 'paid'.freeze
  HAVE_CONVERTION = 'Have Convertion'.freeze
  BLANK_CONVERTION = 'Blank Convertion'.freeze

  CONVERTION = [self::HAVE_CONVERTION, self::BLANK_CONVERTION]
  STATUSES = [self::PENDING, self::PAID].freeze
  
  def status?
    return '<span class="label label-success">Paid</span>'.html_safe if self.status == Invoice::PAID
    return '<span class="label label-warning">Pending</span>'.html_safe if self.status == Invoice::PENDING
    return '<span class="label label-danger">Unpaid</span>'.html_safe if self.status == "unpaid"
  end

  def promo?
    return '<span class="label label-success">FREE</span>'.html_safe if self.promo == true
    return '<span class="label label-default">Berbayar</span>'.html_safe if self.promo == false
  end

  def paid?
    self.status == Invoice::PAID
  end

  def pending?
    self.status == Invoice::PENDING
  end

  def check_period_active
    if self.status == Invoice::PAID && self.period_end >= Time.zone.now.to_date
      return true
    end
  end

  def check_period_expired
    if self.status == Invoice::PAID && self.period_end < Time.zone.now.to_date
      return true
    end
  end

  def expired_at?
    self.created_at.to_date + 4.days
  end

  def package
    if self.invoiceable_type == "Package"
      package = Package.where(id: self.invoiceable_id).first 
    end
  end

  private
    def before_create_service
      self.code = 'I'+ 10.times.map{Random.rand(10)}.join
      uniq_code = 3.times.map{rand(9)}.join.to_i
      price = self.price.to_i + uniq_code
      self.price = price.to_i
      self.uniq_code = uniq_code
    end
end
