function initFlexModal() {
  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: "thumbnails",
    start: function(slider){
      $('body').removeClass('loading');
    }
  });
}

$(document).ready(function(){
  
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.1&appId=544011456024962&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  
  $('.js-scrolltofixed').scrollToFixed();

  $(window).scroll (function () {
    var sT = $(this).scrollTop();
    if (sT >= 30) {
      $('.js-top-navbar').addClass('activated btn-signup')
    }else {
      $('.js-top-navbar').removeClass('activated btn-signup')
    }
  });

  $(window).scroll (function () {
    var sT = $(this).scrollTop();
    if (sT >= 100) {
      $('.js-btn-up').show()
    }else {
      $('.js-btn-up').hide()
    }
  });

  $(window).scroll (function () {
    var sT = $(this).scrollTop();
    if (sT >= 30) {
      $('.js-toggle').addClass('navbar-toggle')
    }else {
      $('.js-toggle').removeClass('navbar-toggle')
    }
  });

  $('#navigation .menu a').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });

  $('.js-btn-up').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });

  $('.js-scroll-all').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });


  $(".view").on( "click", function() {
    $(this).next().slideToggle(250);
    $fexpand = $(this).find(">:first-child");
    if ($(this).hasClass('opened')) {
      $(this).removeClass('opened');
    } else {
      $(this).addClass('opened');
    };
  });


  $('#navigation .menu a navbar-brand a').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 100);
        return false;
      }
    }
  });

  $(document).on('click', '.js-accordion-title', function(){
    $(this).find('.toggle-down').show(); 
    $(this).find('.toggle-up').hide();
  });

  $(document).on('click', '.js-accordion-title.opened', function(){
    $(this).find('.toggle-up').show();
    $(this).find('.toggle-down').hide(); 
  });

  $('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
  })

  $("input.check-all:checkbox").click(function () {
    var checkedStatus = this.checked;
    $("input.pro-chx:checkbox").each(function () {
      this.checked = checkedStatus;
    });
  });

  $('.squarespaceModal').on('shown.bs.modal', function () {
    initFlexModal();
  });

  $('.nav-tabs a').click(function(){
    $(this).tab('show');
  });

  var mainTicker = new Flickity('.js-main-slider', {
    accessibility: true,
    resize: true,
    wrapAround: true,
    prevNextButtons: false,
    pageDots: false,
    percentPosition: true,
    setGallerySize: true,
  });

  mainTicker.x = 0;
  play();

  function play() {
    mainTicker.x -= 1.5;
    mainTicker.settle(mainTicker.x);
    requestId = window.requestAnimationFrame(play);
  }

  AOS.init({
    duration: 1200,
  })

});
