function initFlexModal() {
  $('.js-catalog-carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 100,
    itemMargin: 5,
    asNavFor: '.js-catalog-slider'
  });

  $('.js-catalog-slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: ".js-catalog-carousel"
  });
}

function scrollActiveNavbar(){
  $(window).scroll (function () {
    var sT = $(this).scrollTop();
    if (sT >= 30) {
      $('.js-top-navbar').addClass('activated btn-signup')
    }else {
      $('.js-top-navbar').removeClass('activated btn-signup')
    }
  });

  $(window).scroll (function () {
    var sT = $(this).scrollTop();
    if (sT >= 100) {
      $('.js-btn-up').show()
    }else {
      $('.js-btn-up').hide()
    }
  });

  $(window).scroll (function () {
    var sT = $(this).scrollTop();
    if (sT >= 30) {
      $('.js-toggle').addClass('navbar-toggle')
    }else {
      $('.js-toggle').removeClass('navbar-toggle')
    }
  });
}

function TransitionNavbar(){
  $('#navigation .menu a').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });

  $('.js-btn-up').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });

  $('#navigation .menu a navbar-brand a').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 100);
        return false;
      }
    }
  });
}

function toggleFaq(){
  $(".view").on( "click", function() {
    $(this).next().slideToggle(250);
    $fexpand = $(this).find(">:first-child");
    if ($(this).hasClass('opened')) {
      $(this).removeClass('opened');
    } else {
      $(this).addClass('opened');
    };
  });
}

function toggleFaqUpdown(){
  $(document).on('click', '.js-accordion-title', function(){
    $(this).find('.toggle-down').show(); 
    $(this).find('.toggle-up').hide();
  });

  $(document).on('click', '.js-accordion-title.opened', function(){
    $(this).find('.toggle-up').show();
    $(this).find('.toggle-down').hide(); 
  });
}

function selectPackage(){
  $('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
  })
}

function checkAll(){
  $("input.check-all:checkbox").click(function () {
    var checkedStatus = this.checked;
    $("input.pro-chx:checkbox").each(function () {
      this.checked = checkedStatus;
    });
  });
}

function checkedLp(){
  $(".js-box-request-lp").click(function () {
    $('.js-box-request-lp').removeClass('checked-lp');
    if($(this).find('input').prop("checked", true)){
      $(this).addClass('checked-lp');
    }
  });
}

function disableZero(){
  $('#disable-zero').keypress(function(e){ 
    if (this.value.length == 0 && e.which == 48 ){
      $('.note').show();
      return false;
    }
  });
}

function BtnPlusNumber(){
  $(document).on('click', '.number-spinner button', function () {    
    var btn = $(this),
    oldValue = btn.closest('.number-spinner').find('input').val().trim(),
    newVal = 0;
    
    if (btn.attr('data-dir') == 'up') {
      newVal = parseInt(oldValue) + 1;
    } else {
      if (oldValue > 1) {
        newVal = parseInt(oldValue) - 1;
      } else {
        newVal = 1;
      }
    }
    btn.closest('.number-spinner').find('input').val(newVal);
  });
}

// --------Render Javascript----------
$(document).ready(function(){
  scrollActiveNavbar();
  TransitionNavbar();
  toggleFaq();
  toggleFaqUpdown();
  selectPackage();
  checkAll();
  checkedLp();
  disableZero();
  BtnPlusNumber();
  $("#datepicker").datepicker();
  $('.squarespaceModal').on('shown.bs.modal', function () {
    initFlexModal();
  });
});


