//= require frontend/jquery
//= require rails-ujs
//= require frontend/bootstrap
//= require frontend/grid-plugin
//= require frontend/flexslider
//= require frontend/bootstrap-magnify
//= require frontend/select2
//= require frontend/gmap3
//= require frontend/jquery.custom_ligthbox
//= require cocoon
//= require ckeditor/init
//= require ckeditor/config
//= require frontend/jquery-scrolltofixed-min
//= require moment
//= require bootstrap-datetimepicker
//= require ahoy
//= require range/jshashtable-2.1_src
//= require range/jquery.numberformatter-1.2.3
//= require range/tmpl
//= require range/jquery.dependClass-0.1
//= require range/draggable-0.1
//= require range/jquery.slider


$(document).ready(function() {
  $('#top-navigation').scrollToFixed();
  $('.js-sidebar-fixed').scrollToFixed();

  ahoy.trackView();
  customize();
  carousel();
  xhrsData();
});

function customize(){
  $(document).on('click', '.number-spinner button', function () {    
    var btn = $(this),
    oldValue = btn.closest('.number-spinner').find('input').val().trim(),
    newVal = 0;
    
    if (btn.attr('data-dir') == 'up') {
      newVal = parseInt(oldValue) + 1;
    } else {
      if (oldValue > 1) {
        newVal = parseInt(oldValue) - 1;
      } else {
        newVal = 1;
      }
    }
    btn.closest('.number-spinner').find('input').val(newVal);
  });
  
  $('.helps-sidebar li a').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });

  $(document).on('click', '.dropdown-toggle', function(){
    if($('.overplay-top-menu').is(":visible") == true){
      $('.overplay-top-menu').hide(); 
    }else{
      $('.overplay-top-menu').show();
    }
  });
  
  $(document).on('click', '.overplay-top-menu', function(){
    $('.overplay-top-menu').hide(); 
  });

  $('.datepicker').datetimepicker({
    format: 'YYYY-MM-DD'
  });

  $('[data-toggle="popover"]').each(function(){
    $(this).popover({
      trigger: 'hover',
      placement: 'top',
      html : true,
      content: function() {
        return $(this).children('.tooltips-content').html();
      }
    });
  });

  $(".select2").select2({
    maximumSelectionLength: 2
  });

  // $(".view").on( "click", function() {
  //   $(this).next().slideToggle(250);
  //   $fexpand = $(this).find(">:first-child");
  //   if ($(this).hasClass('opened')) {
  //     $(this).removeClass('opened');
  //   } else {
  //     $(this).addClass('opened');
  //   };
  // });
  
  // $(document).on('click', '.js-accordion-title', function(){
  //   $(this).find('.toggle-down').show(); 
  //   $(this).find('.toggle-up').hide();
  // });

  // $(document).on('click', '.js-accordion-title.opened', function(){
  //   $(this).find('.toggle-up').show();
  //   $(this).find('.toggle-down').hide(); 
  // });

}

function carousel(){
  // $('.home-carousel .item').each(function(){
  //   var topSlider = $('.home-carousel').outerWidth() / 3.5;
  //   $(this).css('height', topSlider);
  // });

  $('#carousel-product').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 80,
    itemMargin: 5,
    asNavFor: '#slider-product'
  });

  $('#slider-product').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel-product"
  });

  $('.js-featured-product').flexslider({
    animation: "slide",
    controlNav: true,
    animationLoop: true,
    slideshow: true,
    itemWidth: 230
  });

  jQuery("#Slider2").slider({
    from: 5000,
    to: 20000000,
    heterogeneity: ['50/50000'],
    step: 1000,
    dimension: '&nbsp;Rupiah',
    skin: "round_plastic"
  });

  $("#transition-timer-carousel").on("slide.bs.carousel", function(event) {
    $(".transition-timer-carousel-progress-bar", this).removeClass("animate").css("width", "0%");
  }).on("slid.bs.carousel", function(event) {
    $(".transition-timer-carousel-progress-bar", this).addClass("animate").css("width", "100%");
  });

  $(".transition-timer-carousel-progress-bar", "#transition-timer-carousel").css("width", "100%");

  $('.featured-section .item').each(function(){
    var avaWidth = $(this).outerWidth() / 1;
    $(this).css('height', avaWidth);
  });
}

function xhrsData(){
  $('.provinces_select').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/cities?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.city-container').html(result);
        $('.city-container .select2').select2();
      })
    }
  });

  $('.provinces_select_tag').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/cities?id='+ $(this).val(), function(result){
        $('.city-container').html(result);
        $('.city-container .select2').select2();
      })
    }
  });

  // $('.shipping-method').each(function(){
  //   $(this).click(function(){
  //     var ship_method = $(this).val();
  //     $.get('/xhrs/shipment_provinces?name='+ship_method+'&node='+$(this).data("node"), function(result){
  //       $('.province-container').html(result);
  //       $('.province-container .select2').select2();
  //     })
  //   });
  // });

  $('.edit_order').on('change', '.provinces_select_shipment', function(){
    var ship_method = $('.shipping-method').filter(':checked').val();
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/shipment_cities?id='+$(this).val()+'&node='+$(this).data("node")+'&shipping_method='+ship_method, function(result){
        $('.city-container').html(result);
        $('.city-container .select2').select2();
      })
    }
  });

  $('.edit_order').on('change', '.cities_select_shipment', function(){
    var ship_method = $('.shipping-method').filter(':checked').val();
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/shipment_district?id='+$(this).val()+'&node='+$(this).data("node")+'&shipping_method='+ship_method, function(result){
        $('.district-container').html(result);
        $('.district-container .select2').select2();
      })
    }
  });

  $('.edit_order').on('click', '.get_estimation', function(){
    var origin = $('.js-origin-code').val();
    var ship_method = $('.shipping-method').filter(':checked').val();
    var province = $('.provinces_select_shipment').val();
    var city = $('.cities_select_shipment').val();
    var district = $('.districts_select_shipment').val();
    var weight = $('.weight').val();

    if(ship_method == undefined){
      alert('Gagal, Estimasi ongkir tidak dapat di hitung, karena anda belum menentukan jasa kurir yang digunakan!');
    }
    else if($('.districts_select_shipment').val() == ''){
      alert('Gagal, Estimasi ongkir tidak dapat di hitung, karena anda belum menentukan kecamatan yang dituju!');
    }else{
      $.get('/xhrs/shipping_price?shipping_method='+ship_method+'&province='+province+'&city='+city+'&district='+district+'&weight='+weight+'&origin='+origin, function(result){
        $('.shipping_price').html(result);
      });
    }
  });

  $('.dynamic_select').bind('change', function () {
    var url = $(this).find(':selected').data('url')
    if (url) {
        window.location = url;
    }
    return false;
  });

  $('.choice-gallery-form .input-file').on('change', function(index){
    if($(this).length){
      previewFile(this, $(this).parent('li').find('.show-choice'));
    }
  });

  $('.change-ava').on('change', function(index){
    if($(this).length){
      previewFile(this, $('.image-change'));
    }
  });

  $('.banner-change-ava').on('change', function(index){
    if($(this).length){
      previewFile(this, $('.banner-image-change'));
    }
  });

  $('.click-payment-method').click(function(){
    $('.paymentTo').show();
  });

  $( ".click-wishlist" ).each(function(index) {
    $(this).on("click", function(){
      $.ajax({
        url: $(this).attr("url"),
        type: 'get',
        dataType: 'html',
        async: false,
        success: function(data) {
            result = data;
        }
      });

      return $(this).parents('.section-whistlist').find('.status-whistlist').html('<i class="fa fa-star set active"></i>');
    });
  });

  $('.choice-category-select').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/sub_categories?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.sub-category-container').html(result);
        $('.sub-category-container .select2').select2();
      })
    }
  });

  $('.choice-category-catalog-select').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/sub_categories_catalog?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.sub-category-catalog-container').html(result);
        $('.sub-category-catalog-container .select2').select2();
      })
    }
  });

  $(document).on('click', '#check_all', function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
  });

  $('.category-point').click(function(){
    $(this).find('input').prop('checked', true);
    $('.sub-category-point input').prop('checked', false);
    $('.form-action-search').submit();
  });

  $('.sub-category-point').click(function(){
    $(this).find('input').prop('checked', true);
    $('.form-action-search').submit();
  });
}

function previewFile(input, imageHeader) {
  var preview = imageHeader[0];
  var file    = input.files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}

