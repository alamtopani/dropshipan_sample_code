//= require backend/jquery
//= require rails-ujs
//= require backend/bootstrap.min
//= require backend/jquery-ui.min
//= require backend/fullcalendar.min
//= require backend/jquery.rateit.min
//= require backend/jquery.prettyPhoto
//= require backend/jquery.slimscroll.min
//= require backend/jquery.dataTables.min
//= require backend/excanvas.min
//= require backend/jquery.flot
//= require backend/jquery.flot.resize
//= require backend/jquery.flot.pie
//= require backend/jquery.flot.stack
//= require backend/jquery.noty
//= require backend/themes/default
//= require backend/layouts/bottom
//= require backend/layouts/topRight
//= require backend/layouts/top
//= require backend/sparklines
//= require backend/jquery.cleditor.min
//= require plugins/bootstrap-datepicker.min
//= require backend/jquery.onoff.min
//= require backend/filter
//= require backend/custom
//= require backend/charts
//= require backend/select2
//= require ckeditor/init
//= require ckeditor/config
//= require autonumeric
//= require backend/configurations
//= require cocoon
//= require moment
//= require backend/jquery.doubleScroll
//= require chartkick


$(document).ready(function(){
  get_invoice_member();
	choiceCheckedTemplateWeb();

	$('.table-responsive').doubleScroll();
	$('#sample2').doubleScroll({resetOnWindowResize: true})

	$('.datepicker').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true
  });

	$('.dynamic_select').bind('change', function () {
		var url = $(this).find(':selected').parent('select').data('url');
		if (url) {
				window.location = url;
		}
		return false;
	});

	$(document).on('change', '.js-onchange-submit', function(){
		$(this).parent('.js-form-submit').trigger('submit.rails');
	});

	$('#check_all').click(function(){
		$('input:checkbox').not(this).prop('checked', this.checked);
	});

	$('.js-tab-analytics a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var ev = document.createEvent('Event');
    ev.initEvent('resize', true, true);
    window.dispatchEvent(ev);
  });

  $('.js-form-analytic button').on('click', function(){
    $(".spinner-load").show();
  });
});

function get_invoice_member(){
	$(document).on('change', '.invoice-member-selected', function() {
		if($(this).val() != '' && $(this).val() != 'undefined'){
			$.get('/xhrs/get_invoice_member?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
				$('.invoice-container').html(result);
			})
		}
	});
}

function choiceCheckedTemplateWeb(){
  $(document).on('click', '.template-web', function(){
    $('.template-web').removeClass('checked');
    $('.template-web').addClass('opacity');
    if($(this).find('input').prop("checked", true)){
      $(this).addClass('checked');
      $(this).removeClass('opacity');
    }
  });
}