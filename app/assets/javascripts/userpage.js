// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require frontend/jquery
//= require jquery_ujs
//= require plugins/jquery_ui
//= require plugins/doubleScroll
//= require plugins/bootstrap.min
//= require plugins/homer
//= require plugins/metisMenu.min
//= require plugins/icheck.min
//= require plugins/jquery.peity.min
//= require plugins/sparkline
//= require plugins/clipboard.min
//= require select2
//= require plugins/bootstrap-datepicker.min
//= require chartist/chartist-plugin-tooltip
//= require plugins/input-image
//= require plugins/input-image2
//= require plugins/jquery.blueimp-gallery.min
//= require autonumeric
//= require frontend/custom
//= require plugins/jquery.flexslider
//= require cocoon
//= require chartkick
//= require backend/jquery.doubleScroll



$(document).ready(function() {
	$('.table-responsive').doubleScroll();
	$('#sample2').doubleScroll({resetOnWindowResize: true})
	
	$('.datepicker').datepicker({
		format: "yyyy-mm-dd",
		autoclose: true
	});

	$(document).on('change', '.js-onchange-submit', function(){
		$(this).parent('.js-form-submit').trigger('submit.rails');
	});

	$('.choice-gallery-form .input-file').on('change', function(index){
    if($(this).length){
      previewFile(this, $(this).parent('li').find('.show-choice'));
    }
  });
	
	xhrsData();
	copyClipboard();
	choiceCheckedTemplateWeb();
	charFromBottom();
	checkedproduc();
  loadMoreScroll();

	$(document).on('click', '#check_all', function(){
		$('input:checkbox').not(this).prop('checked', this.checked);
	});

	$('.choice-category-select').on('change', function(){
		if($(this).val() != '' && $(this).val() != 'undefined'){
			$.get('/xhrs/sub_categories?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
				$('.sub-category-container').html(result);
				$('.sub-category-container .select2').select2();
			})
		}
	});

	$('.choice-category-catalog-select').on('change', function(){
		if($(this).val() != '' && $(this).val() != 'undefined'){
			$.get('/xhrs/sub_categories_catalog?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
				$('.sub-category-catalog-container').html(result);
				$('.sub-category-catalog-container .select2').select2();
			})
		}
	});

	$('.category-point').click(function(){
		$(this).find('input').prop('checked', true);
		$('.sub-category-point input').prop('checked', false);
		$('.form-action-search').submit();
	});

	$('.sub-category-point').click(function(){
		$(this).find('input').prop('checked', true);
		$('.form-action-search').submit();
	});

	$('.js-tab-analytics a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var ev = document.createEvent('Event');
		ev.initEvent('resize', true, true);
		window.dispatchEvent(ev);
	});

	$('.js-form-analytic button').on('click', function(){
		$(".spinner-load").show();
	});

  $(".to_tags").select2({ multiple: true });
});

function xhrsData(){
	$('.provinces_select').on('change', function(){
		if($(this).val() != '' && $(this).val() != 'undefined'){
			$.get('/xhrs/cities?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
				$('.city-container').html(result);
				$('.city-container .select2').select2();
			})
		}
	});

	$('.provinces_select_tag').on('change', function(){
		if($(this).val() != '' && $(this).val() != 'undefined'){
			$.get('/xhrs/cities?id='+ $(this).val(), function(result){
				$('.city-container').html(result);
				$('.city-container .select2').select2();
			})
		}
	});
}

function copyClipboard(){
	$('.clipboard-btn').click(function(){
		var clipboard = new Clipboard($(this)[0]);
	});
}

function choiceCheckedTemplateWeb(){
	$(document).on('click', '.template-web', function(){
		$('.template-web').removeClass('checked');
		$('.template-web').addClass('opacity');
		if($(this).find('input').prop("checked", true)){
			$(this).addClass('checked');
			$(this).removeClass('opacity');
		}
	});
}


function charFromBottom(){
	var charFromBottom = $('.chat-discussion');

	if(charFromBottom.length){
		charFromBottom[0].scrollTop = charFromBottom[0].scrollHeight;
	}
}

function checkedproduc(){
	$('input:checkbox').change(function(){
    if($(this).is(":checked")) {
      $('.checkproduc').addClass("checkproduc-checked");
      $('.overplay').removeClass("checkproduc-checked");
    }else{
      $('.checkproduc').removeClass("checkproduc-checked");
      $('.overplay').removeClass("checkproduc-checked");
    }
	});

	$(".checkproduc").each(function () {
	  if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
	    $(this).addClass('checkproduc-checked');
	    $('.overplay').removeClass("checkproduc-checked");
	  }
	  else {
	    $(this).removeClass('checkproduc-checked');
	    $('.overplay').removeClass("checkproduc-checked");
	  }
	});

	// sync the state to the input
	$(".checkproduc").on("click", function (e) {
	  $(this).toggleClass('checkproduc-checked');
	  var $checkbox = $(this).find('input[type="checkbox"]');
	  $checkbox.prop("checked",!$checkbox.prop("checked"))
	  e.preventDefault();
	});
}

loadMoreScroll = function(){
  if ($('.load-more-scroll .pagination')) {
    $(window).scroll(function() {
      url = $('.load-more-scroll .pagination a').attr('href');
      if (url && $(window).scrollTop() > $(document).height() - $(window).height() - 150) {
        $('.load-more-scroll .pagination').html("<p>LOADING MORE ...</p><br><br>");
        $.getScript(url)
      }
    });
  }
}

function previewFile(input, imageHeader) {
  var preview = imageHeader[0];
  var file    = input.files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}