class RegistrationsController < Devise::RegistrationsController
	before_action :configure_permitted_parameters
	
	def create
    domain_name = params[:domain_name]
    phone = params[:user][:profile_attributes][:phone]
    if domain_name.blank?
      redirect_to new_user_registration_path, alert: 'Nama Domain anda tidak di isi, harap di isi terlebih dahulu!'
    elsif check_domain(domain_name).present?
      redirect_to new_user_registration_path, alert: 'Maaf Nama Domain yang anda masukan telah dipergunakan oleh orang lain, silahkan pilih Nama Domain lain!'
    elsif check_domain_name_valid(domain_name).present?
      redirect_to new_user_registration_path, alert: 'Maaf Nama Domain yang anda masukan tidak valid (tidak boleh menggunakan spasi, titik, koma Dll) silahkan pilih Nama Domain lain!'
    elsif phone.include?("+62").blank?
      redirect_to new_user_registration_path, alert: 'Maaf Nomor HP Aktif/Whatsapp/Line tidak menggunakkan +62, mohon untuk menggunakkan format +62 dalam pengisian Nomor HP Aktif/Whatsapp/Line!'
    else
  		super do
        resource.type = 'Member'
        if cookies[:referral].present?
          user_referral = User.find_by(code: cookies[:referral])
          if user_referral.present?
            if user_referral.member?
              resource.check_referral_top(cookies[:referral])
            else
              resource.account_manager_id = user_referral.id
            end
          end
          
        end
  			if resource.save
          if cookies[:referral].present?
            cookies.delete :referral
          end
          
          member = Member.find(resource.id)
          # ------- UPDATE DOMAIN SUB -------
          site_info = member.site_info
          site_info.domain_name = params[:domain_name]
          site_info.save

          # ------- UPDATE WEB SETTING -------
          # web_setting = member.web_setting
          # web_setting.title = params[:title]
          # web_setting.footer_tags = "© #{web_setting.title}. All Rights Reserved."
          # web_setting.save


          today_date = Time.zone.now.to_date
  				payment = WebSetting.first.payments.first
  				invoice = Invoice.create(user_id: resource.id, 
            											subject: "Invoice #{resource.package.title}", 
            											description: "Invoice #{resource.package.title}", 
            											price: resource.package.price, 
            											bank_name: payment.try(:bank_name), 
            											bank_branch: "Sukabumi", 
            											account_name: payment.try(:name), 
            											account_number: payment.try(:account_number), 
            											invoiceable_id: resource.package.id, 
            											invoiceable_type: resource.package.class.name,
            											already_send: true
            											)

          # ------- UPDATE CONVERTION -------
          if cookies[:s3].present? && cookies[:s4].present?
            convertion = Convertion.new
            convertion.click_id = cookies[:s4]
            convertion.pub_id = cookies[:s3].split('-')[1]
            convertion.campaign_id = cookies[:s3].split('-')[2]
            convertion.invoice_id = invoice.id
            convertion.country_code = 'ID'
            convertion.payout = 0

            if convertion.save
              cookies.delete :s3
              cookies.delete :s4
            end
          end

          # -------AFFILIATE REFERRAL-------
          cpa = cookies[:s3].present? && cookies[:s4].present? ? true : false
          member.check_referral_one(member.ref_id1, cpa, invoice.id)

  				UserMailer.send_invoice_package(resource).deliver
          create_activity(invoice, resource.id, "Tagihan Paket", "#{resource.username} melakukkan pendaftaran paket '#{resource.package.title}'.")
  			end
  		end
    end
	end

	protected

    def after_inactive_sign_up_path_for(resource)
      thank_page_path(resource)
    end

		def configure_permitted_parameters
			devise_parameter_sanitizer.permit(:sign_up) { |u|
				u.permit(
									:username, 
									:email, 
									:password, 
									:password_confirmation, 
									:package_id,
                  :domain_name,
                  :title,
									profile_attributes: [
										:id,
										:user_id,
										:full_name,
										:birthday,
										:gender,
										:phone,
                    :bank_name,
                    :bank_branch,
                    :account_name,
                    :account_number,
										:avatar,
									]
								)
			}
			devise_parameter_sanitizer.permit(:sign_in) { |u| 
				u.permit(:login, :username, :email, :password, :remember_me) 
			}
		end

    def check_domain(domain)
      members = SiteInfo.where(domain_name: domain)
    end

    def check_domain_name_valid(domain)
      if domain.include?('.') || domain.include?(',') || domain.include?(' ')
        return true
      else
        return false
      end
    end

end