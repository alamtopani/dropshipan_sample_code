class OrdersController < FrontendController
	add_breadcrumb "Home", :root_path
	include Ongkir
	
	def index
		add_breadcrumb "My Order Items", orders_path

		@order = current_order
		@stakeholders = @order.stakeholder_orders.latest
		@shipping_methods = ShippingMethod.activated

		get_region_ongkir_province
	end

	def checkout
		if current_order.order_items.present? && current_order.order_items.first.product.merchant.featured == true
			@origin_name = current_order.order_items.first.product.merchant.address.place_city_name_merchant
			@origin_code = current_order.order_items.first.product.merchant.address.place_city_code_merchant
		else
			@origin_name = 'Kota Sukabumi'
			@origin_code = 431
		end

		params[:order][:courier] = 'JASA PENGIRIMAN'

		prepare_shipping_price(params[:order][:shipping_method], params[:order][:address_attributes][:district], current_order.total_weight?, @origin_code)
		if @price <= 0
			redirect_to orders_path, alert: "Pengiriman ke kecamatan #{params[:order][:address_attributes][:district]} tidak tersedia, silahkan pilih kecamatan lain!"
		else
			if order = Order.pay(@subdomain, current_order, prepare_shipping_address, @price, false)
				if order.update(permitted_params)
					session[:order_id] = nil if current_order
					update_address(order)
					UserMailer.order_checkout(order).deliver
          create_activity(order, @subdomain.id, "Pesanan baru dibuat", "Tujuan pengiriman ke #{order.payer_address}")
					redirect_to review_order_path(order.payment_token), notice: 'Terima kasih telah memesan produk kami, berikut ini adalah faktur dari keranjang belanja Anda yang telah berhasil dipesan, silakan lakukan pembayaran segera dan konfirmasi pembayaran Anda kepada kami!'
				end
			else
				redirect_to orders_path, alert: 'Something went wrong. Please try again.'
			end
		end
	end

	def review
		add_breadcrumb "Telah Sukses Melakukan Pemesanan"

		@order = Order.find_by(payment_token: params[:id])
		if @order.blank?
			@order = Order.find_by(code: params[:id])
		end
		@stakeholders = @order.stakeholder_orders.latest
		
		unless @order
			redirect_to root_path, notice: 'You do not have permission to access this page'
		end

		respond_to do |format|
			format.html
			format.pdf do
				render pdf: "Invoice Order - #{@order.code}",
				layout: 'layouts/print_layout.html.erb',
				show_as_html: params[:debug].present?
			end
		end
	end

	private
		def permitted_params
			params.require(:order).permit(Permitable.controller(params[:controller]))
		end

		def prepare_shipping_address
			address = [
				params[:order][:address_attributes][:province].split(':')[1],
				params[:order][:address_attributes][:city].split(':')[1],
				params[:order][:address_attributes][:district].split(':')[1],
				params[:order][:address_attributes][:address],
				params[:order][:address_attributes][:postcode]
			].select(&:'present?').join(', ')
		end

		def update_address(order)
			order.origin_code = @origin_code
			order.origin_name = @origin_name
			order.save

			address = order.address
			address.province = params[:order][:address_attributes][:province].split(':')[1]
			address.city = params[:order][:address_attributes][:city].split(':')[1]
      address.district = params[:order][:address_attributes][:district].split(':')[1]
			address.district_full = params[:order][:address_attributes][:district]
			address.save
		end
end
