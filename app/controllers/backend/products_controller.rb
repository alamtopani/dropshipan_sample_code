class Backend::ProductsController < Backend::ApplicationController
  defaults resource_class: Product, collection_name: 'products', instance_name: 'product'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Products", :collection_path

  include MultipleAction

  def index
    @verifieds = collection.verify.size 
    @not_verifieds = collection.unverify.size 
    @featureds = collection.featured.size
    @collection = collection.latest.page(page).per(per_page)
    
    if params[:export] == 'true'
      @collection_all = collection
    end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def create
    build_resource
    create! do |format|
      if resource.errors.empty?
        update_product_stocks(resource.id)
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to request.referer || root_path}
      end
    end
  end

  def update
    update! do |format|
      if resource.errors.empty?
        update_product_stocks(resource.id)
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to request.referer || root_path}
      end
    end
  end

  protected
    def update_product_stocks(product_id)
      product_spec = ProductSpec.find(product_id)
      product_spec.stock = ProductStock.where(product_id: product_id).sum(:stock)
      product_spec.save
    end

    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
