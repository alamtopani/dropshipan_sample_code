class Backend::RequestLandingPressesController < Backend::ApplicationController
	defaults resource_class: RequestLandingPress, collection_name: 'request_landing_presses', instance_name: 'request_landing_press'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Request Landing Page", :collection_path

  def index
  	@collection = collection.latest.page(page).per(per_page)
  end

  protected
  	def collection
  		@collection ||= end_of_association_chain.search_by(params)
  	end
end