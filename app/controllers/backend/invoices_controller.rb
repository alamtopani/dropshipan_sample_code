class Backend::InvoicesController < Backend::ApplicationController
  defaults resource_class: Invoice, collection_name: 'invoices', instance_name: 'invoice'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Invoices", :collection_path

  include MultipleAction

  def index
    @paid = collection.paid.sum(:price)
    @pending = collection.pending.sum(:price)
    @promo = collection.active_promo.sum(:price)
    @collection = collection.latest.page(page).per(per_page)

    if params[:export] == 'true'
      @collection_all = collection
    end

    collection.where(status: "unpaid").each do |resource|
      resource.status = "pending"
      resource.save
    end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end

  end

  def show
    @activities = resource.activities.latest.search_by(params).page(params[:page_activities]).per(per_page)
    
    respond_to do |format|
      format.html
      format.js
      format.pdf do
        render pdf: "Tagihan Paket - #{helpers.get_date(resource.period_start)} s.d. #{helpers.get_date(resource.period_end)}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
