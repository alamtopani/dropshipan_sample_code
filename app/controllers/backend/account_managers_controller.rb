class Backend::AccountManagersController < Backend::ApplicationController
  defaults resource_class: AccountManager, collection_name: 'account_managers', instance_name: 'account_manager'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Account Managers", :collection_path

  include MultipleAction

  def index
    @verifieds = collection.verified.size 
    @not_verifieds = collection.not_verified.size 
    @featureds = collection.featured.size
    @collection = collection.latest.page(page).per(per_page)

    if params[:export] == 'true'
      @collection_all = collection
    end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def show
    @members = resource.members.latest.search_by(params).page(params[:page_members]).per(per_page)
    @catalogs = resource.catalogs.latest.search_by(params).page(params[:page_catalogs]).per(per_page)
    @orders = resource.orders.latest.search_by(params).page(params[:page_orders]).per(per_page)
    @confirmations = resource.confirmations.latest.search_by(params).page(params[:page_confirmations]).per(per_page)
    @commissions = resource.commissions.latest.search_by(params).page(params[:page_commissions]).per(per_page)
    @payment_snapshots = resource.payment_snapshots.latest.search_by(params).page(params[:page_payment_snapshots]).per(per_page)
    @activities = resource.activities.latest.search_by(params).page(params[:page_activities]).per(per_page)
    @collection_all = collection

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
