class Backend::RecapPaymentsController < Backend::ApplicationController

  add_breadcrumb "Dashboard", :backend_dashboard_path
  
  def index
    add_breadcrumb "Rekap Pembayaran #{params[:user]}", "#{backend_recap_payments_path(user: params[:user])}"

    if params[:start_at].blank? && params[:end_at].blank?
      set_default_date_zone
      if @date_zone >= @four && @date_zone <= @eighteen
        params[:start_at] = @last_month.beginning_of_month + 15.days
        params[:end_at] = @last_month.end_of_month
      else
        params[:start_at] = @beginning_of_month
        params[:end_at] = @beginning_of_month + 14.days
      end
    end

    if params[:user] == "Member"
      commission_group_by = Commission.search_by(params).all_approved.group_by{|r| r.member_id}
      commissions = commission_group_by.values.map{ |r| [r.last.member_id, r.map{|s| s.commission_member}.reduce(:+)] }
      @commissions = Kaminari.paginate_array(commissions).page(page).per(per_page)
    elsif params[:user] == "Manager"
      commission_group_by = Commission.search_by(params).all_approved.group_by{|r| r.account_manager_id}
      commissions = commission_group_by.values.map{ |r| [r.last.account_manager_id, r.map{|s| s.commission_am}.reduce(:+), 0] }
      @commissions = Kaminari.paginate_array(commissions).page(page).per(per_page)
    elsif params[:user] == "Referral"
      commission_group_by = CommissionReferral.search_by(params).all_approved.group_by{|r| r.member_id}
      commissions = commission_group_by.values.map{ |r| [r.last.member_id, r.map{|s| s.commission_value}.reduce(:+)] }
      @commissions = Kaminari.paginate_array(commissions).page(page).per(per_page)
    else 
      commission_group_by = StakeholderOrder.eager_load(:order).track_shipped_received.search_by(params).group_by{|r| r.merchant_id}
      commissions = commission_group_by.values.map{ |r| [r.last.merchant_id, r.map{|s| s.basic_price}.reduce(:+), r.map{|s| s.merchant.status_featured? ? s.order.shipping_price : 0}.reduce(:+), r.map{|s| s.merchant.status_featured? ? s.order.total_basic_price : s.total_basic_price}.reduce(:+)] }
      @commissions = Kaminari.paginate_array(commissions).page(page).per(per_page)
    end

    respond_to do |format|
      format.html
      format.js
    end
  end

  private
    def set_default_date_zone
      @date_zone = Time.zone.now.to_date
      @last_month = (@date_zone.beginning_of_month - 1.month)
      @next_month = (@date_zone.beginning_of_month + 1.month)
      @beginning_of_month = @date_zone.beginning_of_month
      @end_of_month = @date_zone.end_of_month

      @nineteen = @beginning_of_month + 18.days 
      @four = @beginning_of_month + 3.days 
      @three_next_month = @next_month.beginning_of_month + 2.days 
      @eighteen = @beginning_of_month + 17.days
    end

end