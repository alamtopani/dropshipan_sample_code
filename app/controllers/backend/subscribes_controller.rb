class Backend::SubscribesController < Backend::ApplicationController
  defaults resource_class: Subscribe, collection_name: 'subscribes', instance_name: 'subscribe'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Subscribes", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
