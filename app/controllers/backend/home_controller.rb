class Backend::HomeController < Backend::ApplicationController
  add_breadcrumb "Dashboard", :backend_dashboard_path

  def dashboard
    @date = Time.zone.now.to_date
    if params[:start_at].blank?
      @total_income_affiliates = Affiliate.where("DATE(affiliates.created_at) >=?", @date.to_date).where("DATE(affiliates.created_at) <=?", @date.to_date).where(status: ['paid', 'process']).search_by(params).pluck(:fee_company).sum
    else
      @total_income_affiliates = Affiliate.where(status: ['paid', 'process']).search_by(params).pluck(:fee_company).sum
    end
    order_default = Order.without_in_progress
    if current_user.checker_2?
      order_default = order_default.track_verification_packing_cancelled
    elsif current_user.shipment?
      order_default = order_default.paid.track_packing_shipped
    elsif current_user.checker_3?
      order_default = order_default.paid.track_shipped_received
    end
    order_report_day(order_default, 'with_search_by', :price, params[:export])
  end
end
