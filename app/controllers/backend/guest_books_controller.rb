class Backend::GuestBooksController < Backend::ApplicationController
	defaults resource_class: GuestBook, collection_name: 'guest_books', instance_name: 'guest_book'

	add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Buku Tamu", :collection_path

  def index
  	@collection = collection.where(user_id: Admin.first.id).latest.page(page).per(per_page)
  end

  def show
    if resource.present?
      resource.status = true
      resource.save
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
