class Backend::ReportRefundsController < Backend::ApplicationController
  defaults resource_class: ReportRefund, collection_name: 'report_refunds', instance_name: 'report_refund'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Refund Orders", :collection_path

  include MultipleAction

  def index
    @collection_pending = collection.pending.sum(:total)
    @collection_process = collection.process.sum(:total)
    @collection_paid = collection.paid.sum(:total)
    @collection_cancelled = collection.cancelled.sum(:total)

    @collection = collection.latest.page(page).per(per_page)
    
    if params[:export] == 'true'
      @collection_all = collection
    end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def show
    @order = resource.order
    @confirmation = Confirmation.where("LOWER(confirmations.no_invoice) LIKE LOWER('#{@order.code}')").first
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
