class Backend::LandingPressesController < Backend::ApplicationController
	defaults resource_class: LandingPress, collection_name: 'landing_presses', instance_name: 'landing_press'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Landing Page", :collection_path

  include MultipleAction

  def index
  	@collection = collection.latest.page(page).per(per_page)
  end

  protected
  	def collection
  		@collection ||= end_of_association_chain.search_by(params)
  	end
end