class Backend::PaymentReferralsController < Backend::ApplicationController
  defaults resource_class: PaymentReferral, collection_name: 'payment_referrals', instance_name: 'payment_referral'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Payment Referrals", :collection_path

  include MultipleAction

  def index
    @collection_pending = collection.pending.sum(:commission_referral)
    @collection_process = collection.process.sum(:commission_referral)
    @collection_paid = collection.paid.sum(:commission_referral)
    @collection_rejects = collection.rejects.sum(:commission_referral)

    if params[:export] == 'true'
      @collection_all = collection.latest
    end
    
    @collection = collection.latest.page(page).per(per_page)

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def show
    @activities = resource.activities.latest.search_by(params).page(params[:page_activities]).per(per_page)
    
    respond_to do |format|
      format.html
      format.js
      format.pdf do
        render pdf: "Pembayaran referral - #{helpers.get_date(resource.payment_start)} s.d. #{helpers.get_date(resource.payment_end)}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  def create
    member_id = params[:payment_referral][:member_id]

    member = Member.find member_id
    payment_start = params[:payment_referral][:payment_start]
    payment_end = params[:payment_referral][:payment_end]
    status = params[:payment_referral][:status]
    from_acc = params[:from_acc]
    to_acc = params[:to_acc]
    message = params[:message]
    
    @payment_referral = build_resource
    @payment_referral.admin_id = current_user.id

    commission_referrals = CommissionReferral.where("commission_referrals.member_id =? AND DATE(commission_referrals.commission_date) >=? AND DATE(commission_referrals.commission_date) <=? AND commission_referrals.status =?", member_id, payment_start, payment_end, CommissionReferral::APPROVED)
    @to_acc = member.profile.payment_to

    @payment_referral.commission_referral = '%.4f' % commission_referrals.sum(:commission_value)
    @payment_referral.from_acc = from_acc
    @payment_referral.to_acc = to_acc
    @payment_referral.message = message
    @payment_referral.status = status
    
    orders = member.orders.where("DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=? AND orders.order_status_id =?", payment_start, payment_end, 3)
    total_order = '%.4f' % orders.sum(:total_price)
    if total_order.to_f >= PaymentReferral::ORDER_TARGET
      @payment_referral.payment_pending = false
    else
      @payment_referral.quote = PaymentReferral::QUOTE
    end

    if @payment_referral.save
      # @payment_referral.total_delay = member.payment_referrals.delay.count
      @payment_referral.save
      create_activity(@payment_referral, current_user.id, "Pembayaran Referral (#{@payment_referral.member.username})", "#{current_user.username} membuat pembayaran periode #{@payment_referral.payment_period?} sebesar #{@payment_referral.get_commission_referral?}.")
      redirect_to backend_payment_referrals_path, notice: "Successfully created payment referrals"
    else
      redirect_to backend_payment_referrals_path, alert: "Unsuccessfully created payment referrals, #{@payment_referral.errors.full_messages.join(',')}"
    end
  end

  def update
    resource
    member_id = @payment_referral.member_id

    member = Member.find member_id
    payment_start = params[:payment_referral][:payment_start]
    payment_end = params[:payment_referral][:payment_end]
    status = params[:payment_referral][:status]
    from_acc = params[:from_acc].present? ? params[:from_acc] : params[:payment_referral][:from_acc]
    to_acc = params[:to_acc].present? ? params[:to_acc] : params[:payment_referral][:to_acc]
    message = params[:message].present? ? params[:message] : params[:payment_referral][:message]

    if Time.zone.now.to_date > payment_end.to_date
      if check_data_update(member_id, payment_start, payment_end).present?
        @payment_referral.admin_id = current_user.id

        commission_referrals = CommissionReferral.where("commission_referrals.member_id =? AND DATE(commission_referrals.commission_date) >=? AND DATE(commission_referrals.commission_date) <=? AND commission_referrals.status =?", member_id, payment_start, payment_end, CommissionReferral::APPROVED)
        @to_acc = member.profile.payment_to

        @payment_referral.commission_referral = '%.4f' % commission_referrals.sum(:commission_value)
        @payment_referral.from_acc = from_acc
        @payment_referral.to_acc = to_acc
        @payment_referral.message = message
        @payment_referral.status = status
        if @payment_referral.save
          redirect_to backend_payment_referrals_path, notice: "Successfully created payment referrals"
        else
          redirect_to backend_payment_referrals_path, alert: "Unsuccessfully created payment referrals, #{@payment_referral.errors.full_messages.join(',')}"
        end
        
      else
        redirect_back(fallback_location: backend_payment_referrals_path, alert: "Unsuccessfully updated #{@resource_name} because #{member.username} period #{payment_start} - #{payment_end} has never entered!")
      end
    else
      redirect_back(fallback_location: backend_payment_referrals_path, alert: "Unsuccessfully updated #{@resource_name} because payment_end selected is bigger than this day!")
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end

    def check_data_update(member_id, payment_start, payment_end)
      payment_referral = PaymentReferral.where("payment_referrals.member_id =? AND payment_referrals.payment_start =? AND payment_referrals.payment_end =?", member_id, payment_start, payment_end).where.not(status: PaymentReferral::PAID)
      return payment_referral
    end
end

