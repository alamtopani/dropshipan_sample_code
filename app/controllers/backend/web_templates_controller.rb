class Backend::WebTemplatesController < Backend::ApplicationController
  defaults resource_class: WebTemplate, collection_name: 'web_templates', instance_name: 'web_template'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Web Templates", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
