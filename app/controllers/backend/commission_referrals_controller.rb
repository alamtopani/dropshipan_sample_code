class Backend::CommissionReferralsController < Backend::ApplicationController
  defaults resource_class: CommissionReferral, collection_name: 'commission_referrals', instance_name: 'commission_referral'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Komisi Referral", :collection_path

  include MultipleAction

  def index
    commission_report_day(collection.all, 'with_search_by', :commission_value, 'referral', params[:export])

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
