class Backend::PaymentSnapshotsController < Backend::ApplicationController
  defaults resource_class: PaymentSnapshot, collection_name: 'payment_snapshots', instance_name: 'payment_snapshot'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Payment Snapshots", :collection_path

  include MultipleAction

  def index
    @collection_pending = collection.pending.sum(:price)
    @collection_process = collection.process.sum(:price)
    @collection_paid = collection.paid.sum(:price)
    @collection_rejects = collection.rejects.sum(:price)

    if params[:export] == 'true'
      @collection_all = collection.latest
    end
    
    @collection = collection.latest.page(page).per(per_page)

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def show
    @activities = resource.activities.latest.search_by(params).page(params[:page_activities]).per(per_page)
    
    respond_to do |format|
      format.html
      format.js
      format.pdf do
        render pdf: "Pembayaran - #{helpers.get_date(resource.payment_start)} s.d. #{helpers.get_date(resource.payment_end)}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  def create
    user_id = params[:payment_snapshot][:user_id]

    user = User.find user_id
    payment_start = params[:payment_snapshot][:payment_start]
    payment_end = params[:payment_snapshot][:payment_end]
    status = params[:payment_snapshot][:status]
    from_acc = params[:from_acc]
    to_acc = params[:to_acc]
    message = params[:message]
    
    @payment_snapshot = build_resource
    @payment_snapshot.admin_id = current_user.id

    if user.member?
      commissions = Commission.where("commissions.member_id =? AND DATE(commissions.commission_date) >=? AND DATE(commissions.commission_date) <=? AND commissions.status =?", user_id, payment_start, payment_end, Commission::APPROVED)
      @to_acc = Member.find(user_id).profile.payment_to
    elsif user.account_manager?
      commissions = Commission.where("commissions.account_manager_id =? AND DATE(commissions.commission_date) >=? AND DATE(commissions.commission_date) <=? AND commissions.status =?", user_id, payment_start, payment_end, Commission::APPROVED)
      @to_acc = AccountManager.find(user_id).profile.payment_to
    end

    @payment_snapshot.fee_member = '%.4f' % commissions.sum(:commission_member)
    @payment_snapshot.fee_am = '%.4f' % commissions.sum(:commission_am)
    @payment_snapshot.fee_company = '%.4f' % commissions.sum(:commission_company)
    @payment_snapshot.total = @payment_snapshot.fee_member.to_f+@payment_snapshot.fee_am.to_f+@payment_snapshot.fee_company.to_f

    if @payment_snapshot.user.member?
      @payment_snapshot.price = @payment_snapshot.fee_member.to_f
    else
      @payment_snapshot.price = @payment_snapshot.fee_am
    end

    @payment_snapshot.from_acc = from_acc
    @payment_snapshot.to_acc = to_acc
    @payment_snapshot.message = message
    @payment_snapshot.status = status
    if @payment_snapshot.save
      create_activity(@payment_snapshot, current_user.id, "Pembayaran #{@payment_snapshot.user_type} (#{@payment_snapshot.user.username})", "#{current_user.username} membuat pembayaran periode #{@payment_snapshot.payment_period?} sebesar #{@payment_snapshot.get_price?}.")
      redirect_to backend_payment_snapshots_path, notice: "Successfully created payment snapshots"
    else
      redirect_to backend_payment_snapshots_path, alert: "Unsuccessfully created payment snapshots, #{@payment_snapshot.errors.full_messages.join(',')}"
    end
  end

  def update
    resource
    user_id = @payment_snapshot.user_id

    user = User.find user_id
    payment_start = params[:payment_snapshot][:payment_start]
    payment_end = params[:payment_snapshot][:payment_end]
    status = params[:payment_snapshot][:status]
    from_acc = params[:from_acc].present? ? params[:from_acc] : params[:payment_snapshot][:from_acc]
    to_acc = params[:to_acc].present? ? params[:to_acc] : params[:payment_snapshot][:to_acc]
    message = params[:message].present? ? params[:message] : params[:payment_snapshot][:message]

    if Time.zone.now.to_date > payment_end.to_date
      if check_data_update(user_id, payment_start, payment_end).present?
        @payment_snapshot.admin_id = current_user.id

        if user.member?
          commissions = Commission.where("commissions.member_id =? AND DATE(commissions.commission_date) >=? AND DATE(commissions.commission_date) <=? AND commissions.status =?", user_id, payment_start, payment_end, Commission::APPROVED)
          @to_acc = Member.find(user_id).profile.payment_to
        elsif user.account_manager?
          commissions = Commission.where("commissions.account_manager_id =? AND DATE(commissions.commission_date) >=? AND DATE(commissions.commission_date) <=? AND commissions.status =?", user_id, payment_start, payment_end, Commission::APPROVED)
          @to_acc = AccountManager.find(user_id).profile.payment_to
        end

        @payment_snapshot.fee_member = '%.4f' % commissions.sum(:commission_member)
        @payment_snapshot.fee_am = '%.4f' % commissions.sum(:commission_am)
        @payment_snapshot.fee_company = '%.4f' % commissions.sum(:commission_company)
        @payment_snapshot.total = @payment_snapshot.fee_member.to_f+@payment_snapshot.fee_am.to_f+@payment_snapshot.fee_company.to_f

        if @payment_snapshot.user.member?
          @payment_snapshot.price = @payment_snapshot.fee_member.to_f
        else
          @payment_snapshot.price = @payment_snapshot.fee_am
        end

        @payment_snapshot.from_acc = from_acc
        @payment_snapshot.to_acc = to_acc
        @payment_snapshot.message = message
        @payment_snapshot.status = status
        if @payment_snapshot.save
          redirect_to backend_payment_snapshots_path, notice: "Successfully created payment snapshots"
        else
          redirect_to backend_payment_snapshots_path, alert: "Unsuccessfully created payment snapshots, #{@payment_snapshot.errors.full_messages.join(',')}"
        end
        
      else
        redirect_back(fallback_location: backend_payment_snapshots_path, alert: "Unsuccessfully updated #{@resource_name} because #{user.username} period #{payment_start} - #{payment_end} has never entered!")
      end
    else
      redirect_back(fallback_location: backend_payment_snapshots_path, alert: "Unsuccessfully updated #{@resource_name} because payment_end selected is bigger than this day!")
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end

    def check_data_update(user_id, payment_start, payment_end)
      payment_snapshot = PaymentSnapshot.where("payment_snapshots.user_id =? AND payment_snapshots.payment_start =? AND payment_snapshots.payment_end =?", user_id, payment_start, payment_end).where.not(status: PaymentSnapshot::PAID)
      return payment_snapshot
    end
end

