class Backend::SpendingsController < Backend::ApplicationController
  defaults resource_class: Spending, collection_name: 'spendings', instance_name: 'spending'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Pengeluaran", :collection_path

  include MultipleAction

  def index
    @pending = collection.pending.sum(:price)
    @process = collection.process.sum(:price)
    @paid = collection.paid.sum(:price)
    @reject = collection.rejects.sum(:price)

    if params[:export] == 'true'
      @collection_all = collection
    end
    @collection = collection.latest.page(page).per(per_page)

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
