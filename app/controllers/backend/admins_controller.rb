class Backend::AdminsController < Backend::ApplicationController
  defaults resource_class: Admin, collection_name: 'admins', instance_name: 'admin'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Admins"

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to request.referer || root_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to request.referer || root_path}
      end
    end
  end
end
