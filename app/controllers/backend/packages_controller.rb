class Backend::PackagesController < Backend::ApplicationController
  defaults resource_class: Package, collection_name: 'packages', instance_name: 'package'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Packages", :collection_path

  include MultipleAction

  def index
    @collection = collection.latest.page(page).per(per_page)
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
