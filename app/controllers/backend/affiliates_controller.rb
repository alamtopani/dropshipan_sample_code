class Backend::AffiliatesController < Backend::ApplicationController
	defaults resource_class: Affiliate, collection_name: 'affiliates', instance_name: 'affiliate'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Affiliate", :collection_path

  include MultipleAction

  def index
  	@collection = collection.latest.page(page).per(per_page)

    @basic_price = collection.sum(:basic_price)
    @sale_price = collection.sum(:sale_price)
    @fee_company = collection.sum(:fee_company)
    @fee_member = collection.sum(:fee_member)
    @fee_affiliate = collection.sum(:fee_affiliate)

    if params[:export] == 'true'
      @collection_all = collection
    end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  protected
  	def collection
  		@collection ||= end_of_association_chain.search_by(params)
  	end
end