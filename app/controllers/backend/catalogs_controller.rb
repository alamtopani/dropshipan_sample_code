class Backend::CatalogsController < Backend::ApplicationController
  defaults resource_class: Catalog, collection_name: 'catalogs', instance_name: 'catalog'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Catalogs", :collection_path

  include MultipleAction

  def index
    @actives = collection.approved.size 
    @inactives = collection.inactive.size 
    @pendings = collection.pending.size
    @collection = collection.latest.page(page).per(per_page)

    if params[:export] == 'true'
      @collection_all = collection
    end
    
    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def create
    build_resource
    create! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to request.referer || root_path}
      end
    end
  end

  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to request.referer || root_path}
      end
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
