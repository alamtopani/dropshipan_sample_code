class Backend::PaymentMerchantsController < Backend::ApplicationController
  defaults resource_class: PaymentMerchant, collection_name: 'payment_merchants', instance_name: 'payment_merchant'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Payment Merchant", :collection_path

  include MultipleAction

  def index
    @collection_pending = collection.pending.sum(:total)
    @collection_process = collection.process.sum(:total)
    @collection_paid = collection.paid.sum(:total)
    @collection_rejects = collection.rejects.sum(:total)

    if params[:export] == 'true'
      @collection_all = collection
    end
    
    @collection = collection.latest.page(page).per(per_page)

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def show
    @activities = resource.activities.latest.search_by(params).page(params[:page_activities]).per(per_page)
    
    respond_to do |format|
      format.html
      format.js
      format.pdf do
        render pdf: "Pembayaran - #{helpers.get_date(resource.payment_start)} s.d. #{helpers.get_date(resource.payment_end)}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  def create
    merchant_id = params[:payment_merchant][:merchant_id]

    merchant = Merchant.find merchant_id
    payment_start = params[:payment_merchant][:payment_start]
    payment_end = params[:payment_merchant][:payment_end]
    status = params[:payment_merchant][:status]
    from_acc = params[:from_acc]
    to_acc = params[:to_acc]
    message = params[:message]
    
    @payment_merchant = build_resource
    @payment_merchant.admin_id = current_user.id

    if merchant.status_featured?
      order_merchant = Order.joins(:stakeholder_orders).where("stakeholder_orders.merchant_id =?", merchant.id).paid
      order = order_merchant.where("DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=?", payment_start, payment_end)
    else
      order = StakeholderOrder.eager_load(:order).where("stakeholder_orders.merchant_id =? AND DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=? AND orders.order_status_id =?", merchant_id, payment_start, payment_end, 3)
    end

    @payment_merchant.price = '%.4f' % order.sum(:basic_price)
    @payment_merchant.ongkir = '%.4f' % order.sum(:shipping_price)
    @payment_merchant.total = '%.4f' % order.sum(:total_basic_price)

    @payment_merchant.from_acc = from_acc
    @payment_merchant.to_acc = to_acc
    @payment_merchant.message = message
    @payment_merchant.status = status

    if @payment_merchant.save
      create_activity(@payment_merchant, current_user.id, "Pembayaran Merchant (#{resource.merchant.merchant_info.title})", "#{current_user.username} membuat pembyaran untuk priode #{helpers.get_date(resource.payment_start)} s.d. #{helpers.get_date(resource.payment_end)} sebesar #{helpers.get_currency(resource.total)} ")
      redirect_to backend_payment_merchants_path, notice: "Successfully created payment snapshots"
    else
      redirect_to backend_payment_merchants_path, alert: "Unsuccessfully created payment snapshots, #{@payment_merchant.errors.full_messages.join(',')}"
    end
  end

  def update
    resource
    merchant_id = params[:payment_merchant][:merchant_id]

    merchant = Merchant.find merchant_id
    payment_start = params[:payment_merchant][:payment_start]
    payment_end = params[:payment_merchant][:payment_end]
    status = params[:payment_merchant][:status]
    from_acc = params[:from_acc].present? ? params[:from_acc] : params[:payment_merchant][:from_acc]
    to_acc = params[:to_acc].present? ? params[:to_acc] : params[:payment_merchant][:to_acc]
    message = params[:message].present? ? params[:message] : params[:payment_merchant][:message]

    if Time.zone.now.to_date > payment_end.to_date
      if check_data_update(merchant_id, payment_start, payment_end).present?
        @payment_merchant.admin_id = current_user.id

        if merchant.status_featured?
          order_merchant = Order.joins(:stakeholder_orders).where("stakeholder_orders.merchant_id =?", merchant.id).paid.track_shipped_received 
          order = order_merchant.where("DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=?", payment_start, payment_end)
        else
          order = StakeholderOrder.eager_load(:order).where("stakeholder_orders.merchant_id =? AND DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=? AND orders.order_status_id =?", merchant_id, payment_start, payment_end, 3)
        end

        @payment_merchant.price = '%.4f' % order.sum(:basic_price)
        @payment_merchant.ongkir = '%.4f' % order.sum(:shipping_price)
        @payment_merchant.total = '%.4f' % order.sum(:total_basic_price)

        @payment_merchant.from_acc = from_acc
        @payment_merchant.to_acc = to_acc
        @payment_merchant.message = message
        @payment_merchant.status = status

        if @payment_merchant.save
          redirect_to backend_payment_merchants_path, notice: "Successfully created payment snapshots"
        else
          redirect_to backend_payment_merchants_path, alert: "Unsuccessfully created payment snapshots, #{@payment_merchant.errors.full_messages.join(',')}"
        end
        
      else
        redirect_back(fallback_location: backend_payment_merchants_path, alert: "Unsuccessfully updated #{@resource_name} because #{merchant.username} period #{payment_start} - #{payment_end} has never entered!")
      end
    else
      redirect_back(fallback_location: backend_payment_merchants_path, alert: "Unsuccessfully updated #{@resource_name} because payment_end selected is bigger than this day!")
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end

    def check_data_update(merchant_id, payment_start, payment_end)
      payment_merchant = PaymentMerchant.where("payment_merchants.merchant_id =? AND payment_merchants.payment_start =? AND payment_merchants.payment_end =?", merchant_id, payment_start, payment_end).where.not(status: PaymentMerchant::PAID)
      return payment_merchant
    end

end