class Backend::ConfirmationsController < Backend::ApplicationController
  defaults resource_class: Confirmation, collection_name: 'confirmations', instance_name: 'confirmation'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Confirmations", :collection_path

  include MultipleAction

  def index
    if params[:export] == 'true'
      @collection_all = collection.latest
    end
    
    @collection = collection.latest.page(page).per(per_page)

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    @order = Order.find_by(code: resource.no_invoice.downcase)
  end

  def create
    check_invoice(params[:confirmation][:no_invoice])

    if @invoice.present?
      @confirmation = Confirmation.new(permitted_params)
      @confirmation.member_id = @invoice.member_id
      @confirmation.account_manager_id = @invoice.account_manager_id
      @confirmation.order_id = @invoice.id
      if @confirmation.save
        UserMailer.send_confirmation(@confirmation).deliver
        redirect_back(fallback_location: backend_order_path(@invoice), notice: 'Anda telah berhasil mengirim Konfirmasi Pembayaran anda, silahkan tunggu kami akan melakukan proses pengecekan terlebih dahulu maximal menunggu 1x24 jam!')
      else
        redirect_back(fallback_location: backend_order_path(@invoice), alert: @confirmation.errors.full_messages)
      end
    else
      redirect_back(fallback_location: backend_order_path(@invoice), alert: 'Tidak ada nomor invoice yang ditemukan!')
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end

    def permitted_params
      params.require(:confirmation).permit(Permitable.controller(params[:controller]))
    end

    def check_invoice(no_invoice)
      @invoice = Order.find_by(code: no_invoice.downcase)
    end
end
