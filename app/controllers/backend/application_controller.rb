class Backend::ApplicationController < Backend::ResourcesController
  layout 'backend'

  before_action :authenticate_admin!, :prepare_count, :filter_for_access!
  protect_from_forgery with: :exception

  def authenticate_admin!
    unless current_user.present? && (current_user.admin?)
      redirect_to root_path, alert: "Can't Access this page"
    end
  end

  protected
    def prepare_count
      # @members_size = Member.all.size
      # @merchants_size = Merchant.all.size
      # @products_size = Product.all.size
      # @confirmations_size = Confirmation.all.size
      # @orders_size = Order.all.size
    end

    def per_page
      params[:per_page] ||= 20
    end

    def page
      params[:page] ||= 1
    end

    def filter_for_access!
      if user_signed_in? && current_user.checker_1?
        unless params[:controller] == 'backend/orders' || 
           params[:controller] == 'backend/order_distributors' || 
           params[:controller] == 'backend/order_items' ||
           params[:controller] == 'backend/confirmations' ||
           params[:controller] == 'backend/home'
          redirect_to backend_dashboard_path, alert: "Can not access this page!"
        end
      elsif user_signed_in? && current_user.checker_2?
        unless params[:controller] == 'backend/orders' || 
           params[:controller] == 'backend/order_distributors' || 
           params[:controller] == 'backend/order_items' ||
           params[:controller] == 'backend/home' ||
           params[:controller] == 'backend/admins' && (params[:action] == 'edit' || params[:action] == 'update') ||
           params[:controller] == 'backend/products'
          redirect_to backend_dashboard_path, alert: "Can not access this page!"
        end
      elsif user_signed_in? && current_user.checker_3?
        unless params[:controller] == 'backend/orders' ||
          params[:controller] == 'backend/home' ||
          params[:controller] == 'backend/admins' && (params[:action] == 'edit' || params[:action] == 'update')
          redirect_to backend_dashboard_path, alert: "Can not access this page!"
        end
      elsif user_signed_in? && current_user.shipment?
        unless params[:controller] == 'backend/orders' ||
          params[:controller] == 'backend/home' ||
          params[:controller] == 'backend/admins' && (params[:action] == 'edit' || params[:action] == 'update')
          redirect_to backend_dashboard_path, alert: "Can not access this page!"
        end
      elsif user_signed_in? && current_user.staff?
        unless params[:controller] == 'backend/invoices' ||
          params[:controller] == 'backend/home' ||
          params[:controller] == 'backend/admins' && (params[:action] == 'edit' || params[:action] == 'update')
          redirect_to backend_dashboard_path, alert: "Can not access this page!"
        end
      elsif user_signed_in? && current_user.support?
        unless params[:controller] == 'backend/domain_settings' ||
          params[:controller] == 'backend/home' ||
          params[:controller] == 'backend/products' ||
          params[:controller] == 'backend/guest_books' ||
          params[:controller] == 'backend/web_templates' ||
          params[:controller] == 'backend/landing_presses' ||
          params[:controller] == 'backend/request_landing_presses' ||
          params[:controller] == 'backend/blogs' ||
          params[:controller] == 'backend/admins' && (params[:action] == 'edit' || params[:action] == 'update')
          redirect_to backend_dashboard_path, alert: "Can not access this page!"
        end
      elsif user_signed_in? && current_user.finance?
        unless params[:controller] == 'backend/orders' || 
           params[:controller] == 'backend/spendings' ||
           params[:controller] == 'backend/order_distributors' || 
           params[:controller] == 'backend/products' || 
           params[:controller] == 'backend/order_items' ||
           params[:controller] == 'backend/confirmations' ||
           params[:controller] == 'backend/invoices' ||
           params[:controller] == 'backend/commissions' ||
           params[:controller] == 'backend/commission_referrals' ||
           params[:controller] == 'backend/payment_snapshots' ||
           params[:controller] == 'backend/payment_referrals' ||
           params[:controller] == 'backend/payment_merchants' ||
           params[:controller] == 'backend/recap_payments' ||
           params[:controller] == 'backend/home' ||
           params[:controller] == 'backend/report_refunds' || 
           params[:controller] == 'backend/members' || 
           params[:controller] == 'backend/account_managers' || 
           params[:controller] == 'backend/merchants' || 
           params[:controller] == 'backend/admins' && (params[:action] == 'edit' || params[:action] == 'update')

          redirect_to backend_dashboard_path, alert: "Can not access this page!"
        end
      end
    end
end
