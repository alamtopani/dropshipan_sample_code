class Backend::MerchantsController < Backend::ApplicationController
  defaults resource_class: Merchant, collection_name: 'merchants', instance_name: 'merchant'
  before_action :service_before_action
  
  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Merchants", :collection_path

  include MultipleAction
  include Ongkir

  def index
    @verifieds = collection.verified.size 
    @not_verifieds = collection.not_verified.size 
    @featureds = collection.featured.size 
    @collection = collection.latest.page(page).per(per_page)
    
    if params[:export] == 'true'
      @collection_all = collection
    end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def show
    @merchant = resource
    @activities = resource.activities.latest.search_by(params).page(params[:page_activities]).per(per_page)
    @stakeholder_orders = resource.stakeholder_orders.latest.search_by(params).page(params[:page_orders]).per(per_page)
    @payment_merchants = resource.payment_merchants.latest.search_by(params).page(params[:page_payment_snapshots]).per(per_page)
    
    @collection_all = collection
    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end

    def service_before_action
      get_region_ongkir_province
    end
end
