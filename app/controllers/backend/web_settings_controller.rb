class Backend::WebSettingsController < Backend::ApplicationController
  defaults resource_class: WebSetting, collection_name: 'web_settings', instance_name: 'web_setting'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Web Settings", :collection_path

  def index
    @collection = collection.oldest.page(page).per(per_page)
  end

  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to request.referer || root_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to request.referer || root_path}
      end
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
