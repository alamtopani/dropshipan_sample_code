class Backend::OrderDistributorsController < Backend::ApplicationController
  defaults resource_class: StakeholderOrder, collection_name: 'stakeholder_orders', instance_name: 'stakeholder_order'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Order Distributor", :collection_path

  include MultipleAction

  def index
    collection_default = collection
    if current_user.checker_2?
      collection_default = collection_default.paid.track_verification_packing
    end
    @order_verification = collection_default.verification
    @order_paid = collection_default.paid
    @order_cancelled = collection_default.cancelled
    @reports_collect = collection_default.latest
    
    @collection = collection_default.latest.page(page).per(per_page)
    
    if params[:export] == 'true'
      @collection_all = collection
    end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def show
    @order = resource.order
    @confirmation = Confirmation.where("LOWER(confirmations.no_invoice) LIKE LOWER('#{@order.code}')").first
  end


  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params).without_in_progress
    end
end
