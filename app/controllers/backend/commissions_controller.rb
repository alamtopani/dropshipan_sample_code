class Backend::CommissionsController < Backend::ApplicationController
  defaults resource_class: Commission, collection_name: 'commissions', instance_name: 'commission'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Commissions", :collection_path

  include MultipleAction

  def index
    commission_report_day(collection.all, 'with_search_by', :commission_company, 'order', params[:export])

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end
end
