class Backend::MembersController < Backend::ApplicationController
  defaults resource_class: Member, collection_name: 'members', instance_name: 'member'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Members", :collection_path

  include MultipleAction

  def index
    @verifieds = collection.verified.active.size 
    @featureds = collection.verified.active.featured.size
    @not_verifieds = collection.not_verified.size
    @not_actives = collection.not_active.size

    if params[:export] == 'true'
      @collection_all = collection
    end
    
    @collection = collection.latest.page(page).per(per_page)

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def show
    @domain_settings = resource.domain_settings.page(params[:page_domain_settings]).per(per_page)
    @catalogs = resource.catalogs.latest.search_by(params).page(params[:page_catalogs]).per(per_page)
    @orders = resource.orders.latest.search_by(params).page(params[:page_orders]).per(per_page)
    @confirmations = resource.confirmations.latest.search_by(params).page(params[:page_confirmations]).per(per_page)
    @commissions = resource.commissions.latest.search_by(params).page(params[:page_commissions]).per(per_page)
    @commission_referrals = resource.commission_referrals.latest.search_by(params).page(params[:page_commission_referrals]).per(per_page)
    @payment_snapshots = resource.payment_snapshots.latest.search_by(params).page(params[:page_payment_snapshots]).per(per_page)
    @payment_referrals = resource.payment_referrals.latest.search_by(params).page(params[:page_payment_referrals]).per(per_page)
    @report_refunds = resource.report_refunds.latest.search_by(params).page(params[:page_report_refunds]).per(per_page)
    @invoices = resource.invoices.latest.search_by(params).page(params[:page_invoices]).per(per_page)
    @referrals = resource.referrals.latest.search_by(params).page(params[:page_referrals]).per(per_page)
    @request_landing_presses = resource.request_landing_presses.latest.search_by(params).page(params[:page_request_landing_presses]).per(per_page)
    @activities = resource.activities.latest.search_by(params).page(params[:page_activities]).per(per_page)
    @referral_verified = @referrals.verified
    @referral_not_verified = @referrals.not_verified
    @referral_referral1 = @referrals.referral1(resource.id)
    @referral_referral2 = @referrals.referral2(resource.id)
    @referral_referral3 = @referrals.referral3(resource.id)

    respond_to do |format|
      format.html
      format.js
    end
  end

  def update
    super
    new_account_manager = params[:member][:account_manager_id].to_i
    if resource.catalogs.present?
      resource.catalogs.each do |c|
        if new_account_manager != c.account_manager_id
          c.account_manager_id = new_account_manager
          c.save
        end
      end
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by_member(params)
    end
end
