class Backend::DomainSettingsController < Backend::ApplicationController
  defaults resource_class: DomainSetting, collection_name: 'domain_settings', instance_name: 'domain_setting'

  add_breadcrumb "Dashboard", :backend_dashboard_path
  add_breadcrumb "Domain Settings", :collection_path

  include MultipleAction

  def index
    @collection_pending = collection.pending.count
    @collection_process = collection.process.count
    @collection_active = collection.active.count
    @collection_inactive = collection.inactive.count
    
    @collection = collection.latest.page(page).per(per_page)

    if params[:export] == 'true'
      @collection_all = collection
    end
    
    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end
