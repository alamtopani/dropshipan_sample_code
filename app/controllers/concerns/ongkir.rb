module Ongkir
  extend ActiveSupport::Concern

  included do
    def initialize_ongkir(type_url)
      require 'uri'
      require 'net/http'

      if type_url == 'province'
        url = URI("https://pro.rajaongkir.com/api/province")
      elsif type_url == 'city'
        url = URI("https://pro.rajaongkir.com/api/city?province=#{@province_id}")
      elsif type_url == 'district'
        url = URI("https://pro.rajaongkir.com/api/subdistrict?city=#{@city_id}")
      end
          
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE

      request = Net::HTTP::Get.new(url)
      request["key"] = Rails.application.secrets.api_key_raja_ongkir

      @response = http.request(request)
    end

    def get_region_ongkir_province
      initialize_ongkir('province')
      data = JSON.parse(@response.body)["rajaongkir"]["results"]
      @all_province = data.map{|d| [d["province"], d["province_id"]+':'+d["province"]]}
    end

    def get_region_ongkir_city(province_id)
      @province_id = province_id.split(':')[0]
      initialize_ongkir('city')
      data = JSON.parse(@response.body)["rajaongkir"]["results"]
      @all_city = data.map{|d| [d["type"]+' '+d["city_name"], d["city_id"]+':'+d["city_name"]] if d["province_id"] == "#{@province_id}"}.compact
    end

    def get_region_ongkir_district(city_id)      
      @city_id = city_id.split(':')[0]
      initialize_ongkir('district')
      data = JSON.parse(@response.body)["rajaongkir"]["results"]
      @all_district = data.map{|d| [d["subdistrict_name"], d["subdistrict_id"]+':'+d["subdistrict_name"]] if d["city_id"] == "#{@city_id}"}.compact
    end
  end
end