module MultipleAction
	extend ActiveSupport::Concern

	def do_multiple_act
		if params[:ids].present?
			obj_model = collection.name.singularize.classify.constantize
			collect = obj_model.where("id IN (?)", params[:ids])

			if params[:commit] == 'destroy_all'
				do_destroy_all(collect)
			elsif params[:commit] == 'verified'
				verified_multiple(collect)
			elsif params[:commit] == 'non_verified'
				non_verified_multiple(collect)
			elsif params[:commit] == 'activated'
				activated_multiple(collect)
			elsif params[:commit] == 'non_activated'
				non_activated_multiple(collect)
			elsif params[:commit] == 'featured'
				change_featured_status(collect)
			elsif params[:commit] == 'pending'
				change_pending_status(collect)
			elsif params[:commit] == 'rejected'
				change_rejected_status(collect)
			elsif params[:commit] == 'approved'
				change_approved_status(collect)
			elsif params[:commit] == 'active'
				change_active_status(collect)
			elsif params[:commit] == 'inactive'
				change_inactive_status(collect)
			elsif params[:commit] == 'paid'
				change_paid_status(collect)
			elsif params[:commit] == 'make_free'
				change_make_free_status(collect)
			elsif params[:commit] == 'process'
				change_process_status(collect)
			elsif params[:commit] == 'reject'
				change_reject_status(collect)
			elsif params[:commit] == 'unpaid'
				change_unpaid_status(collect)
			elsif params[:commit] == 'cancelled'
				change_cancelled_status(collect)
			elsif params[:commit] == 'membership_active'
				change_membership_active_status(collect)
			elsif params[:commit] == 'membership_inactive'
				change_membership_inactive_status(collect)
			elsif params[:commit] == 'promote_catalog'
				change_promote_catalog(collect)
			end
				
		else
			redirect_to request.referer || root_path, alert: 'Please Choice Selected!'
		end
	end

	def change_promote_catalog(collect)
		collect.each do |p|
			if p.status == true
				Member.verified.each do |user|
					resource = Catalog.find_by(member_id: user.id, product_id: p.id)
					if resource.blank?
						resource = Catalog.new
						resource.product_id = p.id
						resource.member_id = user.id
						resource.account_manager_id = user.account_manager_id
						resource.price = p.recommended_price
						resource.status = "approved"
						resource.save
					end
				end
			end
		end
		redirect_to request.referer || root_path, notice: "Promote create catalog multiple successfully updated!"
	end

	def do_destroy_all(collect)
		if collect.destroy_all
			redirect_to request.referer || root_path, notice: 'Successfully Destroyed!'
		else
			redirect_to request.referer || root_path, alert: 'Not Successfully Destroyed!'
		end
	end

	def verified_multiple(collect)
		collect.each do |user|
			if user.class.name == "Member"
				user.account_manager_id = AccountManager.where.not(username: 'alamtopani').pluck(:id).sample if user.account_manager.blank?
			end

			status_old = user.verified
			user.verified = true
			if user.save
				if user.class.name == "Member"
					if status_old == false && user.catalogs.blank?
						UserMailer.send_user_approve_to_verified(user).deliver
						Product.verify.each do |product|
							resource = Catalog.new
							resource.product_id = product.id
							resource.member_id = user.id
							resource.account_manager_id = user.account_manager_id
							resource.price = product.recommended_price
							resource.status = "approved"
							resource.save
						end
					end
				end
			end
		end
		redirect_to request.referer || root_path, notice: "Verified multiple users successfully change!"
	end

	def non_verified_multiple(collect)
		collect.each do |catalog|
			catalog.verified = false
			catalog.save
		end
		redirect_to request.referer || root_path, notice: "Unverified multiple items successfully change!"
	end

	def activated_multiple(collect)
		collect.each do |catalog|
			catalog.status = true
			catalog.save
		end
		redirect_to request.referer || root_path, notice: "Activated multiple items successfully change!"
	end

	def non_activated_multiple(collect)
		collect.each do |catalog|
			catalog.status = false
			catalog.save
		end
		redirect_to request.referer || root_path, notice: "Activated multiple items successfully change!"
	end

	def change_featured_status(collect)
		collect.each do |catalog|
			if catalog.featured == false
				catalog.featured = true
				catalog.save
			else
				catalog.featured = false
				catalog.save
			end
		end

		redirect_to request.referer || root_path, notice: "You featured multiple items successfully change!"
	end

	def change_pending_status(collect)
		collect.each do |catalog|
			catalog.status = "pending"
			catalog.save
		end
		redirect_to request.referer || root_path, notice: "Activated multiple items successfully change!"
	end

	def change_rejected_status(collect)
		collect.each do |catalog|
			catalog.status = "rejected"
			catalog.save
		end
		redirect_to request.referer || root_path, notice: "Activated multiple items successfully change!"
	end

	def change_approved_status(collect)
		collect.each do |catalog|
			catalog.status = "approved"
			catalog.save
		end
		redirect_to request.referer || root_path, notice: "Activated multiple items successfully change!"
	end

	def change_active_status(collect)
		collect.each do |resource|
			resource.status = "active"
			resource.save
		end
		redirect_to request.referer || root_path, notice: "Activated multiple items successfully change!"
	end

	def change_inactive_status(collect)
		collect.each do |resource|
			resource.status = "inactive"
			resource.save
		end
		redirect_to request.referer || root_path, notice: "Inactivated multiple items successfully change!"
	end

	def change_paid_status(collect)
		collect.each do |resource|
			date_today = Time.zone.now.to_date
			status_old = resource.status
			if resource.class.name == "Invoice"
				if resource.invoiceable_type == "Package"
					if status_old != "paid"
						last_invoice = resource.member.invoices.where.not("invoices.id =?", resource.id).latest.first
						# invoices_paid = resource.member.invoices_paid

						if resource.package.id != resource.member.package_id
							member = resource.member
							member.package_id = resource.package.id
							member.save
						end

						period = resource.package.period.to_i
						if last_invoice.present?
							if last_invoice.period_end < date_today
								resource.period_start = date_today
								resource.period_end = resource.period_start + period.months
							else
								last_period_end = last_invoice.period_end
								resource.period_start = last_period_end + 1.days
								resource.period_end = resource.period_start + period.months
							end
						else
							resource.period_start = date_today
							resource.period_end = resource.period_start + period.months
						end
						member = resource.member
						member.membership = true
						member.save

						affiliate = Affiliate.find_by(invoice_id: resource.id)
						if affiliate.present?
							affiliate.status = "process"
							affiliate.save
						end

						UserMailer.send_invoice_package_paid(resource).deliver
						create_activity(resource, current_user.id, "Pembayaran Paket", "#{current_user.username} mengubah status pembayaran paket dari 'pending' menjadi 'paid'.")
					end
				end
			elsif resource.class.name == "ReportRefund"
				UserMailer.send_report_refund_paid(resource).deliver
			elsif resource.class.name == "PaymentSnapshot"
				create_activity(resource, current_user.id, "Pembayaran #{resource.user.type} (#{resource.user.username})", "#{current_user.username} mengubah status pembayaran periode #{helpers.get_date(resource.payment_start)} s.d. #{helpers.get_date(resource.payment_end)} sebesar #{helpers.get_currency(resource.price)} dari '#{status_old}' menjadi 'paid'.")
				UserMailer.send_payment_snapshot_paid(resource).deliver
			elsif resource.class.name == "PaymentMerchant"
				create_activity(resource, current_user.id, "Pembayaran Merchant (#{resource.merchant.merchant_info.title})", "#{current_user.username} mengubah status pembayaran periode #{helpers.get_date(resource.payment_start)} s.d. #{helpers.get_date(resource.payment_end)} sebesar #{helpers.get_currency(resource.total)} dari '#{status_old}' menjadi 'paid'.")
				UserMailer.send_payment_merchant_paid(resource).deliver
			elsif resource.class.name == "PaymentReferral"
				create_activity(resource, current_user.id, "Pembayaran Referral (#{resource.member.username})", "#{current_user.username} mengubah status pembayaran referral periode #{helpers.get_date(resource.payment_start)} s.d. #{helpers.get_date(resource.payment_end)} sebesar #{helpers.get_currency(resource.commission_referral)} dari '#{status_old}' menjadi 'paid'.")
				UserMailer.send_payment_referral_paid(resource).deliver
			elsif resource.class.name == "Affiliate"
				UserMailer.send_payment_affiliate_paid(resource).deliver unless resource.fee_member == 0
			end
			resource.status = "paid"
			resource.save
		end
		redirect_to request.referer || root_path, notice: "Paid multiple items successfully change!"
	end

	def change_make_free_status(collect)
		collect.each do |resource|
			resource.promo = true
			resource.status = "paid"
			resource.save
			create_activity(resource, current_user.id, "Status Promo 'FREE'", "#{current_user.username} mengubah status promo paket dari 'Berbayar' menjadi 'FREE'.")
		end
		redirect_to request.referer || root_path, notice: "Make FREE Invoice multiple items successfully change!"
	end

	def change_process_status(collect)
		collect.each do |resource|
			status_old = resource.status  
			resource.status = "process"
			if resource.class.name == "DomainSetting"
				resource.ns1 = helpers.name_server1
				resource.ns2 = helpers.name_server2
			elsif resource.class.name == "PaymentSnapshot"
				create_activity(resource, current_user.id, "Pembayaran #{resource.user.type} (#{resource.user.username})", "#{current_user.username} mengubah status pembayaran dari periode #{helpers.get_date(resource.payment_start)} s.d. #{helpers.get_date(resource.payment_end)} sebesar #{helpers.get_currency(resource.price)} '#{status_old}' menjadi 'process'.")
			elsif resource.class.name == "PaymentMerchant"
				create_activity(resource, current_user.id, "Pembayaran (#{resource.merchant.merchant_info.title})", "#{current_user.username} mengubah status pembayaran dari periode #{helpers.get_date(resource.payment_start)} s.d. #{helpers.get_date(resource.payment_end)} sebesar #{helpers.get_currency(resource.total)} '#{status_old}' menjadi 'process'.")
			elsif resource.class.name == "PaymentReferral"
				resource.payment_pending = false
				resource.quote = nil
				create_activity(resource, current_user.id, "Pembayaran Referral (#{resource.member.username})", "#{current_user.username} mengubah status pembayaran referral dari periode #{helpers.get_date(resource.payment_start)} s.d. #{helpers.get_date(resource.payment_end)} sebesar #{helpers.get_currency(resource.commission_referral)} '#{status_old}' menjadi 'process'.")
			end
			resource.save
		end
		redirect_to request.referer || root_path, notice: "Process multiple items successfully change!"
	end

	def change_reject_status(collect)
		collect.each do |resource|  
			status_old = resource.status
			resource.status = "reject"
			if resource.class.name == "PaymentSnapshot"
				create_activity(resource, current_user.id, "Pembayaran #{resource.user.type} (#{resource.user.username})", "#{current_user.username} mengubah status pembayaran dari periode #{helpers.get_date(resource.payment_start)} s.d. #{helpers.get_date(resource.payment_end)} sebesar #{helpers.get_currency(resource.price)} '#{status_old}' menjadi 'reject'.")
			elsif resource.class.name == "PaymentMerchant"
				create_activity(resource, current_user.id, "Pembayaran (#{resource.merchant.merchant_info.title})", "#{current_user.username} mengubah status pembayaran dari periode #{helpers.get_date(resource.payment_start)} s.d. #{helpers.get_date(resource.payment_end)} sebesar #{helpers.get_currency(resource.total)} '#{status_old}' menjadi 'reject'.")
			end
			resource.save
		end
		redirect_to request.referer || root_path, notice: "Reject multiple items successfully change!"
	end

	def change_unpaid_status(collect)
		collect.each do |resource|  
			resource.status = "unpaid"
			resource.save
		end
		redirect_to request.referer || root_path, notice: "Unpaid multiple items successfully change!"
	end

	def change_cancelled_status(collect)
		collect.each do |resource|  
			resource.status = "cancelled"
			resource.save
		end
		redirect_to request.referer || root_path, notice: "Cancelled multiple items successfully change!"
	end

	def change_membership_active_status(collect)
		collect.each do |resource|
			resource.membership = true
			resource.save
		end
		redirect_to request.referer || root_path, notice: "Activated multiple items successfully change!"
	end

	def change_membership_inactive_status(collect)
		collect.each do |resource|
			resource.membership = false
			resource.save
		end
		redirect_to request.referer || root_path, notice: "Inactivated multiple items successfully change!"
	end

end
