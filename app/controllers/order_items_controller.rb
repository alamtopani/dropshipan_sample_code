class OrderItemsController < FrontendController
  before_action :prepare_order

  def create
    stock_available = check_stock_product(params)
    product = Product.find_by(id: params[:order_item][:product_id].to_i)
    
    if current_order.order_items.present? && product.merchant.featured == false && current_order.order_items.eager_load(:merchant).where('users.featured =?', true).present?
      @detect_pre_order_1 = true
      return false
    elsif current_order.order_items.present? && product.merchant.featured == true && current_order.order_items.where.not("order_items.merchant_id =?", product.merchant.id).present?
      @detect_pre_order_2 = true
      return false
    elsif current_order.order_items.map{|o| o.product.status_stock }.include?('pre_order')
      @detect_pre_order_3 = true
      return false
    elsif product.pre_order? && (current_order.order_items.map{|o| o.product.status_stock }.include?('always_ready') || current_order.order_items.map{|o| o.product.status_stock }.include?('ready_stock'))
      @detect_pre_order_4 = true
      return false
    else
      if stock_available == true
        prepare_stakeholder(params[:stakeholder])
        if current_user.present? && current_user.member?
          @subdomain = current_user
        else
          @subdomain = @subdomain
        end
        @order.member_id = @subdomain.id
        @order.save
        session[:order_id] = @order.id
      else
        return false
      end
    end
  end

  def update
    @order_item = @order.order_items.find(params[:id])
    params[:order_item][:variant] = @order_item.variant

    stock_available = check_stock_product(params)
    if stock_available == true
      @order_item.weight = @order_item.product.weight * params[:order_item][:quantity].to_f
      @order_item.update_attributes(permitted_params)
    else
      return false
    end

    @order_items = @order.order_items
    @stakeholders = @order.stakeholder_orders.latest
  end

  def destroy
    @order_item = @order.order_items.find(params[:id])
    if @order_item.destroy
      check_stakeholder_order_item(@order_item.stakeholder_order)
    end

    @order_items = @order.order_items
    @stakeholders = @order.stakeholder_orders.latest
  end
  
  private
    def prepare_order
      @order = current_order
    end

    def check_stock_product(params)
      product = Product.find_by(id: params[:order_item][:product_id].to_i)
      stock = product.product_stock(params[:order_item][:variant])
      if product.ready_stock? && stock > 0  
        return true
      elsif product.pre_order? || product.always_ready?
        return true
      else
        return false
      end
    end

    def prepare_stakeholder(stakeholder)
      if current_user.present? && current_user.member?
        @subdomain = current_user
      else
        @subdomain = @subdomain
      end

      @stakeholder = @order.stakeholder_orders.find_by(merchant_id: stakeholder)

      if @stakeholder.present?
        @order_item = @order.order_items.new(permitted_params.merge(quantity: 1, stakeholder_order_id: @stakeholder.id, member_id: @subdomain.id))
      else
        @stakeholder_order = @order.stakeholder_orders.new
        @stakeholder_order.member_id = @subdomain.id
        @stakeholder_order.merchant_id = stakeholder
        if @stakeholder_order.save
          @order_item = @order.order_items.new(permitted_params.merge(quantity: 1, stakeholder_order_id: @stakeholder_order.id, member_id: @subdomain.id))
        end
      end
    end

    def check_stakeholder_order_item(holder)
      stakeholder = StakeholderOrder.find_by(id: holder)
      if stakeholder.order_items.size < 1
        stakeholder.destroy
      end
    end

    def permitted_params
      params.require(:order_item).permit(Permitable.controller(params[:controller]))
    end

end