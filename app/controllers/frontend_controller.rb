class FrontendController < ApplicationController
  before_action :prepare_subdomain

  protected
    def prepare_subdomain
      @categories = Category.roots.latest.active
      
      if current_user.present? && current_user.admin?
        redirect_to backend_dashboard_path unless params[:controller] == 'xhrs'
      else
        # ------ FOR STAGING
        # domain = SiteInfo.find_by(domain_name: request.subdomain) || DomainSetting.active.find_by(name: request.domain)
        # if domain.present?
        #   @subdomain = Member.find_by(id: domain.member_id) rescue nil
        #   if @subdomain.present?
        #     if @subdomain.verified == true && @subdomain.membership == true && @subdomain.invoices.current_packages(@subdomain.package_id).paid.last.present?
        #       @setting = @subdomain.web_setting
        #     else
        #       redirect_to not_found_page_path unless params[:action] == 'not_found_page'
        #     end
        #   else
        #     redirect_to not_found_page_path unless params[:action] == 'not_found_page'
        #   end
        # end

        # ------ FOR DEVELOPMENT
        @subdomain = Member.find_by(id: Member.latest.first) rescue nil
        if @subdomain.present?
          if @subdomain.verified == true && @subdomain.membership == true && @subdomain.invoices.current_packages(@subdomain.package_id).paid.last.present?
            @setting = @subdomain.web_setting
          else
            redirect_to not_found_page_path unless params[:action] == 'not_found_page'
          end
        else
          redirect_to not_found_page_path unless params[:action] == 'not_found_page'
        end
      end
    end
end