class LandingPagesController < FrontendController
  add_breadcrumb "Home", :root_path

  def show
    resource
  end

  private
    def resource
      @landing_page = LandingPage.find params[:id]
    end
end