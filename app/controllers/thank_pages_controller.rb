class ThankPagesController < ApplicationController
	layout 'thank_page'

  def show
  	@member = Member.find(params[:id])
  	@payment = WebSetting.first.payments.first
  end
end
