class PublicsController < FrontendController
	add_breadcrumb "Home", :root_path
  layout 'blank', only: [:under_maintenance, :not_found_page]
  layout 'landing_press', only: [:landing_press]

	def home
    if @subdomain.present?
      @latest_catalogs = @subdomain.catalogs.bonds.approved.verify.latest.limit(10)
      @featured_catalogs = @subdomain.catalogs.bonds.approved.featured.verify.latest.limit(10)
      @top_sales_catalogs = @subdomain.catalogs.joins(:product, :order_items).approved.verify.select("catalogs.id, count(order_items.id) as order_item_count").order("order_item_count DESC").group("catalogs.id").limit(10)
    else
      if check_domain_valid_dropshipan? == true
        redirect_to home_path
      else
        redirect_to not_found_page_path
      end
    end
	end

  def under_maintenance
  end

  def not_found_page
  end

end
