class PagesController < FrontendController
	layout 'landing_press'
	
	def show
		@request_landing_press = RequestLandingPress.bonds.where('landing_presses.status =?', true).find_by(code: params[:id])
		if @request_landing_press.present?
      @landing_press = @request_landing_press.landing_press
  		@image_products = @request_landing_press.landing_press.landing_press_products
  		@image_testimonials = @request_landing_press.landing_press.landing_press_testimonials
  		@image_galleries = @request_landing_press.landing_press.landing_press_galleries
    else
      redirect_to not_found_page_path
    end
	end
end