class GuestBooksController < FrontendController

  def create
     @guest_book = GuestBook.new(user_id: params[:guest_book][:user_id], name: params[:guest_book][:name], phone: params[:guest_book][:phone], email: params[:guest_book][:email], description: params[:guest_book][:description] )

    respond_to do |format|
      if @guest_book.save
        UserMailer.send_guest_book(@guest_book).deliver
        format.html {redirect_to request.referer || root_path, notice: 'Pesan anda berhasil dikirim, kami akan membalas pesan anda sesegera mungkin ke Email ataupun Kontak yang sudah dikirim!'}
      else
        format.html {redirect_to request.referer || root_path, errors: @guest_book.errors.full_messages}
      end
    end
  end

end
