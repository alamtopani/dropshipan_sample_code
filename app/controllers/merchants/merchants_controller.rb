class Merchants::MerchantsController < Merchants::ApplicationController
	defaults resource_class: Merchant, collection_name: 'merchants', instance_name: 'merchant'
  before_action :service_before_action

  include MultipleAction
  include Ongkir

  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to request.referer || root_path, notice: "Akun anda telah berhasil dirubah"}
      else
        format.html {redirect_to request.referer || root_path, alert: resource.errors.full_messages}
      end
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain
    end

    def service_before_action
      get_region_ongkir_province
    end
end
