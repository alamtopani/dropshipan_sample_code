class Merchants::ApplicationController < Backend::ResourcesController
  layout 'application_merchants'

  before_action :authenticate_merchants!
  protect_from_forgery with: :exception

  def authenticate_merchants!
    unless current_user.present? && (current_user.merchant?)
      redirect_to root_path, alert: "Can't Access this page"
    else
      @merchant = Merchant.find_by(id: current_user.id)
      if @merchant.present?

      end
    end
  end

  protected
    def per_page
      params[:per_page] ||= 20
    end

    def page
      params[:page] ||= 1
    end
end
