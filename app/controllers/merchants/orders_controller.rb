class Merchants::OrdersController < Merchants::ApplicationController
  defaults resource_class: Order, collection_name: 'orders', instance_name: 'order'

  def index
    order_default = Order.joins(:stakeholder_orders).where("stakeholder_orders.merchant_id =?", @merchant.id).paid_cancel
    order_report_day(order_default, 'with_search_by', :basic_price, params[:export])

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def show
    add_breadcrumb "#{params[:id]}"
    @confirmation = Confirmation.where("LOWER(confirmations.no_invoice) LIKE LOWER('#{params[:id]}')").first
    @activities = resource.activities.latest.search_by(params).page(params[:page_activities]).per(per_page)
    
    respond_to do |format|
      format.html
      format.js
      format.pdf do
        render pdf: "Invoice Order - #{resource.code}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  def update
    @old_status = resource.order_status_id
    @old_status_shipping = resource.track_order
    update! do |format|
      if resource.errors.empty?
        resource.prepare_status_payment
        update_order(resource)
        format.html {redirect_to request.referer || root_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to request.referer || root_path}
      end
    end
  end

  def change_number_shipping
    order = Order.find(params[:id])
    number_shipping_old = order.number_shipping
    number_shipping_new = params[:order][:number_shipping]
    order.number_shipping = number_shipping_new
    if order.save
      UserMailer.order_shipped(order).deliver if number_shipping_old.blank? && number_shipping_new.present?
      create_activity(order, current_user.id, "Resi Pesanan", "#{current_user.username} memasukkan resi pesanan '#{order.number_shipping}'.")
      redirect_back(fallback_location: root_path, notice: "Nomor resi pengiriman berhasil disimpan!")
    else
      redirect_back(fallback_location: root_path, notice: "Nomor resi pengiriman gagal disimpan!")
    end
  end

  protected
     def update_order(order)
      if order.save
        Order.update_order(order, @old_status, @old_status_shipping)
        create_activity(order, current_user.id, "Status Pesanan (#{order.status_text?})", "#{current_user.username} mengubah status pesanan dari '#{Order.check_status_text(@old_status)}' menjadi '#{order.status_text?}'.") unless @old_status == order.order_status_id
        if @old_status_shipping == Order::VERIFICATION
          old_status_shipping = Order.check_status_text(@old_status)
        else
          old_status_shipping = Order.check_status_order_text(@old_status_shipping)
        end
        create_activity(order, current_user.id, "Status Pesanan (#{order.status_order_text?})", "#{current_user.username} mengubah status pesanan dari '#{old_status_shipping}' menjadi '#{order.status_order_text?}'.") unless @old_status_shipping == order.track_order
      end
      return order
    end
end