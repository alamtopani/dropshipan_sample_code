class Merchants::PaymentsController < Merchants::ApplicationController
	defaults resource_class: PaymentMerchant, collection_name: 'payment_merchants', instance_name: 'payment_merchant'

  def index
    @collection = collection.latest.page(page).per(per_page)

    if params[:export] == 'true'
      @collection_all = collection
    end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def show
    @resource = PaymentMerchant.find_by(code: params[:id])

    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Pembayaran - #{helpers.get_date(@resource.payment_start)} s.d. #{helpers.get_date(@resource.payment_end)}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  protected
    def collection
      @collection = @merchant.payment_merchants.search_by(params)
    end
end
