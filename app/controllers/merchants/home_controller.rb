class Merchants::HomeController < Merchants::ApplicationController
  add_breadcrumb "Dashboard", :merchants_dashboard_path

  def dashboard
    order_default = Order.joins(:stakeholder_orders).where("stakeholder_orders.merchant_id =?", @merchant.id).paid_cancel
    @products = @merchant.products
    @orders = order_default
    order_report_day(order_default, 'with_search_by', :basic_price, params[:export])
  end
end