class Merchants::OrderItemsController < Merchants::ApplicationController
  defaults resource_class: OrderItem, collection_name: 'order_items', instance_name: 'order_item'

  def update
    @old_status = resource.status
    update! do |format|
      if resource.errors.empty?
        update_order(resource)
        format.html {redirect_to request.referer || root_path}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to request.referer || root_path}
      end
    end
  end

  def update_order(order_item)
    order = order_item.order

    if order_item.save
      if (@old_status != OrderItem::REFUND && order_item.status == OrderItem::REFUND) || (@old_status != OrderItem::CANCELLED && order_item.status == OrderItem::CANCELLED)
        if order_item.product.ready_stock?
          order_item.quantity_cancellation 
          order_item.cancelled_reason = "Stok produk habis"
          order_item.save
        end
      end

      order.stakeholder_orders.each_with_index do |holder, index|
        holder.order_items.map.each do |item|
          item.weight = item.product.weight * item.quantity
          item.total_price = item.total_price?
          item.total_basic_price = item.total_basic_price?
          item.total_company_price = item.total_company_price?
          item.save
        end

        holder.weight = holder.total_weight?
        holder.price = holder.total?
        holder.basic_price = holder.basic_price?
        holder.company_price = holder.company_price?
        holder.total_price = holder.shipping_price + holder.price
        holder.total_basic_price = holder.shipping_price + holder.basic_price
        holder.total_company_price = holder.shipping_price + holder.company_price
        holder.payment_status = order.paid? ? true : false
        holder.track_order = order.track_order
        holder.save
      end

      if order.order_items.not_refund_and_cancelled.blank?
        create_report_refund(order_item, order_item.total_price, order.shipping_price)
        order.order_status_id = 4
        order.track_order = OrderItem::CANCELLED
        order.shipping_price = 0
      else
        shipping_price = prepare_shipping_price(order.shipping_method, order.address.district_full, order.total_weight?, order.origin_code)
        create_report_refund(order_item, order_item.total_price, order.shipping_price-shipping_price)
        order.shipping_price = shipping_price
      end
      
      order.price = order.total?
      order.basic_price = order.total_basic_price?
      order.company_price = order.total_company_price?
      order.total_price = order.total_payment? + order.shipping_price
      order.total_basic_price = order.total_payment_basic_price? + order.shipping_price
      order.total_company_price = order.total_payment_company_price? + order.shipping_price
      order.save
    end
    create_activity(order, current_user.id, "Status Salah Satu Barang Pesanan (#{order_item.status_text?})", "#{current_user.username} mengubah status salah satu barang pesanan dari '#{OrderItem.check_status_text(@old_status)}' menjadi '#{order_item.status_text?}'.") unless @old_status == order_item
    return order
  end

  def create_report_refund(order_item, refund_value, ongkir)
    order = order_item.order
    report_refund = ReportRefund.find_or_initialize_by(order_id: order_item.order_id)
    if report_refund.new_record?
      refund_value = refund_value
      quantity = order_item.quantity
      ongkir = ongkir
    else
      refund_value = report_refund.refund_value+refund_value
      quantity = report_refund.quantity+order_item.quantity
      ongkir = report_refund.ongkir+ongkir
    end
    report_refund.order_id = order_item.order_id
    report_refund.quantity = quantity
    report_refund.refund_value = refund_value
    report_refund.ongkir = ongkir
    report_refund.total = refund_value+ongkir
    report_refund.status = ReportRefund::PENDING
    report_refund.from_acc = order.confirmation.bank_account if order.confirmation.present?
    report_refund.to_acc = order.confirmation.payment_to if order.confirmation.present?
    report_refund.save
  end

end
