class SitemapsController < FrontendController
  layout nil
  def index
    headers['Content-Type'] = 'application/xml'
    respond_to do |format|
      format.xml {
        @catalogs = @subdomain.catalogs.bonds.approved.verify.latest
      }
    end
  end
end