class XhrsController < FrontendController
  include Ongkir

  def cities
    province = Region.find_by(name: params[:id])
    @cities = province ? province.children : Region.none
    render layout: false
  end

  def shipment_provinces
    get_region_ongkir_province
    @provinces = @all_province
    render layout: false
  end

  def shipment_cities
    get_region_ongkir_city(params[:id])
    @cities = @all_city
    render layout: false
  end

  def shipment_district
    get_region_ongkir_district(params[:id])
    @districts = @all_district
    render layout: false
  end

  def shipping_price
    prepare_shipping_price(params[:shipping_method], params[:district], current_order.total_weight?, params[:origin])
    render layout: false
  end

  def sub_categories
    @sub_categories = Category.find(params[:id]).children
    render layout: false
  end

  def sub_categories_catalog
    @sub_categories = Category.find(params[:id]).children
    render layout: false
  end

  def get_invoice_member
    member = Member.find(params[:id])
    package = member.package
    period = package.period.to_i
    invoices_paid = member.invoices_paid
    last_period_end = invoices_paid.last.period_end
    payment = WebSetting.first.payments.first

    @subject = "Tagihan Pembayaran Paket #{package.title}."
    @price = package.price
    @period_start = last_period_end + 1.days
    @period_end = @period_start + period.months
    @description = "Tagihan Pembayaran Paket #{package.title}."
    @bank_name = payment.bank_name
    @bank_branch = "Sukabumi"
    @account_name = payment.name
    @account_number = payment.account_number
    @invoiceable_type = package.class.name
    @invoiceable_id = package.id

    render layout: false
  end

  def get_payment
    node = params[:node].split(" ")
    user_id = node[0].to_i
    payment_start = node[1]
    payment_end = node[2]
    user = User.find user_id
    if user.member?
      commissions = Commission.where("commissions.member_id =? AND DATE(commissions.commission_date) >=? AND DATE(commissions.commission_date) <=? AND commissions.status =?", user_id, payment_start, payment_end, Commission::APPROVED)
      @to_acc = Member.find(user_id).profile.payment_to
      @user_type = "Member"
    elsif user.account_manager?
      commissions = Commission.where("commissions.account_manager_id =? AND DATE(commissions.commission_date) >=? AND DATE(commissions.commission_date) <=? AND commissions.status =?", user_id, payment_start, payment_end, Commission::APPROVED)
      @to_acc = AccountManager.find(user_id).profile.payment_to
      @user_type = "AccountManager"
    end
    @commission_member = '%.4f' % commissions.sum(:commission_member)
    @commission_am = '%.4f' % commissions.sum(:commission_am)
    @commission_company = '%.4f' % commissions.sum(:commission_company)
    
    if @user_type == "Member"
      @price = @commission_member.to_f
    else
      @price = @commission_am.to_f
    end
    @total_commission = @commission_member.to_f+@commission_am.to_f+@commission_company.to_f

    render layout: false
  end

  def get_payment_merchant
    node = params[:node].split(" ")
    merchant_id = node[0].to_i
    payment_start = node[1]
    payment_end = node[2]
    merchant = Merchant.find merchant_id

    if merchant.status_featured?
      order_merchant = Order.joins(:stakeholder_orders).where("stakeholder_orders.merchant_id =?", merchant.id).paid
      order = order_merchant.where("DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=?", payment_start, payment_end)
    else
      order = StakeholderOrder.eager_load(:order).where("stakeholder_orders.merchant_id =? AND DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=? AND orders.order_status_id =?", merchant_id, payment_start, payment_end, 3)
    end

    @to_acc = merchant.profile.payment_to

    @price = '%.4f' % order.sum(:basic_price)
    @ongkir = '%.4f' % order.sum(:shipping_price)
    @total = '%.4f' % order.sum(:total_basic_price)
    
    render layout: false
  end

  def get_payment_referral
    node = params[:node].split(" ")
    member_id = node[0].to_i
    payment_start = node[1]
    payment_end = node[2]
    member = Member.find member_id

    commission_referrals = CommissionReferral.where("commission_referrals.member_id =? AND DATE(commission_referrals.commission_date) >=? AND DATE(commission_referrals.commission_date) <=? AND commission_referrals.status =?", member_id, payment_start, payment_end, CommissionReferral::APPROVED)
    @to_acc = member.profile.payment_to

    @commission_referral = '%.4f' % commission_referrals.sum(:commission_value)
    
    orders = member.orders.where("DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=? AND orders.order_status_id =?", payment_start, payment_end, 3)
    total_order = '%.4f' % orders.sum(:total_price)
    if total_order.to_f >= PaymentReferral::ORDER_TARGET
      @quote = ""
    else
      @quote = PaymentReferral::QUOTE
    end

    render layout: false
  end

end
