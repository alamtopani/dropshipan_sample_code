class OfficialController < ApplicationController

  layout 'official'
  before_action :check_valid_url
  
  def home
    @slider_home = ['official/slider.jpg', 'official/slider2.jpg', 'official/slider3.jpg'].shuffle.first
    @packages = Package.active.oldest
    @blogs = Blog.active.latest

    client = Instagram.client(access_token: Rails.application.secrets.instagram_access_token)
    @instagram_images = client.user_recent_media
  end

  def faq
  end

  def faq2
  end

  def catalog   
  end

  def contact
  end

  def create_contact
    @guest_book = GuestBook.new(user_id: params[:guest_book][:user_id], name: params[:guest_book][:name], phone: params[:guest_book][:phone], email: params[:guest_book][:email], description: params[:guest_book][:description] )
    respond_to do |format|
      if verify_recaptcha(model: @guest_book) && @guest_book.save
        UserMailer.send_guest_book_official(@guest_book).deliver
        format.html {redirect_to request.referer || root_path, notice: 'Pesan anda berhasil dikirim, kami akan membalas pesan anda sesegera mungkin ke Email ataupun Kontak yang sudah dikirim!'}
      else
        format.html {redirect_to request.referer || root_path, alert: @guest_book.errors.full_messages}
      end
    end
  end

  private
    def check_valid_url
      # ------ FOR STAGING
      # if check_domain_valid_dropshipan? == false
      #   redirect_to not_found_page_path
      # end
    end
end