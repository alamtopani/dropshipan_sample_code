class ProductsController < FrontendController
  add_breadcrumb "Home", :root_path
  add_breadcrumb "Produk", :products_path
  
  before_action :prepare_order, except: [:index]
  before_action :prepare_simple_search, only: [:index]

  def index
    categories

    if params[:category].present?
      @category = Category.find_by(name: params[:category])
    end

    if @subdomain.present? && @subdomain.catalogs.present?
      @catalogs_all = @subdomain.catalogs.approved.verify.search_by(params)
      @catalogs = @catalogs_all.page(page).per(per_page)
    end
  end

  def show
    add_breadcrumb "#{params[:id]}"

    resource
    @merchant = resource.merchant
    @user_products = @merchant.products.verify.latest.limit(5)
    @profile = @merchant.profile
    @address = @merchant.address
    @contact = @merchant.contact
    impressionist(@product)

    @comments_size = @product.comments.size
    @sales_size = @product.have_sold?
    @comments = @product.comments.latest.page(page).per(per_page)

    if session[:order_id]
      check_order = Order.find(session[:order_id])
      @check_order = check_order.order_items.where(product_id: @product.id)
    end
  end


  private
    def collection
      @products = Product.verify
    end

    def resource
      if params[:id].include?('-')
        params_id = params[:id].split('-')
        product_id = params_id[0]
        catalog_id = params_id[1]
        @catalog = @subdomain.catalogs.find catalog_id
        @product = Product.find product_id
      else
        @product = Product.find params[:id]
      end
    end

    def categories
      @categories = Category.roots.oldest
    end

    def prepare_order
      @order_item = current_order.order_items.new
    end

    def prepare_simple_search
      @provinces = Region.where.not(ancestry: nil).alfa.pluck(:name)
    end
end

