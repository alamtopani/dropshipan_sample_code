class Managers::OrdersController < Managers::ApplicationController
  defaults resource_class: Order, collection_name: 'orders', instance_name: 'order'
  
  add_breadcrumb "Dashboard", :managers_dashboard_path
  add_breadcrumb "Pesanan", :collection_path

  def index
    order_report_day(@manager.orders.without_in_progress, 'with_search_by', :price, params[:export])
  end

  def show
    add_breadcrumb "#{params[:id]}"
    super
  end

  def detail
    add_breadcrumb "Konfirmasi Pembayaran", managers_confirmations_path
    add_breadcrumb "Detail"
    @confirmation = Confirmation.where("LOWER(confirmations.no_invoice) LIKE LOWER('#{params[:id]}')").first
    @order = Order.find_by(code: @confirmation.no_invoice.downcase)
  end

end