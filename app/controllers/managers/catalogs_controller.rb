class Managers::CatalogsController < Managers::ApplicationController
	defaults resource_class: Catalog, collection_name: 'catalogs', instance_name: 'catalog'

	add_breadcrumb "Dashboard", :managers_dashboard_path
	add_breadcrumb "Katalog", :collection_path

  def index
    @catalogs = current_user.catalogs.latest.approved.search_by(params)
    
    if params[:export] == 'true'
      @collection_all = @catalogs
    end
    @collection = @catalogs.page(page).per(per_page)

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def inactive
    add_breadcrumb "Pending"
    @catalogs = current_user.catalogs.latest.inactive.search_by(params)
    @collection_all = @catalogs
    @collection = @catalogs.page(page).per(per_page)
  end

  def download_catalogs
    @collection = Product.verify.search_by(params).latest.page(page).per(per_page)
    @download_catalogs = Product.verify.search_by(params).latest
    if params[:category_id].present?
      category = Category.find params[:category_id]
      @link_download_category = category.link_download rescue "#"
    end

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end
end