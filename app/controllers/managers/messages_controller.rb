class Managers::MessagesController < Managers::ApplicationController

  add_breadcrumb "dashboard", :managers_dashboards_path
  add_breadcrumb "messages", :managers_messages_path

  def index
    collection = @latest_messages_collection.sort_by {|e| e[4]}
    @collection_size = collection.count
    @collection = Kaminari.paginate_array(collection.reverse!).page(page).per(per_page)

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def create
    create_message(params[:group_message_id], params[:send_to], params[:message])

    respond_to do |format|
      if @message.save
        check_group_message_member(current_user.id, params[:send_to])
        format.html
        format.js
      end
    end
  end

  def blast_message
    members = params[:tags]
    message = params[:message][:message]
    if members.include? "all"
      @my_members.each do |member|
        group_message = GroupMessage.where(account_manager_id: current_user.id, member_id: member.id).first
        create_message(group_message.id, member.id, message)
        @message.save
      end
    else
      members.each do |member_id|
        group_message = GroupMessage.where(account_manager_id: current_user.id, member_id: member_id).first
        create_message(group_message.id, member_id, message)
        @message.save
      end
    end
    redirect_back(fallback_location: managers_members_path, notice: "Successfully send message")
  end

  private
    def create_message(group_message_id, send_to, message)
      @message = Message.new
      @message.group_message_id = group_message_id
      @message.from = current_user.id
      @message.to = send_to
      @message.message = message
    end
end