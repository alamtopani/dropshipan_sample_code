class Managers::HomeController < Managers::ApplicationController
  add_breadcrumb "Dashboard", :managers_dashboard_path

  def dashboard
    order_default = @manager.orders.without_in_progress
    order_report_day(order_default, 'with_search_by', :price, params[:export])
  end
end
