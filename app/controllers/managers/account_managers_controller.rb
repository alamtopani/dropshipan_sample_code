class Managers::AccountManagersController < Managers::ApplicationController
  defaults resource_class: AccountManager, collection_name: 'account_managers', instance_name: 'account_manager'

  add_breadcrumb "Dashboard", :managers_dashboard_path
  add_breadcrumb "Personal Data"

  def edit
    add_breadcrumb "Edit"
    super
  end
  
  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to request.referer || root_path, notice: "Akun anda telah berhasil dirubah"}
      else
        format.html {redirect_to request.referer || root_path, alert: resource.errors.full_messages}
      end
    end
  end
end
