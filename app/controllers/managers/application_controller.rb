class Managers::ApplicationController < Backend::ResourcesController
	layout 'application_manager'

	before_action :authenticate_manager!
	before_action :check_profile
	protect_from_forgery with: :exception

	def authenticate_manager!
		unless current_user.present? && (current_user.account_manager?)
			redirect_to root_path, alert: "Can't Access this page"
		else
			@manager = AccountManager.find_by(id: current_user.id)
			if @manager.present?
				@my_members = @manager.members.verified.order(username: :asc)
				@last_commissions = @manager.commissions.latest.limit(5)
				check_group_message(current_user.id)
			end
		end
	end

	def check_group_message(account_manager_id)
		@group_message = GroupMessage.where(account_manager_id: account_manager_id).first

		if @group_message.blank?
			@group_message = GroupMessage.create(account_manager_id: account_manager_id, status: 'active')
		end

		if @group_message.present?
			messages = Message.where(to: account_manager_id)
			@latest_messages = messages.latest
			latest_messages = messages
			messages_collection = latest_messages.group_by(&:from)
			set_messages_collection = set_collection_message(messages_collection)
			latest_messages_collection = set_messages_collection.sort_by {|e| e[3]}
			@latest_messages_collection = latest_messages_collection.reverse!
		end
	end

	def check_group_message_member(account_manager_id, member_id)
		@group_message = GroupMessage.where(account_manager_id: current_user.id, member_id: member_id).first

		if @group_message.blank?
			@group_message = GroupMessage.create(account_manager_id: current_user.id, member_id: member_id, status: 'active')
		end

		if @group_message.present?
			@member_messages = @group_message.messages.oldest
		end
	end

	protected
		def per_page
			params[:per_page] ||= 20
		end

		def page
			params[:page] ||= 1
		end

		def set_collection_message(messages)
			messages.values.map{|w| [w.map(&:id).last,
														w.map(&:from).last,
														w.map(&:message).last,
														w.map(&:status).last,
														w.map(&:created_at).last] 
													}
		end
end
