class Managers::CommissionsController < Managers::ApplicationController
  add_breadcrumb "Komisi", :managers_commissions_path

  def index
    commission_report_day(@manager.commissions, 'with_search_by', :commission_member, 'order', params[:export])

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end
end
