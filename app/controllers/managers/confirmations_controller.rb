class Managers::ConfirmationsController < Managers::ApplicationController
  defaults resource_class: Confirmation, collection_name: 'confirmations', instance_name: 'confirmation'

  add_breadcrumb "Dashboard", :managers_dashboard_path
  add_breadcrumb "Pesanan", :managers_orders_path
  add_breadcrumb "Konfirmasi Pembayaran", :collection_path

  def index
    @collection = @manager.confirmations.search_by(params).latest.page(page).per(per_page)
  end

end