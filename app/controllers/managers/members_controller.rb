class Managers::MembersController < Managers::ApplicationController
  defaults resource_class: Member, collection_name: 'members', instance_name: 'member'

  add_breadcrumb "Dashboard", :managers_dashboard_path
  add_breadcrumb "Resellers", :collection_path

  def index
    @collection_size = collection.size

    if params[:export] == 'true'
      @collection_all = collection.latest
    end
    @collection = collection.latest.page(page).per(per_page)

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    add_breadcrumb "messages"
    if resource.account_manager_id == current_user.id
      @profile = resource.profile
      check_group_message_member(@manager.id, resource.id)

      if @latest_messages.where(from: resource.id).present?
        @latest_messages.where(from: resource.id).each do |m|
          m.status = 'read'
          m.save
        end
      end
    else
      redirect_to :back, alert: "You can not access this page because he/she is not your reseller."
    end
  end

  def click_message
    message = Message.find(params[:message_id])
    message.status = "read"
    message.save

    redirect_to managers_member_path(params[:id])
  end
  
  protected
    def collection
       @collection = @manager.members.search_by_member(params)
    end
end
