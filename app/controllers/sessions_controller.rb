class SessionsController < Devise::SessionsController
  def create
    if current_user.present? && current_user.member?
      # ------ FOR STAGING
      # if check_domain_valid_dropshipan? == true
      #   if current_user.invoices.current_packages(current_user.package_id).paid.blank?
      #     reset_session
      #     redirect_to new_user_session_path, alert: "Maaf anda belum melakukan pembayaran paket yang dipilih, silahkan terlebih dahulu melakukan pembayaran!"
      #   elsif current_user.verified == false
      #     reset_session
      #     redirect_to new_user_session_path, alert: "Maaf anda belum bisa memasuki halaman Dashboard, karena akun anda masih dalam tahap verifikasi oleh kami, silahkan tunggu maximal 2x24jam atau hubungi kontak kami segera!"
      #   elsif current_user.membership == false
      #     reset_session
      #     redirect_to new_user_session_path, alert: "Maaf status membership anda untuk sementara di nonaktifkan, silahkan hubungi customer service kami untuk informasi lebih lanjut!"
      #   else
      #     self.resource = warden.authenticate!(auth_options)
      #     set_flash_message(:notice, :signed_in) if is_flashing_format?
      #     sign_in(resource_name, resource)
      #     yield resource if block_given?
      #     if request.xhr?
      #       render :json => {:success => true}
      #     else
      #       respond_with resource, location: after_sign_in_path_for(resource)
      #     end
      #   end
      # else
      #   reset_session
      #   redirect_to new_user_session_path, alert: "Anda tidak di izinkan memasuki halaman ini!"
      # end

      # ------ FOR DEVELOPMENT
      if current_user.invoices.current_packages(current_user.package_id).paid.blank?
        reset_session
        redirect_to new_user_session_path, alert: "Maaf anda belum melakukan pembayaran paket yang dipilih, silahkan terlebih dahulu melakukan pembayaran!"
      elsif current_user.verified == false
        reset_session
        redirect_to new_user_session_path, alert: "Maaf anda belum bisa memasuki halaman Dashboard, karena akun anda masih dalam tahap verifikasi oleh kami, silahkan tunggu maximal 2x24jam atau hubungi kontak kami segera!"
      elsif current_user.membership == false
        reset_session
        redirect_to new_user_session_path, alert: "Maaf status membership anda untuk sementara di nonaktifkan, silahkan hubungi customer service kami untuk informasi lebih lanjut!"
      else
        self.resource = warden.authenticate!(auth_options)
        set_flash_message(:notice, :signed_in) if is_flashing_format?
        sign_in(resource_name, resource)
        yield resource if block_given?
        if request.xhr?
          render :json => {:success => true}
        else
          respond_with resource, location: after_sign_in_path_for(resource)
        end
      end
    elsif current_user.present? && current_user.account_manager?
      if current_user.verified == false
        reset_session
        redirect_to new_user_session_path, alert: "Maaf anda belum bisa memasuki halaman Dashboard Manager, karena akun anda belum di verifikasi!"
      else
        self.resource = warden.authenticate!(auth_options)
        set_flash_message(:notice, :signed_in) if is_flashing_format?
        sign_in(resource_name, resource)
        yield resource if block_given?
        if request.xhr?
          render :json => {:success => true}
        else
          respond_with resource, location: after_sign_in_path_for(resource)
        end
      end
    elsif current_user.present? && current_user.admin?
      if current_user.verified == false
        reset_session
        redirect_to new_user_session_path, alert: "Maaf anda belum bisa memasuki halaman Dashboard Admin, karena akun anda belum di verifikasi!"
      else
        self.resource = warden.authenticate!(auth_options)
        set_flash_message(:notice, :signed_in) if is_flashing_format?
        sign_in(resource_name, resource)
        yield resource if block_given?
        if request.xhr?
          render :json => {:success => true}
        else
          respond_with resource, location: after_sign_in_path_for(resource)
        end
      end
    else
      self.resource = warden.authenticate!(auth_options)
      set_flash_message(:notice, :signed_in) if is_flashing_format?
      sign_in(resource_name, resource)
      yield resource if block_given?
      if request.xhr?
        render :json => {:success => true}
      else
        respond_with resource, location: after_sign_in_path_for(resource)
      end
    end
  end
end