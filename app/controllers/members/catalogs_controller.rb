class Members::CatalogsController < Members::ApplicationController
	defaults resource_class: Catalog, collection_name: 'catalogs', instance_name: 'catalog'

	add_breadcrumb "Dashboard", :members_dashboard_path
	add_breadcrumb "Katalog", :collection_path

	include MultipleAction

  def index
    @catalogs = current_user.catalogs.latest.approved.search_by(params)
    
    if params[:export] == 'true'
      @collection_all = @catalogs
    end
    @collection = @catalogs.page(page).per(per_page)
  
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def pending
    add_breadcrumb "Pending"
    @catalogs = current_user.catalogs.latest.pending.search_by(params)
    @collection_all = @catalogs
    @collection = @catalogs.page(page).per(per_page)
  
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def inactive
    add_breadcrumb "Inactive"
    @catalogs = current_user.catalogs.latest.inactive.search_by(params)
    @collection_all = @catalogs
    @collection = @catalogs.page(page).per(per_page)
  
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def download_catalogs
    @collection = Product.verify.search_by(params).latest.page(page).per(per_page)
    @download_catalogs = Product.verify.search_by(params).latest
    if params[:category_id].present?
      category = Category.find params[:category_id]
      @link_download_category = category.link_download rescue "#"
    end

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def new
    @member_catalogs = @member.catalogs
    list_products = Product.verify.where.not(id: @member_catalogs.pluck(:product_id)).search_by(params).latest
    @list_products = list_products.page(page).per(per_page)
    @list_products_size = list_products.size
    add_breadcrumb "New"
    super

  end

  def edit
    add_breadcrumb "Edit"
    super
  end

	def create
    if params[:product_ids].present?
  		params[:product_ids].each do |product_id|
  			resource = build_resource.blank? ? build_resource : Catalog.new
  			resource.product_id = product_id
        resource.member_id = current_user.id
  			resource.account_manager_id = @member.account_manager_id
        resource.price = resource.product.recommended_price
  			resource.status = "approved"
  			resource.save
  		end
  		# create! do |format|
  		respond_to do |format|
  			if resource.errors.empty?
  				format.html {redirect_to collection_path}
  			else
  				flash[:errors] = resource.errors.full_messages
  				format.html {redirect_to request.referer || root_path}
  			end
  		end
    else
      redirect_back(fallback_location: root_path, notice: "Tidak ada produk yang dipilih")
    end
	end

	def update
    price = params[:catalog][:price]
    discount = params[:catalog][:discount]
    update_price = price.to_f - (price.to_f * discount.to_f/100).to_f
    recommended_price = resource.product.recommended_price.to_f
    if update_price >= recommended_price
  		update! do |format|
  			if resource.errors.empty?
  				format.html {redirect_to collection_path}
          # format.js
  			else
  				flash[:errors] = resource.errors.full_messages
  				format.html {redirect_to request.referer || root_path}
          # format.js
  			end
  		end
    else
      redirect_back(fallback_location: root_path, alert: "Harga jual '#{resource.product.title}' tidak boleh lebih kecil dari #{helpers.get_currency(recommended_price)}")
    end
	end

  def destroy
    destroy! do |format|
      if resource.errors.empty?
        format.html {redirect_to :back}
      else
        format.html {redirect_to :back, error: resource.errors.full_messages}
      end
    end
  end

  def restore
    resource = collection.with_deleted.find params[:id]
    if resource.restore(recursive: true)
      redirect_to :back, notice: "Produk #{resource.code} berhasil dikembalikan!"
    else
      redirect_to :back, alert: 'Error'
    end
  end

  def change_status
    resource = Catalog.find(params[:id])
    if params[:button] == Catalog::INACTIVE
      resource.status = Catalog::INACTIVE
      resource.save
    elsif params[:button] == Catalog::APPROVED
      resource.status = Catalog::APPROVED
      resource.save
    end
    redirect_back(fallback_location: root_path, notice: "Perubahan status katalog #{resource.status} berhasil")
  end
end