class Members::GuestBooksController < Members::ApplicationController

  add_breadcrumb "Dashboard", :member_dashboard_path

  def index
  	guest_books = GuestBook.where(user_id: current_user.id).latest
  	guest_books.each do |g|
  		if g.status == false
	  		g.status = true
	  		g.save
	  	end
  	end
  	@guest_books = guest_books.page(page).per(per_page)
  end

end
