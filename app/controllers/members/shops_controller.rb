class Members::ShopsController < Members::ApplicationController
  add_breadcrumb "Dashboard", :members_dashboard_path
  add_breadcrumb "Shop", :collection_path
  include Ongkir

  before_action :prepare_order, except: [:index]
  before_action :prepare_simple_search, only: [:index]

  def index
    categories

    if params[:category].present?
      @category = Category.find_by(name: params[:category])
    end

    if @subdomain.present? && @subdomain.catalogs.present?
      @catalogs_all = @subdomain.catalogs.approved.verify.search_by(params)
      @catalogs = @catalogs_all.page(page).per(per_page)
    end
  end

  def show
    add_breadcrumb "#{params[:id]}"

    resource
    @merchant = resource.merchant
    @user_products = @merchant.products.verify.latest.limit(5)
    @profile = @merchant.profile
    @address = @merchant.address
    @contact = @merchant.contact
    impressionist(@product)

    @comments_size = @product.comments.size
    @sales_size = @product.have_sold?
    @comments = @product.comments.latest.page(page).per(per_page)

    if session[:order_id]
      check_order = Order.find(session[:order_id])
      @check_order = check_order.order_items.where(product_id: @product.id)
    end
  end

  def cart
    add_breadcrumb "My Order Items", orders_path

    @order = current_order
    @stakeholders = @order.stakeholder_orders.latest
    @shipping_methods = ShippingMethod.activated

    get_region_ongkir_province
  end

  def checkout
    if current_order.order_items.present? && current_order.order_items.first.product.merchant.featured == true
      @origin_name = current_order.order_items.first.product.merchant.address.place_city_name_merchant
      @origin_code = current_order.order_items.first.product.merchant.address.place_city_code_merchant
    else
      @origin_name = 'Kota Sukabumi'
      @origin_code = 431
    end
    
    if params[:order][:courier].include?('COD')
      @cod = true
      @price = 0
    else
      @cod = false
      prepare_shipping_price(params[:order][:shipping_method], params[:order][:address_attributes][:district], current_order.total_weight?, @origin_code)
    end
    
    if @price <= 0 && params[:order][:courier].include?('JASA PENGIRIMAN')
      redirect_to cart_members_shops_path, alert: "Pengiriman ke kecamatan #{params[:order][:address_attributes][:district]} tidak tersedia, silahkan pilih kecamatan lain!"
    else
      if order = Order.pay(@subdomain, current_order, prepare_shipping_address, @price, @cod)
        if @cod == true
          params[:order][:payer_name] = @subdomain.username rescue '-'
          params[:order][:payer_email] = @subdomain.email rescue '-'
          params[:order][:payer_phone] = @subdomain.profile.phone rescue '-'
        end

        if order.update(permitted_params)
          if @cod.blank?
            update_address(order)
            create_activity(order, @subdomain.id, "Pesanan baru dibuat (#{@subdomain.username})", "Tujuan pengiriman ke #{order.payer_address}")
            redirect_to review_members_shop_path(order.payment_token), notice: 'Terima kasih telah memesan produk kami, berikut ini adalah faktur dari keranjang belanja Anda yang telah berhasil dipesan, silakan lakukan pembayaran segera dan konfirmasi pembayaran Anda kepada kami!'
          else
            create_activity(order, @subdomain.id, "Pesanan baru dibuat (#{@subdomain.username})", "COD/Pesanan diambil langsung di kantor Dropshipan")
            redirect_to review_members_shop_path(order.payment_token), notice: 'Terima kasih telah memesan produk kami, berikut ini adalah faktur dari keranjang belanja Anda yang telah berhasil dipesan, silakan pesanan diambil dikantor Merchant Langsung'
          end

          UserMailer.order_checkout(order).deliver
          session[:order_id] = nil if current_order
        end
      else
        redirect_to cart_members_shops_path, alert: 'Something went wrong. Please try again.'
      end
    end
  end

  def review
    add_breadcrumb "Telah Sukses Melakukan Pemesanan"

    @order = Order.find_by(payment_token: params[:id])
    if @order.blank?
      @order = Order.find_by(code: params[:id])
    end
    @stakeholders = @order.stakeholder_orders.latest
    
    unless @order
      redirect_to root_path, notice: 'You do not have permission to access this page'
    end

    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Invoice Order - #{@order.code}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  private
    def collection
      @products = Product.verify
    end

    def resource
      if params[:id].include?('-')
        params_id = params[:id].split('-')
        product_id = params_id[0]
        catalog_id = params_id[1]
        @catalog = @subdomain.catalogs.find catalog_id
        @product = Product.find product_id
      else
        @product = Product.find params[:id]
      end
    end

    def categories
      @categories = Category.roots.oldest
    end

    def prepare_order
      @order_item = current_order.order_items.new
    end

    def prepare_simple_search
      @provinces = Region.where.not(ancestry: nil).alfa.pluck(:name)
    end

    def permitted_params
      params.require(:order).permit(Permitable.controller("orders"))
    end

    def prepare_shipping_address
      address = [
        params[:order][:address_attributes][:province].split(':')[1],
        params[:order][:address_attributes][:city].split(':')[1],
        params[:order][:address_attributes][:district].split(':')[1],
        params[:order][:address_attributes][:address],
        params[:order][:address_attributes][:postcode]
      ].select(&:'present?').join(', ')
    end

    def update_address(order)
      order.origin_code = @origin_code
      order.origin_name = @origin_name
      order.save
      
      address = order.address
      address.province = params[:order][:address_attributes][:province].split(':')[1]
      address.city = params[:order][:address_attributes][:city].split(':')[1]
      address.district = params[:order][:address_attributes][:district].split(':')[1]
      address.district_full = params[:order][:address_attributes][:district]
      address.save
    end
end
