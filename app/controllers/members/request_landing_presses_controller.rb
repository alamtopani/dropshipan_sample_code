class Members::RequestLandingPressesController < Members::ApplicationController
 defaults resource_class: RequestLandingPress, collection_name: 'request_landing_presses', instance_name: 'request_landing_press'

  add_breadcrumb "Dashboard", :members_dashboard_path
  add_breadcrumb "Landing Press", :collection_path

  after_action :prepare_member_id, only: [:create]

  def index
    @collection = @member.request_landing_presses.latest.search_by(params).page(page).per(per_page)
  end

  def new
  	get_landing_press
  end

  def create
    build_resource
    resource.meta_title = resource.landing_press.title1
    resource.price = resource.landing_press.price
    resource.contact = current_user.profile.phone
    respond_to do |format|
      if resource.errors.empty?
        format.html {redirect_to collection_path, notice: "Sukses, Landing page #{resource.landing_press.title1} berhasil dibuat!"}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to request.referer || root_path}
      end
    end
  end

  protected
    def get_landing_press
      used_lp_ids = @member.request_landing_presses.latest.pluck(:landing_press_id)
	    @get_landing_press = LandingPress.activated.oldest.where.not(id: used_lp_ids).search_by(params).page(page).per(per_page)
	  end

	  def prepare_member_id
	  	resource.member_id = current_user.id
	  	resource.save
	  end
end
