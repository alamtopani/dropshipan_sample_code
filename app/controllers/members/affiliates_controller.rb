class Members::AffiliatesController < Members::ApplicationController
	defaults resource_class: Affiliate, collection_name: 'affiliates', instance_name: 'affiliate'

  add_breadcrumb "Dashboard", :members_dashboard_path
  add_breadcrumb "Affiliates", :collection_path

  def index
    @collection_pending = collection.pending.sum(:fee_member)
    @collection_process = collection.process.sum(:fee_member)
    @collection_paid = collection.paid.sum(:fee_member)
    @collection_rejects = collection.rejects.sum(:fee_member)

    @collection_size = collection.size

    if params[:export] == 'true'
      @collection_all = collection.latest
    end
  	@collection = collection.latest.page(page).per(per_page)

    respond_to do |format|
      format.html 
      format.xls
      format.js
      format.xls
    end
  end

  protected
    def collection
       @collection = @member.affiliates.search_by(params)
    end
end
