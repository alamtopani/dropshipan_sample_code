class Members::OrdersController < Members::ApplicationController
  defaults resource_class: Order, collection_name: 'orders', instance_name: 'order'
  
  add_breadcrumb "Dashboard", :members_dashboard_path
  add_breadcrumb "Pesanan", :collection_path

  def index
    order_report_day(@member.orders.without_in_progress, 'with_search_by', :price, params[:export])
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    add_breadcrumb "#{params[:id]}"
    super
  end

  def update
    update! do |format|
      if resource.errors.empty?
        format.html {redirect_to :back}
      else
        flash[:errors] = resource.errors.full_messages
        format.html {redirect_to :back}
      end
    end
  end

  def detail
    add_breadcrumb "Konfirmasi Pembayaran", members_confirmations_path
    add_breadcrumb "Detail"
    @confirmation = Confirmation.where("LOWER(confirmations.no_invoice) LIKE LOWER('#{params[:id]}')").first
    @order = Order.find_by(code: @confirmation.no_invoice.downcase)
  end

end