class Members::ApplicationController < Backend::ResourcesController
	layout 'userpage'

	before_action :authenticate_member!
	before_action :check_profile
	protect_from_forgery with: :exception
  helper_method :current_order

	def authenticate_member!
		unless current_user.present? && (current_user.member?)
			redirect_to root_path, alert: "Can't Access this page"
		else
			@member = Member.find_by(id: current_user.id)
			if @member.present? && @member.invoices.present?
				if current_user.membership == false
					reset_session
					redirect_to new_user_session_path, alert: "Maaf status membership anda untuk sementara di nonaktifkan, silahkan hubungi customer service kami untuk informasi lebih lanjut!"
				end
        @subdomain = @member
        @setting = @subdomain.web_setting
				check_group_message(current_user.id, current_user.account_manager_id)
				@guest_books_latest = GuestBook.where(user_id: current_user.id)
        @last_commissions = @member.commissions.latest.limit(5)
			end
		end
	end

	def check_group_message(member_id, account_manager_id)
		@group_message = GroupMessage.where(member_id: member_id, account_manager_id: account_manager_id).first

		if @group_message.blank?
			@group_message = GroupMessage.create(member_id: member_id, account_manager_id: account_manager_id, status: 'active')
		end

		if @group_message.present?
			@messages = @group_message.messages.oldest
			@latest_messages = @group_message.messages.where(to: member_id).latest
		end
	end

	protected
		def per_page
			params[:per_page] ||= 20
		end

		def page
			params[:page] ||= 1
		end

    def current_order
      if !session[:order_id].nil?
        Order.find(session[:order_id])
      else
        Order.new
      end
    end
end
