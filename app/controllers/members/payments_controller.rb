class Members::PaymentsController < Members::ApplicationController
	defaults resource_class: PaymentSnapshot, collection_name: 'payment_snapshots', instance_name: 'payment_snapshot'

	add_breadcrumb "Dashboard", :members_dashboard_path
	add_breadcrumb "payment_snapshot", :collection_path

	include MultipleAction

	def index
		@collection_size = collection.size
		@collection = collection.latest.page(page).per(per_page)

		if params[:export] == 'true'
			@collection_all = collection
		end

		respond_to do |format|
			format.html
			format.js
			format.xls
		end
	end

	def show
		resource = PaymentSnapshot.find_by(code: params[:id])
		@resource = resource
		respond_to do |format|
			format.html
			format.pdf do
				render pdf: "Pembayaran - #{helpers.get_date(resource.payment_start)} s.d. #{helpers.get_date(resource.payment_end)}",
				layout: 'layouts/print_layout.html.erb',
				show_as_html: params[:debug].present?
			end
		end
	end

	protected
		def collection
			@collection = @member.payment_snapshots.search_by(params)
		end
end
