class Members::ReferralsController < Members::ApplicationController
  defaults resource_class: Member, collection_name: 'members', instance_name: 'member'

  add_breadcrumb "Dashboard", :members_dashboard_path
  add_breadcrumb "Referral", :collection_path

  include MultipleAction

  def index
    @referral_verified = collection.verified
    @referral_not_verified = collection.not_verified
    @referral_referral1 = collection.referral1(@member.id)
    @referral_referral2 = collection.referral2(@member.id)
    @referral_referral3 = collection.referral3(@member.id)
    
    @collection_size = collection.size

    if params[:export] == 'true'
      @collection_all = collection.latest
    end
    @collection = collection.latest.page(page).per(per_page)

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  protected
    def collection
      @collection = @member.referrals.search_by(params)
    end

end
