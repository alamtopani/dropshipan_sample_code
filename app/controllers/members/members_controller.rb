class Members::MembersController < Members::ApplicationController
  defaults resource_class: Member, collection_name: 'members', instance_name: 'member'

  add_breadcrumb "Dashboard", :members_dashboard_path
  add_breadcrumb "Personal Data"

  def edit
    add_breadcrumb "Edit"
    super
  end

  def change_theme
    # unless @member.member? && @member.package.premium_or_advance? == true
    #   redirect_to members_dashboard_path, alert: "Anda tidak diperbolehkan mengakses halaman ini"
    # end
  end

  def update_change_theme
    @member = Member.find(params[:id])
    @site_info = @member.site_info
    @site_info.web_template_id = params[:member][:site_info_attributes][:web_template_id]
    if @site_info.save
      redirect_to request.referer || root_path, alert: "Tampilan Website anda berhasil dirubah."
    else
      redirect_to request.referer || root_path, alert: "Tampilan Website anda gagal dirubah."
    end
  end
  
  def update
    # domain_name = params[:member][:site_info_attributes][:domain_name]
    # if domain_name.blank?
    #   redirect_to request.referer || root_path, alert: 'Nama Domain anda tidak di isi, harap di isi terlebih dahulu!'
    # elsif check_domain(domain_name).present?
    #   redirect_to request.referer || root_path, alert: 'Maaf Nama Domain yang anda masukan telah dipergunakan oleh orang lain, silahkan pilih Nama Domain lain!'
    # elsif check_domain_name_valid(domain_name).present?
    #   redirect_to request.referer || root_path, alert: 'Maaf Nama Domain yang anda masukan tidak valid (tidak boleh menggunakan spasi, titik, koma Dll) silahkan pilih Nama Domain lain!'
    # else
    
    domain_exists = false
    domain_process = false
    domain_settings = params[:member][:domain_settings_attributes]
    if domain_settings.present?
      domain_settings.each do |key, val|
        if DomainSetting.where.not(member_id: current_user.id).exists?(name: val["name"])
          domain_exists = true
        end
        if @member.domain_settings.where.not(status: "pending").present?
          unless @member.domain_settings.exists?(name: val["name"]) 
            domain_process = true
          end
        end
      end
    end
    if domain_exists.present? && domain_exists == true
      redirect_to request.referer || root_path, alert: "Maaf nama domain sudah dipergunakan, silahkan ganti dengan nama domain lain."
    elsif domain_process.present? && domain_process == true
      redirect_to request.referer || root_path, alert: "Maaf nama domain tidak bisa diubah karena domain sedang diproses."
    else
      update! do |format|
        if resource.errors.empty?
          format.html {redirect_to request.referer || root_path, notice: 'Akun anda berhasil diubah!'}
        else
          format.html {redirect_to request.referer || root_path, alert: resource.errors.full_messages}
        end
      end
    end

    # end
  end

  def check_domain(domain)
    members = SiteInfo.where.not(member_id: resource.id).where(domain_name: domain)
  end

  def check_domain_name_valid(domain)
    if domain.include?('.') || domain.include?(',') || domain.include?(' ')
      return true
    else
      return false
    end
  end
end
