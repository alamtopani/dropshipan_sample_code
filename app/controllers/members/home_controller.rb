class Members::HomeController < Members::ApplicationController
  add_breadcrumb "Dashboard", :members_dashboard_path

  def dashboard
    order_default = @member.orders.without_in_progress
    order_report_day(order_default, 'with_search_by', :price, params[:export])
  end
end
