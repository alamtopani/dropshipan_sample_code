class Members::MessagesController < Members::ApplicationController

  add_breadcrumb "dashboard", :members_dashboards_path
  add_breadcrumb "messages", :members_messages_path

  def create
    @message = Message.new
    @message.group_message_id = params[:group_message_id]
    @message.from = current_user.id
    @message.to = current_user.account_manager_id
    @message.message = params[:message]

    respond_to do |format|
      if @message.save
        check_group_message(current_user.id, current_user.account_manager_id)
        format.html
        format.js
      end
    end
  end
end