class Members::ConfirmationsController < Members::ApplicationController
  defaults resource_class: Confirmation, collection_name: 'confirmations', instance_name: 'confirmation'

  add_breadcrumb "Dashboard", :members_dashboard_path
  add_breadcrumb "Pesanan", :members_orders_path
  add_breadcrumb "Konfirmasi Pembayaran", :collection_path

  def index
    @collection = @member.confirmations.search_by(params).latest.page(page).per(per_page)
  end

  def new
    @confirmation = Confirmation.new
  end

  def create
    check_invoice(params[:confirmation][:no_invoice])

    if @invoice.present?
      @confirmation = Confirmation.new(permitted_params)
      @confirmation.member_id = @subdomain.id
      @confirmation.account_manager_id = @confirmation.member.account_manager_id
      @confirmation.order_id = @invoice.id
      if @confirmation.save
        UserMailer.send_confirmation(@confirmation).deliver
        redirect_to members_confirmations_path, notice: 'Anda telah berhasil mengirim Konfirmasi Pembayaran anda, silahkan tunggu kami akan melakukan proses pengecekan terlebih dahulu maximal menunggu 1x24 jam!'
      else
        flash[:errors] = @confirmation.errors.full_messages
        redirect_to request.referer || members_orders_path
      end
    else
      redirect_to request.referer || members_orders_path, alert: 'Tidak ada nomor invoice yang ditemukan!'
    end
  end

  private
    def permitted_params
      params.require(:confirmation).permit(Permitable.controller("confirmation_invoices"))
    end

    def check_invoice(no_invoice)
      @invoice = Order.find_by(code: no_invoice.downcase)
    end

end