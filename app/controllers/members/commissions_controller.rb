class Members::CommissionsController < Members::ApplicationController
  add_breadcrumb "Komisi", :members_commissions_path

  def index
    commission_report_day(@member.commissions, 'with_search_by', :commission_member, 'order', params[:export])

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def referral
    target_payment_referral(@member)
    commission_report_day(@member.commission_referrals, 'with_search_by', :commission_value, 'referral', params[:export])
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end
end
