class Members::PaymentReferralsController < Members::ApplicationController
  defaults resource_class: PaymentReferral, collection_name: 'payment_referrals', instance_name: 'payment_referral'

  add_breadcrumb "Dashboard", :members_dashboard_path
  add_breadcrumb "Payment Referral", :collection_path

  def index
    target_payment_referral(@member)
    @collection = collection.latest.page(page).per(per_page)

    if params[:export] == 'true'
      @collection_all = collection
    end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def show
    @resource = PaymentReferral.find_by(code: params[:id])
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Pembayaran Referral- #{helpers.get_date(@resource.payment_start)} s.d. #{helpers.get_date(@resource.payment_end)}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  protected
    def collection
      @collection = @member.payment_referrals.search_by(params)
    end
end
