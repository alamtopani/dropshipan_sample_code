class Members::BlogsController < Members::ApplicationController
	defaults resource_class: Blog, collection_name: 'blogs', instance_name: 'blog'

  def index
    @blogs_active = collection.active.count
    @collection = collection.latest.active.page(page).per(20)

    respond_to do |format|
    	format.html
    	format.js
    	format.xls
    end
  end

  protected
    def collection
      @collection ||= end_of_association_chain.search_by(params)
    end
end