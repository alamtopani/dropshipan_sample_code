class Members::PackagesController < Members::ApplicationController
	add_breadcrumb "Paket", :members_packages_path

  def index
    @packages = Package.active.oldest
    @pending_packages_member = Invoice.pending_packages_member(@member.id)

    if @pending_packages_member.present?
      flash.now[:alert] = "Maaf anda belum melakukan pembayaran paket yang dipilih (Nominal pembayaran bisa dilihat di menu Tagihan), silahkan terlebih dahulu melakukan pembayaran!"
    end
  end

  def upgrade
    package = Package.find params[:package_id]
    payment = WebSetting.first.payments.first

    if @member.package.period < package.period
      title_upgrade = "upgrade"
    else
      title_upgrade = "downgrade"
    end

    @invoice = Invoice.new
    @invoice.user_id = @member.id
    @invoice.subject = "Invoice #{title_upgrade} Paket #{package.title}"
    @invoice.description = "Invoice #{title_upgrade} Paket #{package.title}"
    @invoice.price = package.price
    @invoice.bank_name = payment.bank_name
    @invoice.bank_branch = "Sukabumi"
    @invoice.account_name = payment.name
    @invoice.account_number = payment.account_number
    @invoice.invoiceable_id = package.id
    @invoice.invoiceable_type = package.class.name

    if @invoice.save             
      redirect_to members_dashboard_path, alert: "Permintaan #{title_upgrade} paket telah berhasil, silahkan lakukan pembayaran paket."
    else
      redirect_back(fallback_location: members_dashboard_path, alert: "Permintaan upgrade paket telah gagal, silahkan ulangi lagi.")
    end
  end

end
