class Members::InvoicesController < Members::ApplicationController
	defaults resource_class: Invoice, collection_name: 'invoices', instance_name: 'invoice'

  add_breadcrumb "Dashboard", :members_dashboard_path
  add_breadcrumb "Invoices", :collection_path

  def index
    @collection_size = collection.size

    if params[:export] == 'true'
      @collection_all = collection.latest
    end
  	@collection = collection.latest.page(page).per(per_page)

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    @resource = Invoice.find_by(code: params[:id])
    resource = @resource
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Tagihan Paket - #{helpers.get_date(resource.period_start)} s.d. #{helpers.get_date(resource.period_end)}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end
  
  protected
    def collection
       @collection = @member.invoices.search_by(params)
    end
end
