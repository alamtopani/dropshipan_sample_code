class Members::ManagersController < Members::ApplicationController
  before_action :check_profile
  
  add_breadcrumb "dashboard", :members_dashboards_path
  add_breadcrumb "my manager", :members_managers_path

  def index
    @manager = @member.account_manager
    @profile_manager = @manager.try(:profile)

    if @latest_messages.present?
      @latest_messages.each do |m|
        m.status = 'read'
        m.save
      end
    end
  end
end