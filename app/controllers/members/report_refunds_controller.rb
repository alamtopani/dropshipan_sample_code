class Members::ReportRefundsController < Members::ApplicationController
  defaults resource_class: ReportRefund, collection_name: 'report_refunds', instance_name: 'report_refund'

  add_breadcrumb "Dashboard", :members_dashboard_path
  add_breadcrumb "Refund Order", :collection_path

  def index
    @collection_size = collection.size

    if params[:export] == 'true'
      @collection_all = collection.latest
    end
    @collection = collection.latest.page(page).per(per_page)

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    order = Order.find_by(code: params[:id])
    @resource = order.report_refund
    resource = @resource
    @order = resource.order
    @confirmation = Confirmation.where("LOWER(confirmations.no_invoice) LIKE LOWER('#{@order.code}')").first

    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Refund Order - #{@order.code}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  protected
    def collection
      @collection = @member.report_refunds.search_by(params)
    end
end
