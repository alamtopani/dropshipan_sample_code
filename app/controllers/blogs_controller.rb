class BlogsController < ApplicationController
	# defaults resource_class: Blog, collection_name: 'blogs', instance_name: 'blog'
	layout 'official'

	def index
		@collection = Blog.latest.active.search_by(params).page(page).per(12)

		respond_to do |format|
			format.html
			format.js
		end
	end

	def show
		@resource = Blog.find(params[:id])
		@tips_count = Blog.active.tips.count
		@tutorial_count = Blog.active.tutorial.count
		impressionist(@resource)

		@collection = Blog.latest.active.where.not(id: @resource.id)
	end
	
end