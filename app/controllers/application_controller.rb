class ApplicationController < ActionController::Base
  include ApplicationHelper
  include DeviseHelper
  include ReportHelper
  include UserHelper
  include TheUser::DeviseFilter
  protect_from_forgery with: :exception
  before_action :web_setting, :set_current_user
  helper_method :current_order

  protected
    def per_page
      params[:per_page] ||= 36
    end

    def page
      params[:page] ||= 1
    end

    def web_setting
      @setting_global = WebSetting.first
    end

    def set_current_user
      User.current = current_user
    end

    def current_order
      if !session[:order_id].nil?
        Order.find(session[:order_id])
      else
        Order.new
      end
    end

    def target_payment_referral(member)
      @date_zone = Time.zone.now.to_date
      @last_month = (@date_zone.beginning_of_month - 1.month)
      @beginning_of_month = @date_zone.beginning_of_month
      @half_end_of_month = @date_zone.beginning_of_month + 14.days
      @beginning_half_of_month = @half_end_of_month + 1.days
      @end_of_month = @date_zone.end_of_month

      @half_end_of_last_month = @last_month.beginning_of_month + 15.days
      @end_of_last_month = @last_month.end_of_month

      if @date_zone >= @beginning_of_month && @date_zone <= @half_end_of_month
        @beginning_of_last = @half_end_of_last_month
        @end_of_last = @end_of_last_month
        @beginning_of_ongoing = @beginning_of_month
        @end_of_ongoing = @half_end_of_month

        @last_total_orders = member.orders.where("DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=? AND orders.order_status_id =?", @beginning_of_last, @end_of_last, 3).sum(:company_price)
        @ongoing_total_orders = member.orders.where("DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=? AND orders.order_status_id =?", @beginning_of_ongoing, @end_of_ongoing, 3).sum(:company_price)
        @last_order_percent = '%.4f' % (('%.4f' % (@last_total_orders.to_f/PaymentReferral::ORDER_TARGET)).to_f * 100)
        @ongoing_order_percent = '%.4f' % (('%.4f' % (@ongoing_total_orders.to_f/PaymentReferral::ORDER_TARGET)).to_f * 100)
      else
        @beginning_of_last = @beginning_of_month
        @end_of_last = @half_end_of_month
        @beginning_of_ongoing = @beginning_half_of_month
        @end_of_ongoing = @end_of_month

        @last_total_orders = member.orders.where("DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=? AND orders.order_status_id =?", @beginning_of_last, @end_of_last, 3).sum(:company_price)
        @ongoing_total_orders = member.orders.where("DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=? AND orders.order_status_id =?", @beginning_of_ongoing, @end_of_ongoing, 3).sum(:company_price)
        @last_order_percent = '%.4f' % (('%.4f' % (@last_total_orders.to_f/PaymentReferral::ORDER_TARGET)).to_f * 100)
        @ongoing_order_percent = '%.4f' % (('%.4f' % (@ongoing_total_orders.to_f/PaymentReferral::ORDER_TARGET)).to_f * 100)    
      end
    end

    def create_activity(resource, user_id, title, description)
      Activity.create(user_id: user_id, title: title, description: description, activitiable_id: resource.id, activitiable_type: resource.class.name)
    end
end
