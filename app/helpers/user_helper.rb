module UserHelper
  def check_profile
    if user_signed_in?
      if current_user.member?
        unless params[:controller] == "members/members" && (params[:action] == "update" || params[:action] == "edit")
          if @member.site_info.domain_name.blank?
            redirect_to edit_members_member_path(current_user), alert: 'Silahkan tentukan Nama Domain anda terlebih dahulu di menu Domain Setting!'
          elsif @member.profile.bank_name.blank? || @member.profile.bank_branch.blank? || @member.profile.account_name.blank? || @member.profile.account_number.blank?
            redirect_to edit_members_member_path(current_user), alert: 'Silahkan lengkapi Rekening anda terlebih dahulu!'
          end
          # if params[:controller] == "members/home" && params[:action] == "dashboard" && @member.invoices.current_packages(@member.package_id).paid.last.check_period_expired == true
          #   flash.now[:alert] = "Maaf anda belum melakukan perpanjangan pembayaran paket yang dipilih, silahkan terlebih dahulu melakukan pembayaran untuk memperpanjangnya!"
          # end
        end
      end
    else
      redirect_to root_path, alert: 'Your session has expired!'
    end
  end

  def check_domain_valid_dusbelanja?
    domain = DomainSetting.active.find_by(name: request.domain)
    if domain.present? || request.url.include?('dusbelanja.com')
      return true
    else
      return false
    end
  end

  def check_domain_valid_dropshipan?
    if request.url.include?('dropshipan.com') || request.url.include?('dropshipan.co.id')
      return true
    else
      return false
    end
  end

  def name_server1
    name_server1 = "ns.cloudflase.com"
  end

  def name_server2
    name_server2 = "bill.cloudflase.com"
  end
end