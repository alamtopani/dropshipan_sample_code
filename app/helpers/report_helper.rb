module ReportHelper

	def order_report_day(reports, with_search, key, export)
		@date = Time.zone.now.to_date
		# ---- default
		reports_for_daily = reports.paid
		reports = reports.where("DATE(orders.purchased_at) >=?", @date.to_date).where("DATE(orders.purchased_at) <=?", @date.to_date) if params[:start_at].blank? && (params[:controller] == 'members/home' || params[:controller] == 'managers/home' || params[:controller] == 'backend/home' || params[:controller] == 'merchants/home')
		@order_default = reports.search_by(params)

		# ---- status
		reports = @order_default if with_search == 'with_search_by'
		@order_in_progress = reports.in_progress
		@order_verification = reports.verification
		@order_paid = reports.paid
		@order_cancelled = reports.cancelled

		# ---- daily, count, paginate
		@reports_collect = reports

		# ---- Anlytic
		orders_paid = @order_default.paid
		@daily = orders_paid.group_by_day("orders.created_at", format: "%d %b")
		@monthly = orders_paid.group_by_month("orders.created_at", format: "%b %Y")
		@yearly = orders_paid.group_by_year("orders.created_at", format: "%Y")

		if @member.present?
			@commission_default = @member.commissions.all_approved.search_by(params)
			@order_item_default = @member.order_items.bonds.paid.not_refund_and_cancelled.search_by(params)
    elsif @manager.present?
      @commission_default = @manager.commissions.all_approved.search_by(params)
      @order_item_default = @manager.order_items.bonds.paid.not_refund_and_cancelled.search_by(params)
		elsif @merchant.present?
			@order_item_default = @merchant.order_items.bonds.paid.not_refund_and_cancelled.search_by(params)
		else
			@commission_default = Commission.all_approved.search_by(params)
			@order_item_default = OrderItem.bonds.paid.not_refund_and_cancelled.search_by(params)

			@visitors = Ahoy::Visit.search_by(params)
			@visitors = @visitors.where("DATE(started_at) >=?", @date.to_date).where("DATE(started_at) <=?", @date.to_date) if params[:start_at].blank?
			@daily_visit = @visitors.group_by_day("started_at", format: "%d %b").size
			@monthly_visit = @visitors.group_by_month("started_at", format: "%b %Y").size
			@yearly_visit = @visitors.group_by_year("started_at", format: "%Y").size

			@register_users = Member.search_by(params)
			@register_users = @register_users.where("DATE(users.created_at) >=?", @date.to_date).where("DATE(users.created_at) <=?", @date.to_date) if params[:start_at].blank?
			@daily_register_users = @register_users.group_by_day("users.created_at", format: "%d %b").size
			@monthly_register_users = @register_users.group_by_month("users.created_at", format: "%b %Y").size
			@yearly_register_users = @register_users.group_by_year("users.created_at", format: "%Y").size

			@register_invoices = Invoice.paid.non_active_promo.search_by(params)
			@register_invoices = @register_invoices.where("DATE(invoices.created_at) >=?", @date.to_date).where("DATE(invoices.created_at) <=?", @date.to_date) if params[:start_at].blank?
			@daily_register_invoices = @register_invoices.group_by_day("invoices.created_at", format: "%d %b")
			@monthly_register_invoices = @register_invoices.group_by_month("invoices.created_at", format: "%b %Y")
			@yearly_register_invoices = @register_invoices.group_by_year("invoices.created_at", format: "%Y")
		end

		initialize_commision(@commission_default) unless @merchant.present?
		initialize_order_item(@order_item_default)
		initialize_order_report_day(reports_for_daily, key)
		
    if export == 'true'
    	@collection_all = @order_default.latest
		end

		@collection = @order_default.latest.page(page).per(per_page)
	end

	def initialize_commision(commission_default)
		@commission_default = commission_default.where("DATE(commissions.commission_date) >=?", @date.to_date).where("DATE(commissions.commission_date) <=?", @date.to_date) if params[:start_at].blank?
		@daily_commission = @commission_default.group_by_day("commissions.commission_date", format: "%d %b")
		@monthly_commission = @commission_default.group_by_month("commissions.commission_date", format: "%b %Y")
		@yearly_commission = @commission_default.group_by_year("commissions.commission_date", format: "%Y")
	end

	def initialize_order_item(order_item_default)
		@order_item_default = order_item_default.where("DATE(order_items.created_at) >=?", @date.to_date).where("DATE(order_items.created_at) <=?", @date.to_date) if params[:start_at].blank?
		@top_order_on_list = @order_item_default.group(:product_id).sum(:quantity).map{|o| [Product.find(o[0]).title, o[1]] }.sort_by{|o| o[1]}.reverse.take(5)
	end

	def initialize_order_report_day(reports, key)
		@earning_today = reports.where("DATE(orders.created_at) =?", @date).sum(key) rescue 0
		@earning_yesterday = reports.where("DATE(orders.created_at) =?", @date-1.days).sum(key) rescue 0
		@earning_last_7_day = reports.where("DATE(orders.created_at) >=?", @date-6.days).where("DATE(orders.created_at) <=?", @date).sum(key) rescue 0
		@earning_this_month = reports.where("DATE(orders.created_at) >=?", @date.beginning_of_month).where("DATE(orders.created_at) <=?", @date).sum(key) rescue 0
	end

	def commission_report_day(reports, with_search, key, subject, export)
		@date = Time.zone.now.to_date
		# ---- default
		reports_for_daily = reports
		if subject == 'order'
			reports = reports.where("DATE(commissions.commission_date) >=?", @date.to_date).where("DATE(commissions.commission_date) <=?", @date.to_date) if params[:start_at].blank? && (params[:controller] == 'members/commissions' || params[:controller] == 'managers/commissions' || params[:controller] == 'backend/commissions')
		elsif subject == 'referral'
			reports = reports.where("DATE(commission_referrals.commission_date) >=?", @date.to_date).where("DATE(commission_referrals.commission_date) <=?", @date.to_date) if params[:start_at].blank? && (params[:controller] == 'members/commissions' && params[:action] == 'referral' || params[:controller] == 'backend/commission_referrals')
		end
		@commission_default = reports.search_by(params)

		# ---- status
		reports = @commission_default if with_search == 'with_search_by'
		@commission_pending = reports.all_pending
		@commission_reject = reports.all_reject
		@commission_approved = reports.all_approved

		# ---- daily, count, paginate
		@commission_collect = reports
		if subject == 'order'
			# ---- Anlytic
			@daily = @commission_default.all_approved.group_by_day("commissions.commission_date", format: "%d %b")
			@monthly = @commission_default.all_approved.group_by_month("commissions.commission_date", format: "%b %Y")
			@yearly = @commission_default.all_approved.group_by_year("commissions.commission_date", format: "%Y")
			initialize_commission_report_day(reports_for_daily, key)
		elsif subject == 'referral'
			# ---- Anlytic
			@daily = @commission_default.all_approved.group_by_day("commission_referrals.commission_date", format: "%d %b")
			@monthly = @commission_default.all_approved.group_by_month("commission_referrals.commission_date", format: "%b %Y")
			@yearly = @commission_default.all_approved.group_by_year("commission_referrals.commission_date", format: "%Y")
			key = :commission_value
			initialize_commission_referral_report_day(reports_for_daily, key)
		end

		if export == 'true'
	    @collection_all = @commission_default.latest
		end
		@collection = @commission_default.latest.page(page).per(per_page)
	end

	def initialize_commission_report_day(reports, key)
		@earning_today = reports.where("DATE(commissions.commission_date) =?", @date).sum(key) rescue 0
		@earning_yesterday = reports.where("DATE(commissions.commission_date) =?", @date-1.days).sum(key) rescue 0
		@earning_last_7_day = reports.where("DATE(commissions.commission_date) >=?", @date-6.days).where("DATE(commissions.commission_date) <=?", @date).sum(key) rescue 0
		@earning_this_month = reports.where("DATE(commissions.commission_date) >=?", @date.beginning_of_month).where("DATE(commissions.commission_date) <=?", @date).sum(key) rescue 0
	end

	def initialize_commission_referral_report_day(reports, key)
		@earning_today = reports.where("DATE(commission_referrals.commission_date) =?", @date).sum(key) rescue 0
		@earning_yesterday = reports.where("DATE(commission_referrals.commission_date) =?", @date-1.days).sum(key) rescue 0
		@earning_last_7_day = reports.where("DATE(commission_referrals.commission_date) >=?", @date-6.days).where("DATE(commission_referrals.commission_date) <=?", @date).sum(key) rescue 0
		@earning_this_month = reports.where("DATE(commission_referrals.commission_date) >=?", @date.beginning_of_month).where("DATE(commission_referrals.commission_date) <=?", @date).sum(key) rescue 0
	end
end