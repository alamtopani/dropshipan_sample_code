module ApplicationHelper
  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", errors: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages(opts = {})
    if flash.present?
      content_tag(:div, class: 'section-alert') do
        flash.each do |msg_type, message|
          concat(content_tag(:div, message, class: "alert-publisher alert #{bootstrap_class_for(msg_type)} alert-dismissible", role: 'alert') do
            concat(content_tag(:button, class: 'close', data: { dismiss: 'alert' }) do
              concat content_tag(:span, '&times;'.html_safe, 'aria-hidden' => true)
              concat content_tag(:span, 'Close', class: 'sr-only')
            end)
            if flash[:errors].present?
              message.each do |word|
                concat "<div class='alert-publisher alert alert-success alert-dismissible margin-bottom5'>#{word}</div>".html_safe
              end
            else
              concat "#{message}".html_safe
            end
          end)
        end
        nil
      end
    end
  end

  def active_sidebar?(controller, *actions)
    if actions.include?(params[:action].to_sym) || actions.include?(:all)
      return 'open'
    end if controller_name == controller.to_s
  end

  def active_sidebar_params?(controller, *actions)
    if actions.include?(params[:action].to_sym) || actions.include?(:all)
      return 'open'
    end if request.fullpath == controller.to_s
  end

  def active_sidebar_active?(controller, *actions)
    if actions.include?(params[:action].to_sym) || actions.include?(:all)
      return 'active'
    end if controller_name == controller.to_s
  end

  def get_datetime(datetime)
    datetime.strftime(" %d %B %Y - %I:%M %p")
  end

  def get_date(datetime)
    datetime.strftime(" %d-%m-%y ")
  end

  def get_datetext(datetime)
     datetime.strftime(" %d %B %Y")
  end

  def get_products
    @get_products = Product.active.order(title: :asc)
  end

  def get_account_managers
    @get_account_managers = AccountManager.where(verified: true).order(username: :asc)
  end

  def get_members
    @get_members = Member.verified.order(username: :asc)
  end

  def get_merchants
    @get_merchants = Merchant.verified.order(username: :asc)
  end

  def get_invoice_members
    @get_invoice_members = Member.verified.where(id: Invoice.paid.pluck(:user_id).uniq).order(username: :asc)
  end

  def get_brands
    @get_brands = Brand.oldest
  end

  def user_edit_url(type, current_user)
    if type == 'Member'
      edit_members_member_path(current_user)
    elsif type == 'Merchant'
      edit_merchant_merchant_path(current_user)
    end
  end

  def get_currency(number)
    number = 0 if number.blank?
    number_to_currency(number, :unit => "Rp ", :separator => ",", :delimiter => ".", precision: 0)
  end

  def get_currency_full(number)
    number = 0 if number.blank?
    number_to_currency(number, :unit => "Rp ", :separator => ",", :delimiter => ".", precision: 4)
  end

  def sort_by
    @sort_by ||=
    [
      ["Produk Terbaru", "latest", data: { url: "#" }],
      ["Produk Terlama", "oldest", data: { url: "#" }],
      ["Produk Pilihan", "featured", data: { url: "#" }],
      ["Harga Terendah s/d Tertinggi", "low_to_high", data: { url: "#" }],
      ["Harga Tertinggi s/d Terendah", "high_to_low", data: { url: "#" }]
    ]
  end

  def get_child_categories(category)
    category = Category.find_by(name: category).children.alfa
  end 

  def get_web_templates
    @get_web_templates = WebTemplate.oldest.active
  end

  def message_status(status)
    if status == "unread"
      return "<label class='label label-success'>New</label>".html_safe
    end
  end

  def message_has_readed_am(message_status)
    if message_status == "read"
      return "<li style='background: none;'>".html_safe
    else
      return "<li>".html_safe
    end
  end

  def show_search_form_date(start_at, end_at)
    if start_at.present? && end_at.blank?
      return "Dari Tanggal #{start_at}"
    elsif start_at.blank? && end_at.present?
      return "Sampai Tanggal #{end_at}"
    elsif start_at.present? && end_at.present?
      return "Dari Tanggal #{start_at} s.d. #{end_at}"
    else
      return nil
    end
  end

  def prepare_shipping_price(shipping_method, district, weight, origin)
    require 'uri'
    require 'net/http'

    url = URI("https://pro.rajaongkir.com/api/cost")
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(url)
    request["key"] = Rails.application.secrets.api_key_raja_ongkir
    request["content-type"] = 'application/x-www-form-urlencoded'
    request.body = "origin=#{origin}&originType=city&destination=#{district.split(':')[0]}&destinationType=subdistrict&weight=#{weight.to_s}&courier=#{shipping_method}"

    response = http.request(request)
    if origin != '0' && district.present? && weight.present? && shipping_method.present?
      if shipping_method == 'jne'
        @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "REG"}[0]

        # if @data.blank?
        #   @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "CTC"}[0]
        # end

        if @data.blank?
          @price = 0
          return @price
        else
          @price = @data["cost"][0]["value"]
          return @price
        end
      elsif shipping_method == 'tiki'
        @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "REG"}[0]

        # if @data.blank?
        #   @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "ECO"}[0]
        # end

        if @data.blank?
          @price = 0
          return @price
        else
          @price = @data["cost"][0]["value"]
          return @price
        end
      elsif shipping_method == 'jnt'
        @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "EZ"}[0]

        if @data.blank?
          @price = 0
          return @price
        else
          @price = @data["cost"][0]["value"]
          return @price
        end
      elsif shipping_method == 'pos'
        @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "Paketpos Biasa"}[0]

        if @data.blank?
          @data = JSON.parse(response.body)["rajaongkir"]["results"][0]["costs"].select{|key| key["service"] == "Surat Kilat Khusus"}[0]
        end
        
        if @data.blank?
          @price = 0
          return @price
        else
          @price = @data["cost"][0]["value"]
          return @price
        end
      end
    else
      @price = 0
      return @price
    end
  end

  def time_to_ago(time)
    a = (Time.zone.now - time).to_i

    case a
      when 0 then 'just now'
      when 1 then 'a second ago'
      when 2..59 then a.to_s+' seconds ago' 
      when 60..119 then 'a minute ago' #120 = 2 minutes
      when 120..3540 then (a/60).to_i.to_s+' minutes ago'
      when 3541..7100 then 'an hour ago' # 3600 = 1 hour
      when 7101..82800 then ((a+99)/3600).to_i.to_s+' hours ago' 
      when 82801..172000 then 'a day ago' # 86400 = 1 day
      when 172001..518400 then ((a+800)/(60*60*24)).to_i.to_s+' days ago'
      when 518400..1036800 then 'a week ago'
      else ((a+180000)/(60*60*24*7)).to_i.to_s+' weeks ago'
    end
  end
end
