class UserMailer < ActionMailer::Base
  include ActionView::Helpers::NumberHelper
  helper :application

  # default from: 'dropshipanindonesia@gmail.com'
  # default to: 'dropshipanindonesia@gmail.com'

  default from: 'hi@dropshipan.com'
  default to: 'hi@dropshipan.com'

  # USER MAILER
  def welcome_join(user)
    @setting = WebSetting.first
    @user = user
    mail(to: user.email, subject: 'Selamat bergabung di Dropshipan Indonesia')
  end

  def send_user_approve_to_verified(user)
    @setting = WebSetting.first
    @user = user
    mail(to: user.email, subject: 'Verifikasi Disetujui - Akun Reseller anda sudah aktif dan disetuji')
  end

  # INVOICE
  def send_invoice_package(user)
    @setting = WebSetting.first
    @user = user
    @invoice = @user.invoices.last
    mail(to: user.email, bcc: 'payment@dropshipan.com', subject: 'Informasi Tagihan Paket')
  end

  def send_invoice_package_paid(invoice)
    @setting = WebSetting.first
    @invoice = invoice
    @user = @invoice.user
    mail(to: @user.email, bcc: 'payment@dropshipan.com', subject: 'Informasi Pembayaran Paket')
  end

  def send_invoice_package_reminder(user)
    @setting = WebSetting.first
    @user = user
    @invoice = @user.invoices.last
    mail(to: user.email, bcc: 'payment@dropshipan.com', subject: 'Perpanjangan Tagihan Paket')
  end

  # ORDER
  def order_checkout(order)
    @setting = WebSetting.first
    @order = order
    mail(to: @order.payer_email, bcc: 'payment@dropshipan.com', subject: "Order Product #{@order.code}")
  end

  def order_shipped(order)
    @setting = WebSetting.first
    @order = order
    mail(to: order.payer_email, bcc: 'payment@dropshipan.com', subject: "Pesanan #{@order.code} - telah dikirim")
  end

  def order_paid(order)
    @setting = WebSetting.first
    @order = order
    mail(to: @order.stakeholder_orders.last.merchant.email, bcc: 'payment@dropshipan.com', subject: "Order Product #{@order.code}")
  end

  def order_paid_reseller(order)
    @setting = WebSetting.first
    @order = order
    mail(to: @order.member.email, bcc: 'payment@dropshipan.com', subject: "Order Product #{@order.code}")
  end

  # CONFIRMATION
  def send_confirmation(confirmation)
    @setting = WebSetting.first
    @confirmation = confirmation
    mail(from: @confirmation.email, bcc: 'payment@dropshipan.com', subject: "Confirmation order - #{@confirmation.code}")
  end

  # GUEST BOOK
  def send_guest_book(guest_book)
    @setting = WebSetting.first
    @guest_book = guest_book
    mail(from: @guest_book.email, to: @guest_book.user.email, subject: 'Notification new Guest book from user')
  end

  def send_guest_book_official(guest_book)
    @setting = WebSetting.first
    @guest_book = guest_book
    mail(from: @guest_book.email, subject: 'Notification new Guest book from user')
  end

  # REPORT REFUND
  def send_report_refund_paid(report_refund)
    @report_refund = report_refund
    @order = @report_refund.order
    mail(to: @order.payer_email, subject: "Invoice Order Refund #{@order.code}")
  end

  # PAYMENT MERCHANT
  def send_payment_merchant_paid(payment_merchant)
    @payment_merchant = payment_merchant
    mail(to: @payment_merchant.merchant.email, subject: "Invoice Payment Merchant #{@payment_merchant.code}")
  end

  # PAYMENT SNAPSHOT
  def send_payment_snapshot_paid(payment_snapshot)
    @payment_snapshot = payment_snapshot
    mail(to: @payment_snapshot.user.email, subject: "Invoice Payment Snapshot #{@payment_snapshot.code}")
  end

  # PAYMENT REFERRAL
  def send_payment_referral_paid(payment_referral)
    @payment_referral = payment_referral
    mail(to: @payment_referral.member.email, subject: "Invoice Payment Referral #{@payment_referral.code}")
  end

  # PAYMENT AFFILIATE
  def send_payment_affiliate_paid(payment_affiliate)
    @payment_affiliate = payment_affiliate
    mail(to: @payment_affiliate.member.email, subject: "Invoice Payment Affiliate #{@payment_affiliate.code}")
  end

  # PRODUCT MAILER
  # def send_request_product(product)
  #   @setting = WebSetting.first
  #   @product = product
  #   mail(from: product.merchant.email, subject: "Request New Product to Publish - #{product.code}")
  # end

  # def send_product_verified(product)
  #   @setting = WebSetting.first
  #   @product = product
  #   mail(to: product.merchant.email, subject: "Product Already Publish - #{product.code}")
  # end

  # def send_product_not_verified(product)
  #   @setting = WebSetting.first
  #   @product = product
  #   mail(to: product.merchant.email, subject: "Product Already Unpublish - #{product.code}")
  # end

  # def send_product_featured(product)
  #   @setting = WebSetting.first
  #   @product = product
  #   mail(to: product.merchant.email, subject: "Set Product to Featured  - #{product.code}")
  # end

  # def send_comment(comment, url)
  #   @setting = WebSetting.first
  #   @user = comment.user
  #   @comment = comment
  #   mail(from: @user.email,subject: "Comment Product")
  # end

end
