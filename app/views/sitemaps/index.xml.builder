xml.instruct!

xml.urlset(xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9") do
  xml.url do
    xml.loc root_url
    xml.changefreq("hourly")
    xml.priority "1.0"
  end
  @catalogs.each do |resource|  
    xml.url do
      xml.loc product_url(resource.product.slug+'-'+resource.id.to_s)
      xml.changefreq("daily")
      xml.priority "0.8"
      xml.lastmod resource.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%2N%:z")
    end
  end
end