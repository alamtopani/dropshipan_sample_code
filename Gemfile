source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.1.4'
gem 'sqlite3'
gem 'puma', '~> 3.7'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'jquery-rails', '4.3.1'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'

gem 'pg'
gem 'ancestry'                        # Table Tree tool
gem 'inherited_resources'             # DRY for CRUD methods in controller
gem 'kaminari'                        # pagination
gem 'breadcrumbs_on_rails'
gem 'acts_as_list'
gem 'friendly_id'
gem 'impressionist'
gem 'enumerate_it'
gem 'cocoon'                          # Dynamic nested forms using jQuery
gem 'htmlentities'
gem "recaptcha", :require => "recaptcha/rails"
gem 'exception_notification', '~> 4.0.1'
gem 'figaro'
gem 'roo'
gem 'roo-xls'
gem 'autonumeric-rails'
gem 'dragonfly'
gem 'htmlcompressor'

#A PDF generation plugin for Ruby on Rails
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'
gem 'country_select'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'devise'
gem 'paperclip', :git=> 'https://github.com/thoughtbot/paperclip', :ref => '523bd46c768226893f23889079a7aa9c73b57d68'
gem 'delayed_paperclip'
gem 'paperclip-watermark'
gem 'aws-sdk', '~> 2.6'
gem 'mini_magick'
gem 'ckeditor'
gem 'rails-timeago', '~> 2.0'
gem 'faker'
gem "select2-rails"
gem 'acts_as_votable', '~> 0.10.0'
gem 'rails4-autocomplete'
gem 'searchkick'
gem 'ratyrate'
gem 'shareable'
gem 'paypal-sdk-merchant'
gem "cancan"
gem "lazyload-rails"
gem 'bootstrap-sass', '~> 3.3.5'
gem 'slim-rails'
gem 'font-awesome-sass'
gem 'momentjs-rails', '>= 2.9.0'
gem 'bootstrap3-datetimepicker-rails', '~> 4.15.35'
gem 'google-api-client'
gem 'ratyrate'
gem 'ahoy_matey'
gem 'groupdate'
gem 'chartkick'
gem "paranoia", "~> 2.0"
# gem 'activerecord-session_store', github: 'rails/activerecord-session_store'
gem 'letter_opener'
gem "haml-rails"
gem "instagram", :git => 'git://github.com/Instagram/instagram-ruby-gem.git'

gem 'whenever', :require => false
gem 'jquery-infinite-pages'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
end

group :development do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'

  gem 'mina'
  gem 'mina-puma', require: false
  gem 'mina-sidekiq'
  gem 'mina-whenever'
  gem 'rubocop', require: false
  gem 'brakeman', require: false

  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'spring-commands-rspec'
  gem 'spring-commands-spinach'
  gem 'spring-commands-rubocop'
  gem 'rack-mini-profiler', require: false
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
