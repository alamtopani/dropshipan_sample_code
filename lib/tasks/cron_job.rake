namespace :cron_job do
  desc "TODO"
  task check_job_merchant: :environment do
    payment = WebSetting.first.payments.first

    date_zone = Time.zone.now.to_date
    last_month = (date_zone.beginning_of_month - 1.month)
    beginning_of_month = date_zone.beginning_of_month
    half_end_of_month = date_zone.beginning_of_month + 14.days
    beginning_half_of_month = half_end_of_month + 1.days

    payment_period_one = beginning_of_month + 3.days
    payment_period_two = beginning_half_of_month + 3.days
    
    if payment_period_one == date_zone
      payment_start = last_month.beginning_of_month + 15.days
      payment_end = last_month.end_of_month
    elsif payment_period_two == date_zone
      payment_start = beginning_of_month
      payment_end = half_end_of_month
    end

    if payment_period_one == date_zone || payment_period_two == date_zone
      payment_merchant = StakeholderOrder.eager_load(:order).where("DATE(orders.purchased_at) >=? AND DATE(orders.purchased_at) <=? AND stakeholder_orders.track_order =?", payment_start, payment_end, OrderItem::SHIPPED)

      Merchant.verified.beweekly.where(id: payment_merchant.pluck(:merchant_id).uniq).each do |resource|
        last_payment = resource.payment_merchants.last
        if last_payment.present?
          unless last_payment.payment_start == payment_start && last_payment.payment_end == payment_end
            PaymentMerchant.create_payment_merchant(resource, payment_start, payment_end, payment, payment_merchant)
          end
        else
          PaymentMerchant.create_payment_merchant(resource, payment_start, payment_end, payment, payment_merchant)
        end
      end
    end
  end

  task check_job_member: :environment do
    payment = WebSetting.first.payments.first

    date_zone = Time.zone.now.to_date
    last_month = (date_zone.beginning_of_month - 1.month)
    beginning_of_month = date_zone.beginning_of_month
    half_end_of_month = date_zone.beginning_of_month + 14.days
    beginning_half_of_month = half_end_of_month + 1.days

    payment_period_one = beginning_of_month + 3.days
    payment_period_two = beginning_half_of_month + 3.days
    
    if payment_period_one == date_zone
      payment_start = last_month.beginning_of_month + 15.days
      payment_end = last_month.end_of_month
    elsif payment_period_two == date_zone
      payment_start = beginning_of_month
      payment_end = half_end_of_month
    end

    if payment_period_one == date_zone || payment_period_two == date_zone
      commissions_payment = Commission.where("DATE(commissions.commission_date) >=? AND DATE(commissions.commission_date) <=? AND commissions.status =?", payment_start, payment_end, Commission::APPROVED)
      commission_referrals = CommissionReferral.where("DATE(commission_referrals.commission_date) >=? AND DATE(commission_referrals.commission_date) <=? AND commission_referrals.status =?", payment_start, payment_end, CommissionReferral::APPROVED)

      Member.verified.beweekly.where(id: commissions_payment.pluck(:member_id).uniq).each do |resource|
        last_payment = resource.payment_snapshots.last
        if last_payment.present?
          unless last_payment.payment_start == payment_start && last_payment.payment_end == payment_end
            PaymentSnapshot.create_payment_member(resource, payment_start, payment_end, payment, commissions_payment)
          end
        else
          PaymentSnapshot.create_payment_member(resource, payment_start, payment_end, payment, commissions_payment)
        end
      end

      Member.verified.where(id: commission_referrals.pluck(:member_id).uniq).each do |resource|
        last_payment = resource.payment_referrals.last
        if last_payment.present?
          unless last_payment.payment_start == payment_start && last_payment.payment_end == payment_end
            PaymentReferral.create_payment_referral(resource, payment_start, payment_end, payment, commission_referrals)
          end
        else
          PaymentReferral.create_payment_referral(resource, payment_start, payment_end, payment, commission_referrals)
        end
      end
    end
  end

  task check_job_am: :environment do
    payment = WebSetting.first.payments.first

    date_zone = Time.zone.now.to_date
    last_month = (date_zone.beginning_of_month - 1.month)
    beginning_of_month = date_zone.beginning_of_month
    half_end_of_month = date_zone.beginning_of_month + 14.days
    beginning_half_of_month = half_end_of_month + 1.days

    payment_period_one = beginning_of_month + 3.days
    payment_period_two = beginning_half_of_month + 3.days
    
    if payment_period_one == date_zone
      payment_start = last_month.beginning_of_month + 15.days
      payment_end = last_month.end_of_month
    elsif payment_period_two == date_zone
      payment_start = beginning_of_month
      payment_end = half_end_of_month
    end

    if payment_period_one == date_zone || payment_period_two == date_zone
      commissions_payment = Commission.where("DATE(commissions.commission_date) >=? AND DATE(commissions.commission_date) <=? AND commissions.status =?", payment_start, payment_end, Commission::APPROVED)
      
      AccountManager.verified.where(id: commissions_payment.pluck(:account_manager_id).uniq).each do |resource|
        last_payment = resource.payment_snapshots.last
        if last_payment.present?
          unless last_payment.payment_start == payment_start && last_payment.payment_end == payment_end
            PaymentSnapshot.create_payment_account_manager(resource, payment_start, payment_end, payment, commissions_payment)
          end
        else
            PaymentSnapshot.create_payment_account_manager(resource, payment_start, payment_end, payment, commissions_payment)
        end
      end
    end
  end

  task auto_cancellation_orders: :environment do
    Order.verification.where("orders.due_date =?", Time.zone.now.to_date).each do |resource|
      old_status = resource.order_status_id
      old_status_shipping = resource.track_order
      resource.order_status_id = 4
      Order.update_order(resource, old_status, old_status_shipping)
      User.prepare_activity(resource, 1, "Status Pesanan (#{resource.status_text?})", "Sistem secara otomatis mengubah status pesanan dari '#{Order.check_status_text(old_status)}' menjadi '#{resource.status_text?}'.") unless old_status == resource.order_status_id
    end
  end

  task create_invoice_package: :environment do
    date_today = Time.zone.now.to_date
    payment = WebSetting.first.payments.first

    Invoice.paid.where("period_end >=?", date_today).where("period_end <=?", date_today+5.days).each do |resource|
      last_invoice = resource.member.invoices.last
      if last_invoice.paid? && last_invoice.period_end >= date_today && last_invoice.period_end <= date_today+5.days
        invoice = Invoice.new
        invoice.user_id = resource.user_id
        invoice.subject = "Invoice Perpanjangan Paket #{resource.package.title}"
        invoice.description = "Invoice Perpanjangan Langganan Paket #{resource.package.title}"
        invoice.price = resource.package.price
        invoice.bank_name = payment.bank_name
        invoice.bank_branch = "Sukabumi"
        invoice.account_name = payment.name
        invoice.account_number = payment.account_number
        invoice.invoiceable_id = resource.package.id
        invoice.invoiceable_type = resource.package.class.name
        invoice.already_send = true
        if invoice.save
          UserMailer.send_invoice_package_reminder(invoice.member).deliver
          User.prepare_activity(resource, 1, "Tagihan Perpanjangan Paket", "Sistem secara otomatis membuat tagihan perpanjangan paket #{resource.package.title}.")
        end
      end
    end
  end
end