module Permitable
	USER = [
			:id,
			:type,
			:username,
			:email,
			:password,
			:password_confirmation,
			:provider,
			:uid,
			:oauth_token,
			:oauth_expires_at,
			:slug,
			:featured,
			:verified,
			:package_id,
			:account_manager_id,
			:ref_id1,
			:ref_id2,
			:ref_id3,
			:user_fee,
			:role_access,
			:status_payment
		]

	ADDRESS = {
			address_attributes: [
				:id,
				:address,
				:district,
				:city,
				:province,
				:country,
				:postcode,
				:addressable_type,
				:addressable_id,
				:addressable
			]
	}

	PROFILE = {
			profile_attributes: [
				:id,
				:user_id,
				:full_name,
				:birthday,
				:gender,
				:phone,
				:avatar,
				:cover,
				:meta_title,
				:meta_keywords,
				:meta_description,
				:bank_name,
				:bank_branch,
				:account_name,
				:account_number,
				:no_ktp,
				:no_npwp,
			]
	}

	CONTACT = {
		contact_attributes: [
			:id,
			:phone,
			:handphone,
			:bbm,
			:line,
			:wa,
			:facebook,
			:instagram,
			:contactable_type,
			:contactable_id,
			:contactable,
			:website,
			:link_wa,
			:use_link_wa
		]
	}

	CATEGORY = [
		:id,
		:name,
		:ancestry,
		:parent_id,
		:file,
		:link_download,
		:description,
		:status
	]

	FAQ = {
		faq_attributes: [
			:id,
			:payment,
			:shipping,
			:refund_and_exchange,
			:faq,
			:faqable_id,
			:faqable_type,
			:faqable,
			:_destroy
		]
	}

	GUEST_BOOK = [
		:id,
		:name,
		:email,
		:phone,
		:description,
		:status,
		:user_id
	]

	MERCHANT_INFO = {
		merchant_info_attributes: [
			:id,
			:merchant_id,
			:title,
			:banner,
			:description,
			:policies,
			:payment,
			:shipping,
			:refund_and_exchange,
			:faq,
			:product_type,
			:no_bpom,
			:distribution_stock,
			:sample_product,
			:sample_product_image,
			:available_cod
		]
	}

	SITE_INFO = {
		site_info_attributes: [
			:id,
			:member_id,
			:package_id,
			:buy_domain,
			:domain_name,
			:buy_logo,
			:logo_title,
			:discount_global,
			:keywords_1,
			:keywords_2,
			:keywords_3,
			:web_template_id
		]
	}

	PAYMENT = {
		payments_attributes: [
			:id,
			:name,
			:logo,
			:bank_name,
			:account_number,
			:paymentable_id,
			:paymentable_type,
			:paymentable,
			:position,
			:_destroy
		]
	}

	CATALOG = [
			:id,
			:product_id,
			:member_id,
			:price,
			:discount,
			:deleted_at,
			:description,
			:meta_title,
			:meta_keywords,
			:meta_description,
			:status,
			:message
	]

	PRODUCT = [
			:id,
			:code,
			:slug,
			:merchant_id,
			:category_id,
			:sub_category_id,
			:brand_name,
			:title,
			:price,
			:basic_price,
			:recommended_price,
			:description,
			:status,
			:featured,
			:start_at,
			:end_at,
			:weight,
			:status_stock,
			:cover,
			:link_video,
			:no_referral,
			:deleted_at
	]

	PRODUCT_SPEC = {
			product_spec_attributes: [
				:id,
				:product_id,
				:brand_id,
				:information,
				:material,
				:stock,
				:deleted_at
			]
	}

	PRODUCT_STOCK = {
			product_stocks_attributes: [
				:id,
				:product_id,
				:variant,
				:stock,
				:_destroy
			]
	}

	PRODUCT_TESTIMONIAL = {
			product_testimonials_attributes: [
				:id,
				:product_id,
				:image,
				:_destroy
			]
	}

	GALLERY = {
			galleries_attributes: [
				:id,
				:title,
				:description,
				:file,
				:galleriable_type,
				:galleriable_id,
				:galleriable,
				:position,
				:featured,
				:_destroy,
				:deleted_at
			]
	}

	REGION = [
		:id,
		:name,
		:ancestry,
		:featured,
		:parent_id,
		:image
	]

	COMMENT = [
			:id,
			:user_id,
			:commentable_type,
			:commentable_id,
			:commentable,
			:comment,
			:status
	]

	STAKEHOLDER_ORDER = [
			:id,
			:no_invoice,
			:order_id,
			:merchant_id,
			:member_id,
			:weight,
			:price,
			:basic_price,
			:company_price,
			:shipping_price,
			:total_price,
			:total_basic_price,
			:total_company_price,
			:payment_status,
			:track_order
	]

	ORDER_ITEM = [
			:id,
			:no_invoice,
			:catalog_id,
			:product_id,
			:order_id,
			:member_id,
			:merchant_id,
			:stakeholder_order_id,
			:price,
			:basic_price,
			:company_price,
			:quantity,
			:total_price,
			:total_basic_price,
			:total_company_price,
			:weight,
			:discount,
			:variant,
			:message,
			:status,
			:cancelled_reason
	]

	ORDER = [
			:id,
			:member_id,
			:slug,
			:code,
			:payment_token,
			:payer_name,
			:payer_email,
			:payer_address,
			:payer_phone,
			:payment_method,
			:shipping_method,
			:payment_to,
			:price,
			:basic_price,
			:company_price,
			:shipping_price,
			:total_price,
			:total_basic_price,
			:total_company_price,
			:purchased_at,
			:order_status_id,
			:track_order,
			:number_shipping,
			:origin_code,
			:origin_name,
			:courier
	]

	SHIPMENT = [
			:id,
			:name,
			:province,
			:city,
			:state,
			:postal_code,
			:address,
			:weight,
			:price,
			:shipmentable_type,
			:shipmentable_id,
			:shipmentable
	]

	CONFIRMATION = [
			:id,
			:slug,
			:member_id,
			:no_invoice,
			:name,
			:email,
			:payment_date,
			:nominal,
			:bank_account,
			:payment_method,
			:sender_name,
			:from_bank_name,
			:from_account_name,
			:from_account_number,
			:message,
			:file
	]

	LANDING_PAGE = [
			:title,
			:slug,
			:description,
			:status,
			:category
	]

	SUBSCRIBE = [
			:email,
			:status
	]

	TESTIMONIAL = [
			:user_id,
			:message,
			:status
	]

	WEB_SETTING = [
			:id,
			:user_id,
			:header_tags,
			:footer_tags,
			:contact,
			:email,
			:favicon,
			:logo,
			:logo_banner,
			:facebook,
			:twitter,
			:title,
			:keywords,
			:description,
			:robot,
			:author,
			:corpyright,
			:revisit,
			:expires,
			:revisit_after,
			:geo_placename,
			:language,
			:country,
			:content_language,
			:distribution,
			:generator,
			:rating,
			:target,
			:search_engines,
			:address,
			:longitude,
			:latitude,
			:fb_fixels_dashboard,
			:fb_fixels_view,
			:fb_fixels_cart,
			:fb_fixels_checkout,
			:fb_fixels_purchase,
			:google_analytic,
			:accepted_rules
	]

	WEB_SETTING_ATTIBUTTES = [
		web_setting_attributes: [
			:id,
			:user_id,
			:header_tags,
			:footer_tags,
			:contact,
			:email,
			:favicon,
			:logo,
			:logo_banner,
			:facebook,
			:instagram,
			:title,
			:keywords,
			:description,
			:robot,
			:author,
			:corpyright,
			:revisit,
			:expires,
			:revisit_after,
			:geo_placename,
			:language,
			:country,
			:content_language,
			:distribution,
			:generator,
			:rating,
			:target,
			:search_engines,
			:address,
			:longitude,
			:latitude,
			:fb_fixels_dashboard,
			:fb_fixels_view,
			:fb_fixels_cart,
			:fb_fixels_checkout,
			:fb_fixels_purchase,
			:google_analytic,
			:order_via_cart,
			:order_via_wa,
			galleries_attributes: [
				:id,
				:title,
				:description,
				:file,
				:galleriable_type,
				:galleriable_id,
				:galleriable,
				:position,
				:featured,
				:_destroy,
				:deleted_at
			]
		]
	]

	SHIPPING_METHOD = {
		shipping_methods_attributes: [
			:id,
			:name,
			:logo,
			:status,
			:shipping_methodable_type,
			:shipping_methodable_id,
			:shipping_methodable,
			:position,
			:_destroy
		]
	}

	PACKAGE = [
			:id,
			:title,
			:price,
			:description,
			:status,
			:image,
			:package_type,
			:period
	]

	INVOICE = [
			:id,
			:user_id,
			:subject,
			:description,
			:price,
			:uniq_code,
			:bank_name,
			:bank_branch,
			:account_name,
			:account_number,
			:period_start,
			:period_end,
			:status,
			:invoiceable_type,
			:invoiceable_id,
			:promo,
			:already_send
	]

	COMMISSION = [
			:id,
			:catalog_id,
			:product_id,
			:member_id,
			:account_manager_id,
			:order_id,
			:order_item_id,
			:commission_member,
			:commission_company,
			:commission_am,
			:status,
			:uniq_code,
			:quantity,
			:commission_date
	]

	PAYMENT_SNAPSHOT = [
			:id,
			:code,
			:admin_id,
			:user_type,
			:user_id,
			:payment_start,
			:payment_end,
			:fee_am,
			:fee_member,
			:fee_referral,
			:fee_company,
			:total,
			:price,
			:category,
			:message,
			:from_acc,
			:to_acc,
			:status
	]

	DOMAIN_SETTING =  {
		domain_settings_attributes: [
			:id,
			:member_id,
			:name,
			:ns1,
			:ns2,
			:message,
			:status,
			:_destroy
		]
	}

	DOMAIN_SETTING_BACKEND = [
			:id,
			:member_id,
			:name,
			:ns1,
			:ns2,
			:message,
			:status
	]

	WEB_TEMPLATE = [
		:id,
		:name,
		:image,
		:code_css,
		:status
	]

	REPORT_REFUND = [
		:id,
		:order_id,
		:order_item_id,
		:refund_value,
		:ongkir,
		:total,
		:status
	]

	LANDING_PRESS = [
		:banner,
		:title1,
		:description1,
		:image1,
		:image2,
		:image3,
		:title2,
		:description2,
		:image4,
		:image5,
		:title3,
		:description3,
		:title_banner,
		:title4,
		:title_banner2,
		:description4,
		:image6,
		:title5,
		:description5,
		:price,
		:telephone,
		:footer,
		:code,
		:category,
		:link_video,
		:screen_capture,
		:status,
		:code_css,
		:brand,
		:product_id
  ]

	LANDING_PRESS_GALLERY = {
		landing_press_galleries_attributes: [
			:id,
			:title,
			:position,
			:image,
			:_destroy
		]
	}

	LANDING_PRESS_PRODUCT = {
		landing_press_products_attributes: [
			:id,
			:title,
			:position,
			:image,
			:_destroy
		]
	}

	LANDING_PRESS_TESTIMONIAL = {
		landing_press_testimonials_attributes: [
			:id,
			:title,
			:position,
			:image,
			:_destroy
		]
	}

	REQUEST_LANDING_PRESS = [
		:id,
		:code,
		:member_id,
		:landing_press_id,
		:price,
		:contact,
		:meta_title,
		:meta_keywords,
		:link_wa,
		:use_link_wa
	]

	BRAND = [
		:id,
		:name,
		:logo
	]

	PAYMENT_MERCHANT = [
		:code,
		:merchant_id,
		:payment_start,
		:payment_end,
		:price,
		:ongkir,
		:total,
		:from_acc,
		:to_acc,
		:status,
		:message
	]

	PAYMENT_REFERRAL = [
		:id,
		:code,
		:admin_id,
		:member_id,
		:payment_start,
		:payment_end,
		:commission_referral,
		:message,
		:from_acc,
		:to_acc,
		:status,
		:payment_pending,
		:quote,
		:total_delay
	]

	SPENDING = [
		:admin_id,
		:code,
		:title,
		:description,
		:price,
		:transaction_date,
		:from_acc, 
		:to_acc, 
		:file,
		:status,  
		:category 
	]

	BLOG = [
		:slug,
		:user_id,
		:title,
		:description,
		:category,
		:link_video,
		:file,
		:status,
		:view_count
	]

	def self.controller(name)
		self.send name.gsub(/\W/,'_').singularize.downcase
	end

	# -----------FOR BACKEND------------

	def self.backend_user
		USER.dup.push(PROFILE.dup).push(ADDRESS.dup).push(CONTACT.dup)
	end

	def self.backend_admin
		backend_user
	end

	def self.backend_member
		backend_user.push(SITE_INFO.dup).push(DOMAIN_SETTING.dup).push(WEB_SETTING_ATTIBUTTES.dup)
	end

	def self.backend_account_manager
		backend_user
	end

	def self.backend_merchant
		backend_user.push(MERCHANT_INFO.dup)
	end

	def self.backend_category
		CATEGORY
	end

	def self.backend_catalog
		CATALOG
	end

	def self.backend_product
		PRODUCT.dup.push(GALLERY.dup).push(PRODUCT_SPEC.dup).push(PRODUCT_STOCK.dup).push(PRODUCT_TESTIMONIAL.dup)
	end

	def self.backend_order
		ORDER.push(ADDRESS.dup)
	end

	def self.backend_order_item
		ORDER_ITEM
	end

	def self.backend_confirmation
		CONFIRMATION
	end

	def self.backend_landing_page
		LANDING_PAGE
	end

	def self.backend_subscribe
		SUBSCRIBE
	end

	def self.backend_testimonial
		TESTIMONIAL
	end

	def self.backend_package
		PACKAGE
	end

	def self.backend_invoice
		INVOICE
	end

	def self.backend_commission
		COMMISSION
	end

	def self.backend_payment_snapshot
		PAYMENT_SNAPSHOT
	end

	def self.backend_domain_setting
		DOMAIN_SETTING_BACKEND
	end

	def self.backend_web_setting
		WEB_SETTING.dup.push(GALLERY.dup).push(FAQ.dup).push(PAYMENT.dup).push(SHIPPING_METHOD.dup)
	end

	def self.backend_web_template
		WEB_TEMPLATE
	end

	def self.backend_landing_press
		LANDING_PRESS.dup.push(LANDING_PRESS_GALLERY.dup).push(LANDING_PRESS_PRODUCT.dup).push(LANDING_PRESS_TESTIMONIAL.dup)
	end

	def self.backend_report_refund
		REPORT_REFUND
	end

	def self.backend_brand
		BRAND
	end

	def self.backend_payment_merchant
		PAYMENT_MERCHANT
	end

	def self.backend_payment_referral
		PAYMENT_REFERRAL
	end

	def self.backend_spending
		SPENDING
	end

	def self.backend_blog
		BLOG
	end

	# -----------FOR MEMBER------------

	def self.members_member
		backend_member
	end

	def self.members_catalog
		backend_catalog
	end

	def self.members_request_landing_press
		REQUEST_LANDING_PRESS
	end

	# -----------FOR MANAGER------------
	
	def self.managers_account_manager
		backend_account_manager
	end

	# -----------FOR MERCHANT------------
	
	def self.merchants_merchant
		backend_merchant
	end

	def self.merchants_product
		backend_product
	end

	def self.merchants_order_item
		ORDER_ITEM
	end

	def self.merchants_order
		ORDER.push(ADDRESS.dup)
	end

	# -----------FOR FRONTEND------------

	def self.comment
		COMMENT
	end

	def self.order
		backend_order
	end

	def self.order_item
		ORDER_ITEM
	end

	def self.confirmation_invoice
		CONFIRMATION
	end

	def self.subscribe
		SUBSCRIBE
	end

	def self.guest_book
		GUEST_BOOK
	end

end
