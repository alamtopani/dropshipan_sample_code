Rails.application.routes.draw do
  devise_for :users,
    controllers: {
      omniauth_callbacks: 'omniauth_callbacks',
      registrations: 'registrations',
      sessions: 'sessions',
      confirmations: "confirmations"
    },
    path_names: {
      sign_in:  'login',
      sign_out: 'logout',
      sign_up:  'register'
    }

  root 'publics#home'
  get 'contact', to: 'official#contact', as: 'contact'
  post 'create_contact', to: 'official#create_contact', as: 'create_contact'
  get 'home', to: 'official#home', as: 'home'
  get 'faq', to: 'official#faq', as: 'faq'
  get 'policy', to: 'official#policy', as: 'policy'
  get 'aboutus', to: 'official#aboutus', as: 'aboutus'
  get 'faq2', to: 'official#faq2', as: 'faq2'
  get 'catalog', to: 'official#catalog', as: 'catalog'
  get 'under-maintenance', to: 'publics#under_maintenance', as: 'under_maintenance'
  get '404-not-found', to: 'publics#not_found_page', as: 'not_found_page'
  get "sitemap.xml" => "sitemaps#index", format: "xml", as: 'sitemap'

  resources :blogs
  resources :thank_pages
  resources :pages
  resources :subscribes
  resources :landing_pages
  resources :guest_books
  resources :comments
  resources :products
  resources :helps
  resources :about_us
  resources :contacts
  resources :orders do
    collection do
      get :checkout
    end
    member do
      get :review
    end
  end
  resources :order_items, only: [:create, :update, :destroy]
  resources :confirmation_invoices, only: [:new, :create]
  resources :xhrs do
    collection do
      get :cities
      get :shipment_provinces
      get :shipment_cities
      get :shipment_district
      get :shipping_price
      get :sub_categories
      get :sub_categories_catalog
      get :get_invoice_member
      get :get_payment
      get :get_payment_merchant
      get :get_payment_referral
    end
  end

  namespace :backend do
    get 'dashboard', to: 'home#dashboard', as: 'dashboard'
    resources :admins
    resources :recap_payments
    resources :members do
      collection do
        post :do_multiple_act
      end
    end
    resources :account_managers do
      collection do
        post :do_multiple_act
      end
    end
    resources :merchants do
      collection do
        post :do_multiple_act
      end
    end
    resources :categories do
      collection do
        post :do_multiple_act
      end
    end
    resources :products do
      collection do
        post :do_multiple_act
      end
    end
    resources :orders do
      collection do
        post :do_multiple_act
      end
      member do
        put :change_number_shipping
      end
    end
    resources :order_distributors do
      collection do
        post :do_multiple_act
      end
    end
    resources :order_items do
      collection do
        post :do_multiple_act
      end
    end
    resources :confirmations do
      collection do
        post :do_multiple_act
      end
    end
    resources :affiliates do
      collection do
        post :do_multiple_act
      end
    end
    resources :shipments do
      collection do
        post :do_multiple_act
        post :import
      end
    end
    resources :regions do
      collection do
        post :do_multiple_act
      end
    end
    resources :comments do
      collection do
        post :do_multiple_act
      end
    end
    resources :landing_pages do
      collection do
        post :do_multiple_act
      end
    end
    resources :subscribes do
      collection do
        post :do_multiple_act
      end
    end
    resources :testimonials do
      collection do
        post :do_multiple_act
      end
    end
    resources :web_settings
    resources :catalogs do
      collection do
        post :do_multiple_act
        get :pending
      end
      member do
        get :restore
      end
    end
    resources :packages do
      collection do
        post :do_multiple_act
      end
    end
    resources :invoices do
      collection do
        post :do_multiple_act
      end
    end
    resources :commissions do
      collection do
        post :do_multiple_act
      end
    end
    resources :commission_referrals do
      collection do
        post :do_multiple_act
      end
    end
    resources :payment_snapshots do
      collection do
        post :do_multiple_act
      end
    end
    resources :payment_merchants do
      collection do
        post :do_multiple_act
      end
    end
    resources :payment_referrals do
      collection do
        post :do_multiple_act
      end
    end
    resources :domain_settings do
      collection do
        post :do_multiple_act
      end
    end
    resources :guest_books
    resources :web_templates do
      collection do
        post :do_multiple_act
      end
    end
    resources :report_refunds do
      collection do
        post :do_multiple_act
      end
    end
    resources :landing_presses do
      collection do
        post :do_multiple_act
      end
    end
    resources :request_landing_presses
    resources :brands
    resources :blogs do
      collection do
        post :do_multiple_act
      end
    end
    resources :spendings do
      collection do
        post :do_multiple_act
      end
    end

  end

  namespace :members do
    get 'dashboard', to: 'home#dashboard', as: 'dashboard'
    resources :members, only: [:edit, :update, :show]
    get 'change_theme', to: 'members#change_theme', as: 'change_theme'
    put 'update_change_theme/:id', to: 'members#update_change_theme', as: 'update_change_theme'
    resources :catalogs do
      collection do
        get :inactive
        get :pending
        get :archive
        get :download_catalogs
      end
      member do
        get :restore
        put :change_status
      end
    end
    resources :orders do
      member do
        get :detail
      end
    end
    resources :confirmations
    resources :commissions do
      collection do
        get :referral
      end
    end
    resources :payments
    resources :packages do
      collection do
        get :upgrade
      end
    end
    resources :referrals
    resources :settings
    resources :invoices
    resources :affiliates
    resources :managers
    resources :messages
    resources :guest_books
    resources :blogs
    resources :request_landing_presses
    resources :shops do
      member do
        get :review
      end
      collection do
        get :cart
        get :checkout
      end
    end
    resources :report_refunds
    resources :payment_referrals
  end

  namespace :managers do
    get 'dashboard', to: 'home#dashboard', as: 'dashboard'
    resources :account_managers, only: [:edit, :update, :show]
    resources :catalogs do
      collection do
        get :inactive
        get :pending
        get :archive
        get :download_catalogs
      end
      member do
        get :restore
        put :change_status
      end
    end
    resources :orders do
      member do
        get :detail
      end
    end
    resources :confirmations
    resources :commissions
    resources :payments
    resources :invoices
    resources :members do
      member do
        get :click_message
      end
    end
    resources :messages do
      collection do
        post :blast_message
      end
    end
    resources :report_refunds
  end

  namespace :merchants do
    get 'dashboard', to: 'home#dashboard', as: 'dashboard'
    resources :products
    resources :merchants
    resources :payments
    resources :orders do
      member do
        put :change_number_shipping
      end
    end
    resources :order_items
  end
end
