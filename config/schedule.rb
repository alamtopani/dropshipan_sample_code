set :environment, "staging"
# set :environment, ENV['RAILS_ENV']

set :output, "/home/dropshipan/staging.dropshipan.com/shared/log/cron_log.log"
set :bundle_command, "/home/dropshipan/.rbenv/shims/bundle exec"

every 2.hours do
  command "/home/dropshipan/.rbenv/shims/backup perform --trigger pg_backup"
  command "sudo truncate /home/dropshipan/staging.dropshipan.com/shared/log/*.log --size 0"
end

every 4.hours do
  rake "cron_job:check_job_merchant"
end

every 5.hours do
  rake "cron_job:check_job_member"
end

every 6.hours do
  rake "cron_job:check_job_am"
end

every 1.days do
  # rake "cron_job:create_invoice_package"
  rake "cron_job:auto_cancellation_orders"
end

every 3.days do
  rake "log:clear"
end