# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
Rails.application.config.assets.precompile += %w( backend.js backend.scss thank_page.scss official.scss official.js ckeditor/*)
Rails.application.config.assets.precompile += %w( devise.js chartkick.js)
Rails.application.config.assets.precompile += %w( devise.scss )
Rails.application.config.assets.precompile += %w( userpage.js )
Rails.application.config.assets.precompile += %w( userpage.scss )
Rails.application.config.assets.precompile += %w( pdf.scss )
Rails.application.config.assets.precompile += %w( shop.js )
Rails.application.config.assets.precompile += %w( shop.scss )
Rails.application.config.assets.precompile += %w( landing_press.js )
Rails.application.config.assets.precompile += %w( landing_press.scss )
Rails.application.config.assets.precompile += %w( thank_page.scss )



